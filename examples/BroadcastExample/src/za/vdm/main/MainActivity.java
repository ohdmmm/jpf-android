package za.vdm.main;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

  public int count = 0;

  LocalReceiver r = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Button button1 = (Button) findViewById(R.id.button1);
    button1.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (r == null) {
          r = new LocalReceiver();
          System.out.println("Register Receiver");
          registerReceiver(r, new IntentFilter("za.vdm.main.LOCAL_BROADCAST"));
        }
      }
    });

    Button button2 = (Button) findViewById(R.id.button2);
    button2.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (r != null) {
          System.out.println("Unregister Receiver");
          unregisterReceiver(r);
          r = null;
        }
      }
    });

    Button button3 = (Button) findViewById(R.id.button3);
    button3.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        System.out.println("Send intent LocalReceiver");
        sendIntent();
      }
    });

    Button button4 = (Button) findViewById(R.id.button4);
    button4.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        System.out.println("Send Intent NormalReceiver");
        test1();
      }
    });

  }

  public void sendIntent() {
    Intent i = new Intent("za.vdm.main.LOCAL_BROADCAST");
    Bundle b = new Bundle();
    b.putInt("value", 4);
    i.putExtras(b);
    sendBroadcast(i);
  }

  /**
   * Statically registered BR receives B + enabled/disable + export=false
   */
  public void test1() {
    Intent i = new Intent(this, za.vdm.receivers.normal.NormalReceiver1.class);
    i.putExtra("value", count++);
    sendBroadcast(i);
  }

  public static class LocalReceiver extends BroadcastReceiver {
    int value = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
      System.out.println("LocalReceiver: onReceive");

      Bundle b = intent.getExtras();
      value = b.getInt("value");
    }
  }
}
