package com.mamlambo.supersimpledialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class SuperSimpleDialogsActivity extends Activity {

  private static final int MY_SUPER_SIMPLE_DIALOG_ID = 0;
  private static final int MY_SIMPLE_DIALOG_ID = 1;
  private static final int MY_DIALOG_ID = 2;

  // private static final int MY_DATE_DIALOG_ID = 3;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
  }

  public void onToastButtonClick(View v) {
    // showDialog(MY_SUPER_SIMPLE_DIALOG_ID);
  }

  public void onDialogButtonClick(View v) {
    showDialog(MY_SIMPLE_DIALOG_ID);
    System.out.println("showDialog(MY_SIMPLE_DIALOG_ID)");

  }

  public void onDateDialogButtonClick(View v) {
    showDialog(MY_DIALOG_ID);
    System.out.println("showDialog(MY_DIALOG_ID)");

  }

  int test = 0;

  @Override
  protected Dialog onCreateDialog(int id) {
    switch (id) {
    // case MY_SUPER_SIMPLE_DIALOG_ID:
    // DatePickerDialog dateDlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
    // public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
    // Time chosenDate = new Time();
    // chosenDate.set(dayOfMonth, monthOfYear, year);
    // System.out.println("SuperSimpleDialogsActivity.this Date picked: " + chosenDate.year + " "
    // + chosenDate.month + " " + chosenDate.monthDay);
    // long dtDob = chosenDate.toMillis(true);
    // CharSequence strDate = DateFormat.format("MMMM dd, yyyy", dtDob);
    // System.out.println("SuperSimpleDialogsActivity.this Date picked: " + strDate);
    // }
    // }, 2011, 0, 1);
    //
    // dateDlg.setMessage("When's Your Birthday?");
    //
    // return dateDlg;

    case MY_SIMPLE_DIALOG_ID:
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      builder.setTitle(R.string.dialog_title);
      builder.setMessage(R.string.dialog_message);
      // builder.setIcon(android.R.drawable.btn_star);
      builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          test = 4;
          System.out.println("Clicked OK!");
          return;
        }
      });
      return builder.create();

    case MY_DIALOG_ID:
      AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
      builder2.setTitle(R.string.dialog_title);
      // builder2.setIcon(android.R.drawable.btn_star);
      builder2.setMessage("");
      builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          System.out.println("Clicked OK2!");
          return;
        }
      });

      builder2.setNegativeButton("NO", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          System.out.println("Clicked NO!");
          return;
        }
      });

      return builder2.create();

      // case MY_DATE_DIALOG_ID:
      // DatePickerDialog dateDlg = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
      // public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
      // Time chosenDate = new Time();
      // chosenDate.set(dayOfMonth, monthOfYear, year);
      // long dtDob = chosenDate.toMillis(true);
      // CharSequence strDate = DateFormat.format("MMMM dd, yyyy", dtDob);
      // Toast.makeText(SuperSimpleDialogsActivity.this, "Date picked: " + strDate,
      // Toast.LENGTH_SHORT).show();
      // }
      // }, 2011,0, 1);
      //
      // dateDlg.setMessage("When's Your Birthday?");

      // return dateDlg;
    }
    return null;

  }

  @Override
  protected void onPrepareDialog(int id, Dialog dialog) {
    super.onPrepareDialog(id, dialog);
    switch (id) {
    // case MY_SUPER_SIMPLE_DIALOG_ID:
    // // Static dialog contents. No initialization needed
    // break;

    case MY_SIMPLE_DIALOG_ID:
      // Static dialog contents. No initialization needed
      break;
    case MY_DIALOG_ID:
      // Some initialization needed. Date/time changes each time this
      // dialog is displayed, so update its contents in prepare (not
      // create)
      AlertDialog myDialog = (AlertDialog) dialog;
      // Date mCurrentTime = new Date();
      // SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
      myDialog.setMessage("This dialog was launched at ");// + dFormat.format(mCurrentTime));
      break;

    // case MY_DATE_DIALOG_ID:
    // DatePickerDialog dateDlg = (DatePickerDialog) dialog;
    // int iDay,
    // iMonth,
    // iYear;
    //
    // // Always init the date picker to today's date
    // Calendar cal = Calendar.getInstance();
    // iDay = cal.get(Calendar.DAY_OF_MONTH);
    // iMonth = cal.get(Calendar.MONTH);
    // iYear = cal.get(Calendar.YEAR);
    // dateDlg.updateDate(iYear, iMonth, iDay);
    // break;
    }
    return;
  }

  @Override
  protected void onDestroy() {
System.out.println("onDestroy()");
super.onDestroy();
  }

  @Override
  protected void onPause() {
    System.out.println("onPause()");
    super.onPause();
  }

  @Override
  protected void onRestart() {
    System.out.println("onRestart()");
    super.onRestart();
  }

  @Override
  protected void onResume() {
    System.out.println("onResume()");

     super.onResume();
  }

  @Override
  protected void onStart() {
    System.out.println("onStart()");
    super.onStart();
  }

  @Override
  protected void onStop() {
    System.out.println("onStop()");
    super.onStop();
  }

  @Override
  public void finish() {
    System.out.println("finish()");
    super.finish();
  }

  @Override
  public void onBackPressed() {
    System.out.println("onBackPressed()");
    super.onBackPressed();
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    System.out.println("onKeyDown()");
    return super.onKeyDown(keyCode, event);
  }
  
  
}