package com.example.com;

import android.app.ListActivity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.Contacts.People;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MylistView2 extends ListActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.listview2);

    ListView listView = getListView();
    System.out.println("test " + listView);

    // id this null?
    ListAdapter adapter = getListAdapter();
    System.out.println("test2 " + adapter);
    SimpleCursorAdapter mAdapter = new SimpleCursorAdapter(this,
    // Use a template that displays a text view
        R.layout.contact_row,
        // Give the cursor to the list adatper
        createCursor(""),
        // Map from database columns...
        new String[] { People.CUSTOM_RINGTONE, People.STARRED, People.DISPLAY_NAME },
        // To widget ids in the row layout...
        new int[] {
            R.id.row_ringtone,
            R.id.row_starred,
            R.id.row_display_name });
    
    mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
      public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
        String name = cursor.getColumnName(columnIndex);
        String value = cursor.getString(columnIndex);
        System.out.println("MylistView2 " + columnIndex + " " + name + " " + value);

        if (name.equals(People.CUSTOM_RINGTONE)) {
          if (value != null && value.length() > 0) {
            view.setVisibility(View.VISIBLE);
          } else {
            view.setVisibility(View.INVISIBLE);
          }
          return true;
        }
        if (name.equals(People.STARRED)) {
          if (value != null && value.equals("1")) {
            view.setVisibility(View.VISIBLE);
          } else {
            view.setVisibility(View.INVISIBLE);
          }
          return true;
        }

        return false;
      }
    });

    setListAdapter(mAdapter);

    // On click, assign ringtone to contact
    getListView().setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick(AdapterView parent, View view, int position, long id) {
        System.out.println("CHECK5");
        Toast.makeText(MylistView2.this, "testing " + position, Toast.LENGTH_SHORT).show();
      }
    });

  }

  private Cursor createCursor(String filter) {
    String selection;
    if (filter != null && filter.length() > 0) {
      selection = "(DISPLAY_NAME LIKE \"%" + filter + "%\")";
    } else {
      selection = null;
    }
    Cursor cursor = managedQuery(getContactContentUri(), new String[] { People._ID, People.CUSTOM_RINGTONE,
        People.DISPLAY_NAME, People.LAST_TIME_CONTACTED, People.STARRED, People.TIMES_CONTACTED }, selection,
        null, "STARRED DESC, " + "TIMES_CONTACTED DESC, " + "LAST_TIME_CONTACTED DESC, " + "DISPLAY_NAME ASC");

    Log.i("Ringdroid", cursor.getCount() + " contacts");

    return cursor;
  }

  private Uri getContactContentUri() {
    return Contacts.People.CONTENT_URI;
  }

}
