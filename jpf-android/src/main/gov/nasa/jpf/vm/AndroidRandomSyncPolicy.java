package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;

import java.util.Random;

public class AndroidRandomSyncPolicy extends AllRunnablesSyncPolicy {

  Random rand = new Random();

  public AndroidRandomSyncPolicy(Config config) {
    super(config);
  }

  @Override
  protected ThreadInfo[] getTimeoutRunnables(ApplicationContext appCtx) {
    ThreadInfo[] allRunnables = super.getTimeoutRunnables(appCtx);
    int i = rand.nextInt(allRunnables.length);
    return new ThreadInfo[] { allRunnables[i] };
  }
}
