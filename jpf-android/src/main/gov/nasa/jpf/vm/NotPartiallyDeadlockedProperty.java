/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */
package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.GenericProperty;
import gov.nasa.jpf.search.Search;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;

/**
 * property class to check for no-runnable-threads conditions
 */
public class NotPartiallyDeadlockedProperty extends GenericProperty {
  Search search;

  public NotPartiallyDeadlockedProperty(Config conf, Search search) {
    this.search = search;
  }

  @Override
  public String getErrorMessage() {
    VM vm = search.getVM();
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);

    pw.println("deadlock encountered:");

    ThreadInfo[] liveThreads = vm.getLiveThreads();
    for (ThreadInfo ti : liveThreads) {
      pw.print("  ");
      pw.println(ti.getStateDescription());
    }

    return sw.toString();
  }

  @Override
  public boolean check(Search search, VM vm) {
    ThreadList tl = vm.getThreadList();

    // list of threads blocked and waiting for lock
    ThreadInfo[] blocked = new ThreadInfo[tl.length()];
    int i = 0;
    for (ThreadInfo ti : tl.threads) {
      if (ti.isBlocked()) {
        blocked[i] = ti;
        i++;
      }
    }
    if (i > 1) {
      System.out.println("\n\n " + getErrorMessage() + "\n\n");
      ThreadInfo[] tmp = new ThreadInfo[i];
      System.arraycopy(blocked, 0, tmp, 0, i);
      blocked = tmp;

      // connects each thread with the holding the lock is requires
      HashMap<Integer, Integer> edges = new HashMap<Integer, Integer>();
      for (int i1 = 0; i1 < blocked.length; i1++) {
        int lockRef = blocked[i1].lockRef;
        // for each thread, find another thread holding its lock
        for (int i2 = 0; i2 < blocked.length; i2++) {
          if (i1 != i2) {
            for (int lock : blocked[i2].lockedObjectReferences) {
              if (lock == lockRef) {
                edges.put(i1, i2);
                break;
              }
            }
          }
        }
      }

      // traverse edges until end state or previous state (cycle) is found
      for (int i1 = 0; i1 < blocked.length; i1++) {

        // keeps which locks have been seen for the
        int[] blocked_check = new int[blocked.length];
        for (int j = 0; j < blocked.length; j++) {
          blocked_check[j] = -1;
        }

        // current root is "check"
        int check = i1;
        blocked_check[check] = 1;
        Integer next = null;
        do {
          next = edges.get(check);
          if (next == null) {
            break;
          } else if (blocked_check[next] > -1) {
            return false;
          }
          blocked_check[next] = 1;
          check = next;
        } while (true);
      }
    }
    return true;

  }

  @Override
  public void reset() {

  }
}
