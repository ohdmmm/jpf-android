/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf.listener;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ExceptionPrinter extends ListenerAdapter {

  /** events directory where to put results in */
  String outputDir;

  public ExceptionPrinter(Config config) {
    String output = config.getString("jpf-android.output.run", ".");
    try {
      // Assume file does not exist
      Path p = Files.createDirectories(Paths.get(output + File.separator + "cgs"));
      outputDir = p.toFile().getAbsolutePath();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void exceptionThrown(VM vm, ThreadInfo currentThread, ElementInfo thrownException) {
    printException(currentThread, thrownException);
  }

  @Override
  public void exceptionBailout(VM vm, ThreadInfo currentThread) {
    System.out.println("ExceptionPrinter: exceptionBailout");
    currentThread.printStackTrace();
  }

  @Override
  public void exceptionHandled(VM vm, ThreadInfo currentThread) {
    System.out.println("ExceptionPrinter: exceptionHandled");
    currentThread.printStackTrace();
  }

  public void printException(ThreadInfo currentThread, ElementInfo thrownException) {
    PrintWriter w;
    try {
      w = new PrintWriter(Files.newBufferedWriter(Paths.get(outputDir + File.separator + "exceptions.log")));
      w.write("****************** Exception *****************\n");
      w.write(thrownException + "\n");
      System.out.println(thrownException);
      currentThread.printStackTrace(w, thrownException.getObjectRef());
      currentThread.printStackTrace();
      w.write("****************** Exception *****************");
      w.flush();
      w.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
