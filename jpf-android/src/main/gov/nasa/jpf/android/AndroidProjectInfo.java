package gov.nasa.jpf.android;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.android.RParser.RParseException;
import gov.nasa.jpf.util.JPFLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Helper class that retrieves and parses the project package.
 * 
 * @author Heila
 * 
 */
public class AndroidProjectInfo {
  static JPFLogger log = JPF.getLogger("gov.nasa.jpf.android.AndroidProjectInfo");

  /**
   * Stored the two type of builders that can be used to build an Android project
   *
   */
  public static enum BuildType {
    GRADLE, ANT;
  }

  public static String layoutType = "layout";

  /** The absolute path to the project on the filesystem. */
  public String projectDir;

  /**
   * The build tool that was used to build the project. This is important because it determines where the
   * compiled class files are stored after a build.
   */
  private BuildType buildType;

  /* Quick lookup for the absolute path to the build/bin directory */
  private String antBuildDirectoryPath;
  private String gradleBuildDirectoryPath;

  /** Used to store the registered component information */
  private AndroidManifest androidManifest;

  /** Used to map resources hex values to filename */
  private RFile RFile;

  /** used to look for layout files */
  private String layoutDir;

  private String menuDir;
  private String prefsDir;

  /** Cache that maps a resource id to its String value */
  private Map<String, String> stringNameToValueMap;

  /** Cache that maps a resource id to an inflated String[] */
  private Map<String, String[]> arrayNameToValueMap;

  /** Cache that maps a resource id to its String value */
  private Map<String, Integer> intNameToValueMap;

  /** Cache that maps a resource id to its String value */
  private Map<String, Boolean> boolNameToValueMap;

  /** This is a singleton instance of the class */
  private static AndroidProjectInfo instance;

  /**
   * Get singleton instance
   * 
   * @throws ProjectParseException
   */
  public static AndroidProjectInfo get() {
    if (instance == null) {
      throw new RuntimeException(
          "AndroidProjectInfo has not been initialized in JPF_android_os_ServiceManager.init0() before being requested.");
    }
    return instance;
  }

  public static boolean isInitialized() {
    if (instance == null) {
      return false;
    }
    return true;
  }

  public static void initialize(String projectPath) throws ProjectParseException {
    if (instance == null) {
      instance = new AndroidProjectInfo(projectPath);
    } else {
      log.warning("AndroidProjectInfo has already been initialized");
    }
  }

  public AndroidProjectInfo(String projectPath) throws ProjectParseException {
    log.info("************************ Parsing Android Project ***********************");
    this.projectDir = projectPath;
    log.info("Project path: " + projectPath);

    // build path setup
    antBuildDirectoryPath = projectDir + File.separator + "bin";
    File antBuildDirectory = new File(antBuildDirectoryPath);

    gradleBuildDirectoryPath = projectDir + File.separator + "build";
    File gradleBuildDirectory = new File(gradleBuildDirectoryPath);

    if (antBuildDirectory.exists()) {
      buildType = BuildType.ANT;
      log.info("Build type: Detected ANT Project");
    } else if (gradleBuildDirectory.exists()) {
      buildType = BuildType.GRADLE;
      log.info("Build type: Detected Gradle Project");
    } else {
      throw new ProjectParseException(
          "The Android project has not been built. Please build it before running JPF-Android.");
    }

    // get path to manifest file (bin/AndroidManifest.xml or
    // build/manifests/release/AndroidManifest.xml)
    String manifestPath = null;
    if (buildType.equals(BuildType.ANT)) {
      manifestPath = antBuildDirectoryPath + File.separator + "AndroidManifest.xml";
    } else {
      manifestPath = gradleBuildDirectoryPath + File.separator + "manifests" + File.separator + "release"
          + File.separator + "AndroidManifest.xml";
    }
    parseAndroidManifest(manifestPath);

    // parse R.java file
    String RPath = null;
    if (buildType.equals(BuildType.ANT)) {
      RPath = projectDir + File.separator + "gen" + File.separator
          + androidManifest.getPackageNameAsDirectoryPath() + File.separator + "R.java";
    } else {
      RPath = projectDir + File.separator + "build" + File.separator + "source" + File.separator + "r"
          + File.separator + "release" + File.separator + androidManifest.getPackageNameAsDirectoryPath()
          + File.separator + "R.java";
    }
    parseRFile(RPath);

    // get path to layout files (/build/res/all/release/layout)
    if (buildType.equals(BuildType.ANT)) {
      layoutDir = projectDir + File.separator + "res" + File.separator + layoutType;
      menuDir = projectDir + File.separator + "res" + File.separator + "menu";
      prefsDir = projectDir + File.separator + "res" + File.separator + "xml";

    } else {
      layoutDir = gradleBuildDirectoryPath + File.separator + "res" + File.separator + "all" + File.separator
          + "release" + File.separator + layoutType;
      menuDir = gradleBuildDirectoryPath + File.separator + "res" + File.separator + "all" + File.separator
          + "release" + File.separator + "menu";
      prefsDir = gradleBuildDirectoryPath + File.separator + "res" + File.separator + "all" + File.separator
          + "release" + File.separator + "xml";
    }

    // get path to value files (/build/res/all/release/values)
    String stringDir = null;
    if (buildType.equals(BuildType.ANT)) {
      stringDir = projectDir + File.separator + "res" + File.separator + "values";
    } else {
      stringDir = projectDir + File.separator + "build" + File.separator + "res" + File.separator + "all"
          + File.separator + "release" + File.separator + "values";
    }
    parseResourceValues(stringDir);

    log.info("AndroidProjectInfo: successfully initialized.");

  }

  protected AndroidProjectInfo(String projectDir, BuildType buildType) {

  }

  public void parseRFile(String rPath) throws ProjectParseException {
    // Search for R.java file (either gen/package or
    // /build/source/r/release/com/example/calculator/R.java)

    File rfile = new File(rPath);
    if (!rfile.exists()) {
      throw new ProjectParseException("Could not find R.java file at: " + rPath
          + ". Did build complete without errors?");
    } else {
      try {
        RFile = RParser.getInstance().parse(new FileInputStream(rfile));
      } catch (RParseException e) {
        throw new ProjectParseException(e.getMessage());
      } catch (FileNotFoundException e) {
        throw new ProjectParseException(e.getMessage());
      }
    }
    if (rfile != null) {
      log.info("R.java file parsed successfully");
    }

  }

  public void parseAndroidManifest(String manifestPath) throws ProjectParseException {
    File androidManifestFile = new File(manifestPath);
    if (!androidManifestFile.exists()) {
      throw new ProjectParseException("Could not find AndroidManifest.xml file on path: " + manifestPath
          + ". Did build complete without errors?");
    } else {
      try {
        androidManifest = AndroidManifestParser.getInstance().parse(new FileInputStream(androidManifestFile));
      } catch (Exception s) {
        s.printStackTrace();
        throw new ProjectParseException("AndroidManifest.xml could not parse correctly: " + s.getMessage());
      }
    }
    if (androidManifest != null) {
      log.info("AndroidManifest parsed successfully");
    }

  }

  public void parseResourceValues(String stringDir) throws ProjectParseException {

    try {
      stringNameToValueMap = new HashMap<String, String>();
      arrayNameToValueMap = new HashMap<String, String[]>();
      intNameToValueMap = new HashMap<String, Integer>();
      boolNameToValueMap = new HashMap<String, Boolean>();
      parseValueFiles(stringDir);
    } catch (Exception e) {
      e.printStackTrace();
      throw new ProjectParseException("Could not parse strings.xml" + e.getMessage());
    }

  }

  /**
   * 
   * @param stringDir
   * @throws SAXException
   * @throws IOException
   * @throws ParserConfigurationException
   */
  private void parseValueFiles(String stringDir) throws SAXException, IOException,
      ParserConfigurationException {
    DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();

    for (File file : new File(stringDir).listFiles()) {
      if (!file.getName().endsWith(".xml") || file.getName().endsWith("AndroidManifest.xml")) {
        continue;
      }
      Document doc = db.parse(file);

      // parse string values
      NodeList nodeList = doc.getElementsByTagName("string");
      Node node = null;
      for (int i = 0; i < nodeList.getLength(); i++) {
        node = nodeList.item(i);
        String nameAttributeString = node.getAttributes().getNamedItem("name").toString();
        String[] nameAttrSplit = nameAttributeString.split("=");
        stringNameToValueMap.put((String) nameAttrSplit[1].subSequence(1, nameAttrSplit[1].length() - 1),
            node.getTextContent());
      }
      nodeList = doc.getElementsByTagName("int");
      node = null;
      for (int i = 0; i < nodeList.getLength(); i++) {
        node = nodeList.item(i);
        String nameAttributeString = node.getAttributes().getNamedItem("name").toString();
        String[] nameAttrSplit = nameAttributeString.split("=");
        intNameToValueMap.put((String) nameAttrSplit[1].subSequence(1, nameAttrSplit[1].length() - 1),
            Integer.parseInt(node.getTextContent()));
      }
      nodeList = doc.getElementsByTagName("bool");
      node = null;
      for (int i = 0; i < nodeList.getLength(); i++) {
        node = nodeList.item(i);
        String nameAttributeString = node.getAttributes().getNamedItem("name").toString();
        String[] nameAttrSplit = nameAttributeString.split("=");
        boolNameToValueMap.put((String) nameAttrSplit[1].subSequence(1, nameAttrSplit[1].length() - 1),
            Boolean.valueOf(node.getTextContent()));
      }

      nodeList = doc.getElementsByTagName("string-array");
      node = null;
      for (int i = 0; i < nodeList.getLength(); i++) {
        node = nodeList.item(i);
        NodeList items = node.getChildNodes();
        List<String> stringArray = new LinkedList<String>();
        for (int c = 0; c < items.getLength(); c++) {
          Node item = items.item(c);
          if (!item.getNodeName().equals("item"))
            continue;
          String text = item.getTextContent();
          if (text != null) {
            stringArray.add(text);
          }
        }
        String nameAttributeString = node.getAttributes().getNamedItem("name").toString();
        String[] nameAttrSplit = nameAttributeString.split("=");

        arrayNameToValueMap.put((String) nameAttrSplit[1].subSequence(1, nameAttrSplit[1].length() - 1),
            stringArray.toArray(new String[stringArray.size()]));

      }
    }
  }

  /**
   * Returns the absolute path to the project on the file system.
   * 
   * @return the absolute path of the project directory.
   */
  public String getProjectDir() {
    return projectDir;
  }

  /**
   * Returns the absolute path to the Android manifest file of the project. This going to be a problem. Since
   * Android applications can have multiple AndroidManifest files we need to specify in Config which one we
   * want to use. For now we will assume the file is either in Project's directory or in the src/ directory
   * 
   * @return path to the manifest file.
   */
  public AndroidManifest getAndroidManifest() {
    return androidManifest;
  }

  /**
   * Returns the R file object
   * 
   * @param packageName
   *          the package name of the project
   * @return the path to the R.java file.
   */
  public RFile getRFile() {
    return RFile;
  }

  /**
   * Returns the absolute path to the layout files of the project.
   * 
   * @return the path to the layout files
   * @throws ProjectParseException
   */
  public String getLayoutFilename(Integer layoutID) throws ProjectParseException {

    String filename = null;
    try {
      filename = RFile.getLayoutNameForId(layoutID);
    } catch (Exception e) {
      throw new ProjectParseException(e.getMessage());
    }

    if (filename != null) {
      filename = layoutDir + File.separator + filename + ".xml";
      File layoutfile = new File(filename);
      if (!layoutfile.exists()) {
        log.warning("Could not find layout file: " + filename);
      }
    } else {
      log.warning("Could not find layout file: " + filename);
    }
    return filename;
  }

  /**
   * Returns the String value given its id
   * 
   * @throws ProjectParseException
   * 
   * 
   */
  public String getStringValue(int id) throws ProjectParseException {
    String stringName;
    try {
      stringName = RFile.getStringNameForId(id);
    } catch (Exception e) {
      log.warning(e.getMessage());
      return null;
    }
    return stringNameToValueMap.get(stringName);
  }

  /**
   * Returns the String value given its name
   */
  public String getStringValue(String name) {
    return stringNameToValueMap.get(name);
  }

  public String getPreferenceFilename(int resourceID) throws ProjectParseException {
    String filename = null;
    try {
      filename = RFile.getXmlNameForId(resourceID);
      if (filename == null) {
        filename = RFile.getLayoutNameForId(resourceID);
        filename = layoutDir + File.separator + filename + ".xml";
      } else {
        filename = prefsDir + File.separator + filename + ".xml";
      }
    } catch (Exception e) {
      throw new ProjectParseException(e.getMessage());
    }

    File layoutfile = new File(filename);
    if (!layoutfile.exists()) {
      log.warning("Could not find preference file: " + filename);
    }
    return filename;
  }

  public String getMenuLayoutFilename(int resourceID) throws ProjectParseException {
    String filename = null;
    try {
      filename = RFile.getMenuNameForId(resourceID);
    } catch (Exception e) {
      throw new ProjectParseException(e.getMessage());
    }

    filename = menuDir + File.separator + filename + ".xml";
    File layoutfile = new File(filename);
    if (!layoutfile.exists()) {
      log.warning("Could not find layout file: " + filename);
    }
    return filename;
  }

  // /**
  // * Used by xml parser to lookup filename of resource containing xml content with id resourceID.
  // *
  // * @param resourceID
  // * @return
  // * @throws ProjectParseException
  // */
  // public String getFilename(int resourceID) throws ProjectParseException {
  // String filename = null;
  // filename = RFile.getResourceName(resourceID);
  // if (filename == null)
  // throw new ProjectParseException("AndroidProjectInfo: Could not find resource with id " + resourceID);
  // // search for file in
  //
  // File resFolder = new File(projectDir + "/res");
  // File file = search(resFolder, filename + ".xml");
  //
  // if (file == null) {
  // throw new ProjectParseException("AndroidProjectInfo: Could not find resource file \"" + projectDir
  // + "/res.\"");
  // }
  // return file.getAbsolutePath();
  // }

  private File search(File folder, String filename) throws ProjectParseException {
    filename = filename.toLowerCase();
    File ret = null;
    if (folder.exists() && folder.isDirectory()) {
      // System.out.println("Searching directory ... " +
      // folder.getAbsoluteFile());

      // do you have permission to read this directory?
      if (folder.canRead()) {
        for (File temp : folder.listFiles()) {
          if (temp.isDirectory()) {
            File f = search(temp, filename);
            if (f != null) {
              ret = f;
            }
          } else {
            if (filename.equals(temp.getName().toLowerCase())) {
              ret = temp;
            }
          }
        }
      } else {
        throw new ProjectParseException("AndroidProjectInfo: Could not find resource folder");
      }
    }
    return ret;

  }

  /**
   * Returns the value (object) of a resource inflated from xml with name "@type/valueName".
   * 
   * 
   * @param valueName
   * @return
   */
  public Object getValue(String valueName) {
    if (valueName.startsWith("@string/")) {
      return getStringValue(valueName.substring(8));
    } else if (valueName.startsWith("@array/")) {
      // parse arrays.xml in values dir
      return getArrayObject(valueName.substring(7));
    } else if (valueName.startsWith("@bool/")) {
      // parse arrays.xml in values dir
      return boolNameToValueMap.get(valueName.substring(7));
    } else if (valueName.startsWith("@layout/")) {
      // parse arrays.xml in values dir
      valueName = valueName.substring(8);
      return getRFile().getLayoutIdForName(valueName);
    } else {
      log.warning("Could not find resource by name \"" + valueName + "\"");
      return null;
      // throw new RuntimeException("Could not find resource by name \"" + valueName + "\"");
    }
  }

  public String[] getArrayObject(String resourceName) {
    // check if values has been cached
    String[] retArray = arrayNameToValueMap.get(resourceName);
    for (int i = 0; i < retArray.length; i++) {
      String value = retArray[i];
      if (value.startsWith("@")) {
        retArray[i] = getStringValue(retArray[i].substring(8));
      }
    }
    return retArray;
  }

  public static class ProjectParseException extends Exception {
    private static final long serialVersionUID = 1L;

    public ProjectParseException() {
    }

    public ProjectParseException(String message) {
      super(message);
    }
  }

}
