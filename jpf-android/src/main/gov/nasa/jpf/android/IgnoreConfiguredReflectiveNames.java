/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf.android;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.serialize.FieldAmmendmentByName;

public class IgnoreConfiguredReflectiveNames extends FieldAmmendmentByName {

  // "java.util.Date.milliseconds",
  // "java.util.Date.fasttime",
  static final String[] reflectiveNameFields = { "java.util.Random.seed",
      "java.util.regex.Matcher.nInstances", "java.text.Format.nInstances", "android.app.ResultInfo.CREATOR",
      "java.util.HashMap.entrySet", "java.util.HashMap.threshold", "android.util.ArrayMap.mBaseCache",
      "android.util.ArrayMap.mBaseCacheSize", "android.util.ArrayMap.mTwiceBaseCache",
      "android.util.ArrayMap.mTwiceBaseCacheSize", "android.view.InputEvent.mNextSeq",
      "android.view.InputEvent.mSeq", "android.text.Editable$Factory.sInstance",
      "android.widget.TextView$BufferType", "android.provider.ContactsContract$Contacts",
      "android.provider.ContactsContract$Contacts$People.", "android.provider.Contacts",
      "android.provider.Contacts$People", "java.lang.Class.enumConstantDirectory",
      "android.net.NetworkInfo.stateMap", "java.lang.String.hash",
      "java.util.Arrays$LegacyMergeSort.userRequested", "sun.misc.FloatingDecimal.B2AC_NEGATIVE_INFINITY",
      "sun.misc.FloatingDecimal.B2AC_POSITIVE_INFINITY", "sun.misc.FloatingDecimal.INFINITY_LENGTH",
      "sun.misc.FloatingDecimal.B2AC_NOT_A_NUMBER", "sun.misc.FloatingDecimal.B2AC_POSITIVE_ZERO",
      "sun.misc.FloatingDecimal.B2AC_NEGATIVE_ZERO",
      "sun.misc.FloatingDecimal.threadLocalBinaryToASCIIBuffer",
      "sun.misc.FloatingDecimal.A2BC_POSITIVE_INFINITY", "sun.misc.FloatingDecimal.A2BC_NEGATIVE_INFINITY",
      "sun.misc.FloatingDecimal.A2BC_NOT_A_NUMBER", "sun.misc.FloatingDecimal.A2BC_POSITIVE_ZERO",
      "sun.misc.FloatingDecimal.A2BC_NEGATIVE_ZERO", "sun.misc.FloatingDecimal.$assertionsDisabled",
      "sun.misc.FloatingDecimal$BinaryToASCIIBuffer.insignificantDigitsNumber",
      "sun.misc.FloatingDecimal$BinaryToASCIIBuffer.N_5_BITS", "sun.misc.FloatingDecimal.NAN_LENGTH" };

  static final String[] ignoreFields = initializeFields();

  private static String[] initializeFields() {
    Config c = VM.getVM().getConfig();
    String[] ignoreList = c.getStringArray("filter.ignoreFields");
    String[] combined = {};
    if (ignoreList != null && ignoreList.length > 0) {
      combined = concat(reflectiveNameFields, ignoreList);
    } else {
      combined = reflectiveNameFields;
    }
    return combined;
  }

  public static String[] concat(String[] a1, String[] a2) {
    int a1Len = a1.length;
    int a2Len = a2.length;
    String[] c = new String[a1Len + a2Len];
    System.arraycopy(a1, 0, c, 0, a1Len);
    System.arraycopy(a2, 0, c, a1Len, a2Len);
    return c;
  }

  public IgnoreConfiguredReflectiveNames() {
    super(ignoreFields, POLICY_IGNORE);
  }

  // must be at bottom!
  public static final IgnoreConfiguredReflectiveNames instance = new IgnoreConfiguredReflectiveNames();
}
