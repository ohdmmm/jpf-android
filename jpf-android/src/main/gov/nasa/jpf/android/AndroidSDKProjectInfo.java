package gov.nasa.jpf.android;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.android.AndroidProjectInfo.ProjectParseException;
import gov.nasa.jpf.util.JPFLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class retries string values and layout filenames by searching the android R.java and xml files.
 * 
 * input: hash of string value output: actual string value
 * 
 * R.xml
 * 
 * @author Heila van der Merwe
 * 
 */
public class AndroidSDKProjectInfo {
  static JPFLogger log = JPF.getLogger("gov.nasa.jpf.android.AndroidSDKProjectInfo");

  /** Used to map id's to Strings */
  private Map<Integer, String> hashToStringValueMap;

  private Map<Integer, String> hashToLayoutName;

  private String jpaPath;

  private static AndroidSDKProjectInfo instance;

  public static AndroidSDKProjectInfo get() {
    if (instance == null) {
      throw new RuntimeException(
          "AndroidSDKProjectInfo has not been initialized in JPF_android_os_ServiceManager.init0() before being requested.");

    }
    return instance;
  }

  public static void initialize(String jpaPath) {
    if (instance == null) {
      instance = new AndroidSDKProjectInfo();
      // TODO need to check this
      System.out.println("JPF-Android Directory: " + jpaPath);
      instance.jpaPath = jpaPath;
    } else {
      log.warning("AndroidProjectInfo has already been initialized");
    }
  }

  public String grepForName(int hash, String filename) {

    BufferedReader br;
    // the hash int value to search for in a line
    String inputSearch = String.valueOf(hash) + ";";
    String line = "";

    try {
      br = new BufferedReader(new FileReader(filename));
      try {
        while ((line = br.readLine()) != null) {
          if (line.endsWith(inputSearch)) {
            break;
          }
        }
        br.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    if (line != null) {
      // line was found, extract the name of the constant
      Pattern pattern = Pattern.compile("(    public static final int )(.*)( =)");
      Matcher matcher = pattern.matcher(line);
      if (matcher.find()) {
        return matcher.group(2);
      }
    }

    return null;
  }

  public String searchForValue(String filename, String name) {
    BufferedReader br;
    // the hash int value to search for in a line
    name.trim();
    String inputSearch = "    <string name=\"" + name + "\">";
    String line = null;

    try {
      br = new BufferedReader(new FileReader(filename));
      try {
        while ((line = br.readLine()) != null) {
          if (line.startsWith(inputSearch)) {
            break;
          }
        }
        br.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    if (line != null) {
      // line was found, extract the name of the constant
      Pattern pattern = Pattern.compile("(<string name=.*?>)(.*)(</string>)");
      Matcher matcher = pattern.matcher(line);
      if (matcher.find()) {
        return matcher.group(2);
      }
    }

    return null;

  }

  public AndroidSDKProjectInfo() {

    // search for bin(ant) or build(gradle) folder to confirm build tool
    // if none exist, throw error that project has not been not built.

    // get path to value files (/build/res/all/release/values)
    try {
      hashToStringValueMap = new HashMap<Integer, String>();
      hashToLayoutName = new HashMap<Integer, String>();
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  /**
   * Returns the absolute path to the layout files of the project.
   * 
   * @return the path to the layout files
   * @throws ProjectParseException
   */
  public String getLayoutFilename(Integer layoutID) {

    String filename = hashToLayoutName.get(layoutID);
    if (filename == null) {
      filename = grepForName(layoutID, jpaPath + "/res/android/R.java");
      if (filename == null) {
        filename = grepForName(layoutID, jpaPath + "/res/com/android/internal/R.java");
      }
      if (filename != null) {
        hashToLayoutName.put(layoutID, filename);
      }

    }

    String filepath = System.getenv("ANDROID_HOME") + "/platforms/android-19/data/res/layout"
        + File.separator + filename + ".xml";

    return filepath;
  }

  /**
   * Returns the String value
   * 
   * @throws ProjectParseException
   * 
   * 
   */
  public String getStringValue(int id) {
    String string = hashToStringValueMap.get(id);
    if (string == null) {
      String stringName = grepForName(id, jpaPath + "/res/android/R.java");
      if (stringName == null) {
        stringName = grepForName(id, jpaPath + "/res/com/android/internal/R.java");
      }
      if (stringName != null) {
        string = searchForValue(System.getenv("ANDROID_HOME")
            + "/platforms/android-19/data/res/values/strings.xml", stringName);
        if (string != null)
          hashToStringValueMap.put(id, stringName);
      }
      return string;

    } else {
      return string;
    }
  }
}
