//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA). All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3. The NOSA has been approved by the Open Source
// Initiative. See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.android;

import gov.nasa.jpf.JPF;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * AndroidManifest.xml parser. Implemented as a singleton class. Creates a PackageInfo object containing all
 * the package information. It also stores all filters, permissions etc. defined in the AndroidManifest.xml
 * file of the SUT.
 * 
 * @author Heila van der Merwe
 * 
 * 
 *         TODO:
 * 
 *         - parse permissions - check filters - add further properties as needed for Activity, Service and
 *         Content Provider
 */
/**
 * AndroidManifest.xml parser. Implemented as a singleton class. Creates a PackageInfo object containing all
 * the package information. It also stores all filters, permissions etc. defined in the AndroidManifest.xml
 * file of the SUT.
 * 
 * @author Heila van der Merwe
 * 
 * 
 *         TODO:
 * 
 *         - parse permissions - check filters - add further properties as needed for Activity, Service and
 *         Content Provider
 */
public class JPFXMLParser {

  public final static String TAG = JPFXMLParser.class.getSimpleName();
  static Logger log = JPF.getLogger("gov.nasa.jpf.android");

  private static class XMLInfo {
    // map of all nodes
    Map<Integer, Node> xmlMap;
    // root of the xml tree
    Node root;
    // name of the xml file
    String filename;

    public XMLInfo() {
      xmlMap = new HashMap<Integer, Node>();
    }
  }

  /** Stores a Map of XMLInfos for each xml file indexed by their resourceID */
  private static Map<Integer, XMLInfo> xmlMap = new HashMap<Integer, XMLInfo>();

  public String loadXML(int resourceID, String type) {
    // retrieve the file name of the layout resource
    String filename;

    if (resourceID == -1) {
      filename = "src/tests/gov/nasa/jpf/android/preferences/preferences.xml";
    } else {
      try {
        AndroidProjectInfo projectInfo = AndroidProjectInfo.get();
        if (type.equals("menu")) {
          filename = projectInfo.getMenuLayoutFilename(resourceID);
        } else if (type.equals("prefs")) {
          filename = projectInfo.getPreferenceFilename(resourceID);
        } else if (type.equals("layout")) {
          filename = projectInfo.getLayoutFilename(resourceID);
        } else
          throw new RuntimeException("XMLParser could not find filename for id: " + resourceID);
      } catch (Exception e1) {
        e1.printStackTrace();
        log.severe("XMLParser could not find filename for resource id: " + resourceID);
        throw new RuntimeException("XMLParser could not find filename for id: " + resourceID);
      }
    }
    // check if the xml file has been parsed before
    XMLInfo layoutInfo = xmlMap.get(resourceID);

    if (layoutInfo == null) {
      // if file has not been loaded, try to load it now.
      try {
        layoutInfo = load(resourceID, filename);
        log.info("XMLParser Loaded xml file " + filename);
      } catch (Exception e) {
        log.severe("XMLParser could not parse file: " + filename);
        throw new RuntimeException("XMLParser could not parse file: " + filename);
      }
    }
    assert layoutInfo != null;

    // now the layout file is loaded and we have its LayoutInfo
    return filename;
  }

  public int getRootHash(int resourceID) {
    // retrieve the file name of the layout resource
    return xmlMap.get(resourceID).root.hashCode();
  }

  private static XMLInfo load(int resourceID, String filename) throws ParserConfigurationException,
      SAXException, IOException {
    // parse the document
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    InputStream is = new FileInputStream(filename);
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document dom = builder.parse(is, null);
    Element docRoot = dom.getDocumentElement();

    // create new LayoutInfo entry
    XMLInfo info = new XMLInfo();
    info.root = docRoot;
    info.filename = filename;
    insertXMLInfoInMap(info, info.root);
    xmlMap.put(resourceID, info);
    return info;
  }

  /**
   * Recursively inserts all child nodes into a hash map of all the views in the layout.
   * 
   * @param info
   *          the info on the Layout
   * @param n
   *          the node to insert
   */
  private static void insertXMLInfoInMap(XMLInfo info, Node n) {
    if (n == null) {
      return;
    }
    // put node in hashmap
    info.xmlMap.put(n.hashCode(), n);

    // get the children
    NodeList list = n.getChildNodes();

    // if has children, call insert for each child
    if (list != null && list.getLength() > 0) {
      for (int i = 0; i < list.getLength(); i++) {
        Node child = list.item(i);
        if (child.getNodeType() == Node.ELEMENT_NODE) {
          insertXMLInfoInMap(info, child);
        }
      }
    }
  }

  public String[] getNodeInfo(int hashcode, int resourceID) {
    if (hashcode == -1) {
      log.warning(TAG + ": No view node with hashcode -1");
      return null;
    }

    XMLInfo info = xmlMap.get(resourceID);
    Node n = info.xmlMap.get(hashcode);
    if (n == null) {
      log.warning(TAG + ": Could not find XML node with hashcode " + hashcode);
      return null;
    }

    // we have found the node
    String[] returnVals = { getType(n), Integer.toString(getViewIDFromNode(n)), getName(n),
        Integer.toString(hashcode), getText(n), getListener(n) };
    return returnVals;

  }

  public Object[] getNodeAttributes(int hashcode, int resourceID) {
    if (hashcode == -1) {
      log.warning(TAG + ": No view node with hashcode -1");
      return null;
    }

    XMLInfo info = xmlMap.get(resourceID);
    Node n = info.xmlMap.get(hashcode);
    if (n == null) {
      log.warning(TAG + ": Could not find XML node with hashcode " + hashcode);
      return null;
    }

    NamedNodeMap list = n.getAttributes();
    int len = list.getLength();

    Object[] returnAtt = new Object[len];
    for (int i = 0; i < len; i++) {
      Node attNode = list.item(i);
      String key = attNode.getNodeName();
      String value = attNode.getNodeValue();
      Object valueObject = null;

      // if (value.contains("@"))
      // valueObject = AndroidProjectInfo.get().getValue(value);
      // else
      valueObject = value;

      Object[] atts = { key, valueObject };
      returnAtt[i] = atts;
    }

    return returnAtt;

  }

  private String getListener(Node node) {
    String name = "";
    NamedNodeMap list = node.getAttributes();
    Node n = list.getNamedItem("android:onClick");
    if (n != null) {
      name = n.getNodeValue();
    }

    return name;
  }

  public int[] getChildren(int hashcode, int resourceID) {

    if (hashcode == -1) {
      log.warning(TAG + ": No view node with hashcode -1");
      return null;
    }

    XMLInfo info = xmlMap.get(resourceID);
    Node n = info.xmlMap.get(hashcode);

    if (n == null) {
      log.warning(TAG + ": Could not find XML node with hashcode " + hashcode);
      return null;
    }

    // get the children
    NodeList list = n.getChildNodes();

    ArrayList<Integer> returnList = null;
    // if has children, return children
    if (list != null && list.getLength() > 0) {
      returnList = new ArrayList<Integer>();
      for (int i = 0; i < list.getLength(); i++) {
        Node child = list.item(i);
        if (child.getNodeType() == Node.ELEMENT_NODE) {
          returnList.add(child.hashCode());
        }
      }

      int[] returns = new int[returnList.size()];
      for (int i = 0; i < returnList.size(); i++) {
        returns[i] = returnList.get(i);
      }
      return returns;
    }
    return null;

  }

  public static String getType(Node node) {
    return node.getNodeName();

  }

  public static String getName(Node node) {
    String name = "";
    NamedNodeMap list = node.getAttributes();
    Node n = list.getNamedItem("android:id");
    if (n != null) {
      name = n.getNodeValue().substring(5);

    } else {
      name = node.getNodeName().toLowerCase() + "_" + Integer.toString(node.hashCode());
    }

    return name;
  }

  public static String getText(Node node) {
    String name = "";
    NamedNodeMap list = node.getAttributes();
    Node n = list.getNamedItem("android:text");
    if (n != null) {
      name = n.getNodeValue();
      if (name.startsWith("@string/")) {
        name = AndroidProjectInfo.get().getStringValue(name.substring(8));
      }
    }

    return name;
  }

  public static int getViewIDFromNode(Node node) {
    int id = -1;
    String name = getName(node);
    log.fine("Getting ViewId for: " + name);
    try {
      id = AndroidProjectInfo.get().getRFile().getViewIdForName(name);
    } catch (Exception e) {
      // log.warning("LayoutInflator could not find ViewID for: " + name);
      // generating ID
      return node.hashCode();
    }
    return id;
  }

  private static JPFXMLParser instance = null;

  public static JPFXMLParser getInstance() {
    if (instance == null) {
      instance = new JPFXMLParser();
    }
    return instance;
  }

}