package gov.nasa.jpf.util.event.tree;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.report.ConsolePublisher;
import gov.nasa.jpf.report.Publisher;
import gov.nasa.jpf.report.Statistics;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.util.AndroidStateExtensionListener;
import gov.nasa.jpf.util.StateExtensionClient;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.EventImpl;
import gov.nasa.jpf.util.event.generator.dynamic.XStreamConverter;
import gov.nasa.jpf.util.event.generator.dynamic.XStreamConverter.XStreamConverterException;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.choice.BreakGenerator;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class traverses the AndroidEventTree and backtracks the pointer into the tree. It is also a listener
 * that prints out the tree and a summary about its sequences.
 * 
 * @author heila
 *
 */
public class EventTreeWrapper extends ListenerAdapter implements StateExtensionClient<EventNode> {

  /** The event tree */
  private AndroidEventTree eventTree;
  /** Pointer into the event tree */
  private EventNode current;
  /**
   * Stores the pointer location for each state for backtracking. This is stored as a field to ensure the
   * listener is only registered once.
   */
  private AndroidStateExtensionListener<EventNode> sel;

  /** Stores a list of unique events for each Activity */
  Map<String, List<Event>> activities = new HashMap<String, List<Event>>();

  /** Used to query of state of search from state extension methods. */
  public Search search;

  /** events directory where to put results in */
  String outputDir;

  private boolean includeEnvState = false;

  public EventTreeWrapper(Config config) {
    eventTree = new AndroidEventTree(config);
    includeEnvState = config.getBoolean("event.tree.includeEnvironmentState", false);

    String output = config.getString("jpf-android.output.run", ".");
    try {
      // Assume file does not exist
      Path p = Files.createDirectories(Paths.get(output + File.separator + "event"));
      outputDir = p.toFile().getAbsolutePath();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void addEventToDiscovery(Event event) {
    EventImpl e = (EventImpl) event;
    String windowName = e.getWindowName();
    if (windowName == null) {
      windowName = "default";
    }
    List<Event> eventlist = activities.get(windowName);
    if (eventlist == null) {
      eventlist = new LinkedList<Event>();
      activities.put(windowName, eventlist);
    }
    if (!eventlist.contains(e)) {
      eventlist.add(e);
    }
  }

  public boolean addEvent(Event event) {
    addEventToDiscovery(event);
    int searchID = 0;
    if (includeEnvState)
      searchID = search.getStateId();

    System.out.println("\nEventTree add event : " + event + " to "
        + ((this.current != null) ? this.current.getEvent() : "null"));

    EventNode added = null;
    if (current == null && eventTree.root() == null) {
      // empty tree 1st time
      added = eventTree.setRoot(event);
      // added.stateID = search.getStateId();
      // this.current = eventTree.root();
    } else if (current == null && eventTree.root() != null) {
      added = eventTree.root().addSibling(event, searchID);
    } else {
      added = eventTree.addEvent(current, event, searchID);
    }
    if (added != null) {
      added.stateID = search.getStateId();
      current = added;
    }
    return added != null;
  }

  @Override
  public EventNode getStateExtension() {
    if (!search.isVisitedState()) {
      return this.current;
    } else {
      return sel.getState(search.getStateId());
    }

  }

  @Override
  public void restore(EventNode stateExtension) {
    this.current = stateExtension;
  }

  @Override
  public void registerListener(JPF jpf) {
    if (sel == null) {
      search = jpf.getSearch();
      sel = new AndroidStateExtensionListener<EventNode>(this);
      jpf.addSearchListener(sel);
      jpf.addSearchListener(this);
      jpf.addVMListener(this);
      jpf.addPublisherExtension(ConsolePublisher.class, this);
    }
  }

  public void printTreeSummary() {
    PrintWriter w;
    try {
      w = new PrintWriter(Files.newBufferedWriter(Paths.get(outputDir + File.separator + "summary.txt")));
      w.println("****************** EventTree *****************");
      w.println("Number of Events: " + eventTree.getNumEvents());
      w.println("Number of Branches: " + eventTree.getNumBranches());

      Statistics stats = search.getVM().getJPF().getListenerOfType(Statistics.class);

      w.println("Number of Paths: " + stats.visitedStates);
      w.println("Number of Stopped Paths: " + (numStops));
      w.println("Number of Matched Paths: " + (stats.visitedStates - (numStops)));

      w.println("Event histogram: "
          + Arrays.toString(eventTree.getEventHistogram(eventTree.getMaxDepth() + 1)));
      w.println("**********************************************");
      w.flush();
      w.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void printTree() {
    if (eventTree == null) {
      System.out.println("eventTree == null");
    } else {
      PrintWriter w;
      try {
        w = new PrintWriter(Files.newBufferedWriter(Paths.get(outputDir + File.separator + "eventtree.txt")));
        eventTree.printTree(w);
        w.flush();
        w.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public void printTreeSequences() {
    if (eventTree != null) {
      try {
        PrintWriter[] ws = new PrintWriter[eventTree.getMaxDepth()];
        for (int i = 0; i < ws.length; i++) {
          ws[i] = new PrintWriter(Files.newBufferedWriter(Paths.get(outputDir + File.separator
              + "sequenceLen" + i + ".log")));
        }
        eventTree.printSequences(ws);
        for (int i = 0; i < ws.length; i++) {
          ws[i].flush();
          ws[i].close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

  public void printDiscovery() {
    try {
      PrintWriter w = new PrintWriter(Files.newBufferedWriter(Paths.get(outputDir + File.separator
          + "discovery.txt")));
      int count = 0;
      for (String s : activities.keySet()) {
        w.println(s + ":");
        for (Event e : activities.get(s)) {
          w.println("\t" + e.toString() + "(" + ((EventImpl) e).getWindowName() + ","
              + ((EventImpl) e).getNextWindowName() + ")");
          count++;
        }
      }

      w.println("\nTotal events fired: " + count);
      w.flush();
      w.close();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public void printEventSequence() {
    try {
      if (current != null) {
        PrintWriter w = new PrintWriter(Files.newBufferedWriter(Paths.get(outputDir + File.separator
            + "errorSequence.txt")));
        current.printPath(w);
        w.println();
        w.flush();
        w.close();
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public void publishFinished(Publisher publisher) {
    printTreeSummary();
    printTree();
    printTreeSequences();
    printDiscovery();
    // writeToFile();
    super.publishFinished(publisher);
  }

  @Override
  public void publishPropertyViolation(Publisher publisher) {
    printEventSequence();
  }

  @Override
  public void publishConstraintHit(Publisher publisher) {
    printEventSequence();
    super.publishConstraintHit(publisher);
  }

  @Override
  public void searchConstraintHit(Search search) {
    printEventSequence();
    super.searchConstraintHit(search);
  }

  int numStops = 0;

  @Override
  public void stateAdvanced(Search search) {
    if (current != null) {
      if (search.isEndState() || search.isVisitedState() || search.isErrorState() || search.isDone()
          || search.isIgnoredState()) {
        this.current.addEndState(search.getStateId());

      } else {
        if (search.getVM().getNextChoiceGenerator() instanceof AndroidEventChoiceGenerator) {
          this.current.nextStates.add(search.getStateId());
        }
      }
    }

    if (search.isVisitedState()) {
      ChoiceGenerator<?> cg = search.getVM().getChoiceGenerator();
      if (cg instanceof BreakGenerator) {
        numStops++;
      }
    }
  }

  // this is called when the previous state is visited or end state and we need to stop that path and continue
  // on this new state.
  @Override
  public void stateBacktracked(Search search) {
    super.stateBacktracked(search);
  }

  @Override
  public void stateProcessed(Search search) {
    // called by certain searches
  }

  public boolean reachedMaxDepth() {
    return eventTree.reachedMaxDepth(eventTree.getDepth(current));
  }

  public boolean isEmpty() {
    return eventTree.isEmpty();
  }

  public boolean isFirstEvent() {
    return current == null;
  }

  public void newWindow(String window) {
    ((EventImpl) current.getEvent()).setNextWindowName(window);

  }

  public void writeToXML() {

    XStreamConverter converter;
    try {
      converter = new XStreamConverter();
      File f = new File(outputDir + File.separator + "test.xml");
      f.createNewFile();
      BufferedWriter bw;
      bw = new BufferedWriter(new FileWriter(f));
      bw.write(converter.serialize(eventTree));
      bw.flush();
      bw.close();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (XStreamConverterException e1) {
      e1.printStackTrace();
    }
  }

  public EventNode getCurrentEvent() {
    return current;
  }

  /** Used for testing */
  public String getEventPath() {
    if (current != null) {
      ByteArrayOutputStream output = new ByteArrayOutputStream();
      PrintWriter w = new PrintWriter(output);
      current.printPath(w);
      w.flush();
      return output.toString();
    } else {
      return "";
    }
  }

  /** Used for testing */
  public void unregisterListener(JPF jpf) {
    jpf.removeListener(sel);
    jpf.removeListener(this);
  }
}
