package gov.nasa.jpf.util.event.events;

/**
 * Base class for all events. Currently there is no fields/methods that can be shared by all events so this
 * class provides a skeleton for all event classes.
 * 
 * @author heila
 *
 */
public abstract class EventImpl implements Event {
  private String windowName;
  private String nextWindowName;

  public String getNextWindowName() {
    return nextWindowName;
  }

  public void setNextWindowName(String nextWindowName) {
    this.nextWindowName = nextWindowName;
  }

  /**
   * @return the windowName
   */
  public String getWindowName() {
    return windowName;
  }

  /**
   * @param windowName
   *          the windowName to set
   */
  public void setWindowName(String windowName) {
    this.windowName = windowName;
  }

  @Override
  /**
   * Prints out entire event to enabling us to distinguish between subtle differences in events 
   */
  public abstract String print();

  @Override
  /**
   * Shorter version of event as a string
   */
  public abstract String toString();

  @Override
  /**
   * This is very important since it is used to check if an event is unique in the event tree. It is also used in
   * a hashmap to lookup processors registered for the event type.
   */
  public abstract int hashCode();

  @Override
  /**
   * Java Docs: Note that it is generally necessary to override the hashCode method whenever this method is overridden,
   * so as to maintain the general contract for the hashCode method, which states that equal objects must have 
   * equal hash codes.
   */
  public abstract boolean equals(Object object);

  /**
   * Used if event is expanded
   */
  @Override
  public abstract Object clone() throws CloneNotSupportedException;
}
