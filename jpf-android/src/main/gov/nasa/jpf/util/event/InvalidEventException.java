package gov.nasa.jpf.util.event;

public class InvalidEventException extends Exception {

  public InvalidEventException(String string) {
    super(string);
  }

}
