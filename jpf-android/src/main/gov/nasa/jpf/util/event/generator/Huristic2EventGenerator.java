package gov.nasa.jpf.util.event.generator;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.util.StateExtensionClient;
import gov.nasa.jpf.util.event.EventGenerator;
import gov.nasa.jpf.util.event.EventsProducer;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.EventImpl;
import gov.nasa.jpf.util.event.events.SystemEvent;
import gov.nasa.jpf.util.event.tree.AndroidEventChoiceGenerator;
import gov.nasa.jpf.util.event.tree.EventNode;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.VM;

import java.util.ArrayList;
import java.util.List;

import android.content.ComponentName;
import android.content.Intent;

/**
 * Fires events only once in the event tree
 * 
 * @author heila
 *
 */
public class Huristic2EventGenerator implements EventGenerator {

  public static class State {
    String windowName;
    List<Integer> events;

    public State(String windowName) {
      this.windowName = windowName;
      this.events = new ArrayList<Integer>(10);
    }

    public State(String windowName, int size) {
      this.windowName = windowName;
      this.events = new ArrayList<Integer>(size);
    }

    public boolean addEvent(String event) {
      int hashCode = event.hashCode();
      int index = events.indexOf(hashCode);
      if (index < 0) {
        // new event
        this.events.add(hashCode);
        return true;
      } else {
        return false;
      }
    }

    public Event[] checkEvents(Event[] events) {
      int newLen = 0;
      int len = events.length;

      for (int i = 0; i < len; i++) {
        Event e = events[i];
        if (!this.events.contains(e.toString().hashCode())) {
          newLen++;
        } else {
          events[i] = null;
        }
      }

      // if events have been filtered, construct new array
      if (newLen < len) {
        Event[] ret = new Event[newLen];

        int j = 0;
        for (int i = 0; i < len; i++) {
          if (events[i] != null) {
            ret[j] = events[i];
            j++;
          }
        }
        return ret;
      }
      return events;
    }

    public StateMemento getMemento() {
      StateMemento m = new StateMemento();
      m.windowName = this.windowName;
      m.events = new int[this.events.size()];
      for (int i = 0; i < this.events.size(); i++) {
        m.events[i] = this.events.get(i);
      }
      return m;
    }

    public void restoreMemento(StateMemento memento) {
      for (int i = 0; i < memento.events.length; i++) {
        this.events.add(memento.events[i]);
      }
    }
  }

  public static class StateMemento {
    String windowName;
    int[] events;
  }

  State mState;
  String target;
  VM vm;
  StateListener listener;
  EventsProducer eventProducer;
  DynamicEventGenerator generator;
  boolean dynamicEvents = false;

  public Huristic2EventGenerator(Config config, EventsProducer eventProducer) {
    target = config.getTarget();
    mState = new State("default", 1);
    this.eventProducer = eventProducer;
    dynamicEvents = config.getBoolean("event.heuristic.dynamic", false);
    if (dynamicEvents)
      generator = new DynamicEventGenerator(config);

  }

  public static class StateListener extends ListenerAdapter implements StateExtensionClient<StateMemento> {

    Huristic2EventGenerator mGenerator;

    public StateListener(Huristic2EventGenerator generator) {
      mGenerator = generator;
    }

    @Override
    public StateMemento getStateExtension() {
      return mGenerator.mState.getMemento();
    }

    @Override
    public void choiceGeneratorAdvanced(VM vm, ChoiceGenerator<?> currentCG) {

      if (currentCG instanceof AndroidEventChoiceGenerator) {
        Event e = ((AndroidEventChoiceGenerator) currentCG).getNextChoice();
        if (e != null) {
          if (!mGenerator.mState.windowName.equals(((EventImpl) e).getWindowName())) {
            mGenerator.mState = new State(((EventImpl) e).getWindowName());
          }
        }
      }
    }

    @Override
    public void restore(StateMemento stateExtension) {
      mGenerator.mState = new State(stateExtension.windowName, stateExtension.events.length);
      mGenerator.mState.restoreMemento(stateExtension);
    }

    @Override
    public void registerListener(JPF jpf) {
      jpf.addListener(this);
    }

  }

  @Override
  public Event[] getEvents(MJIEnv env, int objRef, EventsProducer producer) {
    Event[] events = producer.getEvents();

    if (producer.isFirstEvent(env)) {
      return generateFirstEvent();
    } else {
      // processEvents
      EventNode e = eventProducer.getCurrentEvent();
      if (e != null) {
        EventImpl ei = (EventImpl) e.getEvent();
        mState.addEvent(e.getEvent().toString());
      }
      String windowName = producer.getCurrentWindow(env, objRef);
      if (windowName.equals(mState.windowName)) {
        events = mState.checkEvents(events);
      }
      if (dynamicEvents)
        return generator.getEvents(env, objRef, producer, events);
      else
        return events;
    }
  }

  private Event[] generateFirstEvent() {
    // get package and activity of first Activity
    String packageName = target.substring(0, target.lastIndexOf('.'));
    String componentName = target.substring(target.lastIndexOf('.') + 1, target.length());
    Intent startIntent = new Intent();
    startIntent.setComponent(new ComponentName(packageName, componentName));
    startIntent.setAction(Intent.ACTION_MAIN);
    startIntent.addCategory(Intent.CATEGORY_LAUNCHER);
    SystemEvent[] events = { new SystemEvent(startIntent) };
    events[0].setAction("startActivity");
    events[0].setWindowName("default");
    return events;
  }

  @Override
  public void destroy(JPF jpf) {
    if (listener != null) {
      jpf.removeListener(listener);
    }
    if (generator != null) {
      generator.destroy(jpf);
    }
  }

  @Override
  public boolean collectEvents() {
    return true;
  }

  public void registerListener(JPF jpf) {
    this.vm = jpf.getVM();
    listener = new StateListener(this);
    listener.registerListener(jpf);
  }

}
