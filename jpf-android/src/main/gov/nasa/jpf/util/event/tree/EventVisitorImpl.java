package gov.nasa.jpf.util.event.tree;

import java.util.Iterator;

public class EventVisitorImpl implements EventVisitor {
  private int tab = 0;

  @Override
  public void visit(EventNode node) {
    System.out.println(node.toString());
    Iterator<EventNode> it = node.getChildren();
    // store tabs
    int temp = ++tab;
    while (it.hasNext()) {
      printTabs(tab);
      EventNode e = it.next();
      e.accept(this);
      tab = temp;
    }
    printTabs(temp - 1);

  }

  public void printTabs(int num) {
    for (int i = 0; i < num; i++) {
      System.out.print("  ");
    }
  }
}
