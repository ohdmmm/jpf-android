package gov.nasa.jpf.util.event.events;

/**
 * Generic Event representing an action that fires execution of the application.
 * 
 * @author heila
 *
 */
public interface Event {

  public String print();

}
