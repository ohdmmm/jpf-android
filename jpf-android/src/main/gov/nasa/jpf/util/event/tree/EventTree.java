package gov.nasa.jpf.util.event.tree;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.tree.EventNode.EventNodeIterator;

import java.io.PrintWriter;
import java.util.Iterator;

public class EventTree {

	private EventNode root;

	public EventTree() {
	}

	public EventTree(Config config) {
		this(config, null);
	}

	/**
	 * Event must not be null
	 * 
	 * @param r
	 *            the root
	 */
	public EventTree(Config config, Event r) {
		if (r != null) {
			this.root = new EventNode();
			this.root.setEvent(r);
		}
	}

	public int size() {
		return (root != null) ? root.size() : 0;
	}

	public boolean isEmpty() {
		return root == null;
	}

	// public Iterator<EventNode> elements() {
	// return null;
	// }

	public EventNode root() {
		return root;
	}

	public EventNode setRoot(Event event) {
		EventNode e = null;
		if (event == null) {
			this.root = null;
		} else {
			e = new EventNode();
			e.setEvent(event);
			this.root = e;
		}
		return e;
	}

	public EventNode parent(EventNode p) {
		return p.getParent();
	}

	public Iterator<EventNode> children(EventNode p) {
		return p.getChildren();
	}

	public boolean isInternal(EventNode p) {
		return p.isInternal();
	}

	public boolean isExternal(EventNode p) {
		return p.isExternal();
	}

	public boolean isRoot(EventNode p) {
		return p.equals(root);
	}

	public int getDepth(EventNode pointer) {
		int counter = 0;
		while (pointer != null) {
			pointer = pointer.getParent();
			counter++;
		}
		return counter;
	}

  public EventNode addEvent(EventNode current, Event e) {
    if (current == null) {
      throw new NullPointerException("Could not add event " + e.toString() + " to a null current node");
    }
    return current.addChild(e, 0);
  }

  public EventNode addEvent(EventNode current, Event e, int stateID) {
		if (current == null) {
			throw new NullPointerException("Could not add event "
					+ e.toString() + " to a null current node");
		}
    return current.addChild(e, stateID);
	}

	public int printTree(PrintWriter writer) {
		int count = 0;
		if (root != null) {
			EventNodeIterator it = new EventNodeIterator(root);
			while (it.hasNext()) {
				EventNode n = it.next();
				count += n.printTree(writer, 0);
				writer.println();

			}
		} else {
			writer.println("Empty");
		}
		return count;
	}

	public int getNumEvents() {
		int count = 0;
		EventNodeIterator it = new EventNodeIterator(root);
		while (it.hasNext()) {
			EventNode n = it.next();
			count += n.count();
		}
		return count;
	}

	public int getNumBranches() {
		int count = 0;
		// calculate number of end events
		EventNodeIterator it = new EventNodeIterator(root);
		while (it.hasNext()) {
			EventNode n = it.next();
			count += n.countEnd();
		}
		return count;
	}

  public int getNumSequences() {
    int count = 0;
    // calculate number of end events
    EventNodeIterator it = new EventNodeIterator(root);
    while (it.hasNext()) {
      EventNode n = it.next();
      count += n.countEnds();
    }
    return count;
  }

	public int[] getEventHistogram(int depth) {
		int[] hist = new int[depth];

		for (int i = 0; i < depth; i++) {
			hist[i] = 0;
		}

		// calculate number of end events
		if (root != null) {
			EventNodeIterator it = new EventNodeIterator(root);
			while (it.hasNext()) {
				EventNode n = it.next();
				n.hist(hist, 0);
			}
		}
		return hist;
	}

	public void printSequences(PrintWriter[] writers) {
		if (root != null) {
			EventNodeIterator it = new EventNodeIterator(root);
			while (it.hasNext()) {
				EventNode n = it.next();
				n.printSequences(writers, 0);

			}
		} else {
		}
	}

}
