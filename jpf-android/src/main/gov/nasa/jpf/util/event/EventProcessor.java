package gov.nasa.jpf.util.event;

import gov.nasa.jpf.util.event.events.Event;

public interface EventProcessor {

  public Event[] getEvents();

  public void processEvent(Event event) throws InvalidEventException;

}
