package gov.nasa.jpf.util.event;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.FilterFrame;
import gov.nasa.jpf.annotation.NeverBreak;
import gov.nasa.jpf.util.event.events.AndroidEvent;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.EventImpl;
import gov.nasa.jpf.vm.Verify;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.util.Log;

/**
 * This class is the main interface between the input generation service running natively and the Android
 * application environment. Events can be requested by calling the processNextEvent() method. This method will
 * return true if a next event is available and is scheduled to be processed.
 * 
 * @author Heila
 * 
 */
public class AndroidEventProducer {
  public static final String TAG = "AndroidEventProducer";

  /**
   * This field stores the id's of the next event for each Section in the script. The index of the list
   * corresponds to a specific section and the the int value maps to the ID of the next element in the script.
   * The state of the script must be included in the state to ensure that states for which the next element do
   * not match, can not be matched.
   */
  @NeverBreak
  private int[] scriptState = null;

  /**
   * The repeat state stores the counter of each repeat element in the script. The reason we need to store
   * this as part of the state is that we need backtrack the state of each repeat in the case of
   * nondetermisitic application execution as well as that it forms part of the scriptstate that needs to be
   * used for state-matching.
   */
  @NeverBreak
  private int[] repeatState = null;

  // Maps the type of an event to the processor that processed it. Only one
  // processor per event. But a
  // processor can be mapped to many events that it can handle.
  @NeverBreak
  @FilterField
  Map<Class<?>, EventProcessor> eventMappings = new HashMap<Class<?>, EventProcessor>();

  // EventProcessors keeps a unique set of processors that must be queried for
  // events
  @NeverBreak
  @FilterField
  List<EventProcessor> eventProcessors = new LinkedList<EventProcessor>();

  public AndroidEventProducer() {
    setup();
  }

  public native boolean collectEvents();

  /**
   * Setup event generators from config
   */
  public native void setup();

  /**
   * Reset Event generators (start fresh for testing mainly)
   */
  public static native void reset();

  @NeverBreak
  @FilterField
  Event[] events = null;
  @NeverBreak
  @FilterField
  Event event = null;

  /**
   * the main interface to be used from clients (test drivers)
   * 
   * @return false if there was no next event (i.e. this is the end of a trace)
   * @throws InvalidEventException
   */
  @FilterFrame
  public int processNextEvent() {
    try {
      // check if the current event generator requires event collection
      if (collectEvents()) {
        events = getEvents();
      }
      // AndroidStateComparator.markState("Android", 15, 7);

      // generate (choose) an event to process
      event = getNextEvent(events);

      // dispatch event to event processor
      return processEvent(event);

    } catch (Exception e) {
      // problem retrieving/processing event
      throw new RuntimeException("AndroidEventProcessor: Could not produce next event.", e);
    }
  }

  @FilterField
  @NeverBreak
  AndroidEvent e = new AndroidEvent();
  /**
   * Dispatches event to EventProducers registered to receive the eventType
   * 
   * @param event
   *          The event that will be processed.
   * @return true if the looper should continue processing events
   * @throws InvalidEventException
   */
  public int processEvent(Event event) throws InvalidEventException {
    boolean delivered = false;

    if (event == null) {
      // event was null - this indicates that no more events are available (stop
      // searching)
      Log.w(TAG, "Event Generation returned NULL event -- stop exploring");
      return 0;

    } else if (e.equals(event)) {
      Log.w(TAG,
          "Event Generation returned AndroidEvent.DEFAULT -- stop exploring since this bytecode will be rescheduled");
      Verify.ignoreIf(true);
      return 1; // TODO was true

    } else {
      Log.i(TAG, "Processing event " + event.print());

      for (Entry<Class<?>, EventProcessor> entry : eventMappings.entrySet()) {
        Class<?> className = entry.getKey();
        if (className.isInstance(event)) { // compare types
          Log.i(TAG, "Event dispatched to " + entry.getValue().getClass().getSimpleName() + ".");
          entry.getValue().processEvent(event);
          delivered = true;
        }
      }
    }

    if (!delivered) {
      Log.w(TAG, "No EventProcessor found to process event!");

    }
    return 2;
  }

  /**
   * Queries all registered EventProcessors for available events.
   * 
   * @return
   */
  public Event[] getEvents() {

    String windowName = getCurrentWindowName();
    List<Event> events = new LinkedList<Event>();
    if (!windowName.equals("default")) {
      // ensure we only query each processor once event if registered for multiple
      // event types
      for (EventProcessor processor : eventProcessors) {
        Event[] e = processor.getEvents();
        for (Event i : e) {
          ((EventImpl) i).setWindowName(windowName);
          events.add(i);
        }
      }
    }
    if (events.size() == 0) {
      Log.w(TAG, "No Events could be collected from EventProcessors!");
      return new Event[0];
    } else {
      Log.i(TAG, events.size() + " Events collected from EventProcessors!");
      Event[] result = new Event[events.size()];
      return events.toArray(result);
    }

  }

  public native String getCurrentWindowName();

  /**
   * Filters and processes currently available events and returns a single event to process
   * 
   * @param event
   * @return
   */
  public native Event getNextEvent(Event event[]);

  /**
   * Registers a services to process events of type eventType. Services can be registered for multiple types
   * of events.
   * 
   * @param eventType
   * @param eventProcessor
   */
  public void registerService(Class<?> eventType, EventProcessor eventProcessor) {
    eventMappings.put(eventType, eventProcessor);
    if (!eventProcessors.contains(eventProcessor)) {
      // ensure we only query each processor once
      eventProcessors.add(eventProcessor);
    }
  }

  // --- for testing and debugging purposes

  /**
   * Used for testing. Does not create an AndroidEventChoiceGenerator for each event.
   * 
   * @return
   */
  public native String processNextScriptEvent();

  public native String getEventName();

  // public native boolean checkPath();
  //
  // public native boolean isCompletelyCovered();

  public native String getEventTreeString();

  /**
   * For testing to expand event to multiple events using logs
   * 
   * @param classname
   * @param method_name
   * @param method_signature
   * @param params
   * @return
   */
  public native Object[] getDynamicReturnValues(String classname, String method_name,
                                                String method_signature, Object[] params);

}
