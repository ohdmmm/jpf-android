package gov.nasa.jpf.util.script.visitor;

import gov.nasa.jpf.util.event.EventForest;
import gov.nasa.jpf.util.script.impl.ScriptElement;
import gov.nasa.jpf.util.script.impl.ScriptElementContainer;
import gov.nasa.jpf.util.script.impl.Visitor;

/**
 * This Visitor is used to traverse the tree of ScriptElements that represents the script and build a new tree
 * of Tokens which can be traversed to print out each of the exapnded scripts.
 * 
 * @author Heila Botha
 * 
 */
public  class TranslateVisitor implements Visitor {

  boolean mRepeat = false;

  /** Stores the string representation of the script built by traversing the tree of script elements */
  protected StringBuilder stringScript;

  protected StringBuilder outputBuilder;

  public void setRepeat(boolean expand) {
    this.mRepeat = expand;
  }
  

  @Override
  public void visit(ScriptElement element){
    
  }

  public void visit(ScriptElementContainer element){
    
  }

  /**
   * Used to write out a single expanded script to either a new file or append it to a global buffer.
   * 
   * @param out
   *          the Script as a String
   * @param numTraversal
   *          the number of the current traversal (This will always be one in the case of random of number
   *          expand visitor)
   */
  protected void writeScript(String out, int numTraversal) {
    if (outputBuilder == null) {
      outputBuilder = new StringBuilder();
    }
    outputBuilder.append("#Traversal #" + (numTraversal + 1) + ":\n" + out );

  }

  /**
   * Counts the number of children in the LinkList of ScriptElements starting with child.
   * 
   * @param child
   * @return
   */
  protected static int getLength(ScriptElement child) {
    int len = 0;
    while (child != null) {
      len++;
      child = child.getNextSibling();
    }
    return len;
  }

  /**
   * Returns the expanded scripts as one long string
   * 
   * @return
   */
  public String getScriptsOutput() {
    return outputBuilder.toString();
  }


  public EventForest getEventForest() {
    // TODO Auto-generated method stub
    return null;
  }

}
