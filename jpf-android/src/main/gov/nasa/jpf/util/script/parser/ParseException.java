package gov.nasa.jpf.util.script.parser;

/**
 * 
 * @author Heila <heila@ml.sun.ac.za>
 * 
 */
public class ParseException extends Exception {

  private static final long serialVersionUID = 6316278327475690767L;

  private final String message;

  /**
   * Sole constructor
   * 
   * @param msg
   *          error message.
   */
  public ParseException(String msg) {
    message = new String(msg);
  }

  public ParseException(String msg, int linenumber) {
    message = new String("Error on line " + linenumber + ": " + msg);
  }

  public String getMessage() {
    return new String(message);
  }

}
