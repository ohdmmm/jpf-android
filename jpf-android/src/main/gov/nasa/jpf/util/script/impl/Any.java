package gov.nasa.jpf.util.script.impl;


public class Any extends ScriptElementContainer {

  public Any(int linenumber, ScriptElementContainer parent) {
    super(linenumber, parent);
  }

  public static class Choice extends ScriptElementContainer {

    public Choice(int linenumber, ScriptElementContainer parent) {
      super(linenumber, parent);
    }

  }

  
  public void setParent(ScriptElementContainer parent){
    this.parent = parent;
  }
  
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + getLine();
//    result = prime * result + this.toString().hashCode();
    result = prime * result + ((getNextSibling() == null) ? 0 : getNextSibling().toString().hashCode());
    result = prime * result + ((getParent() == null) ? 0 : getParent().toString().hashCode());
    return result;
  }
}
