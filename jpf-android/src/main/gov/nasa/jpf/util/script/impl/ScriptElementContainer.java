package gov.nasa.jpf.util.script.impl;


/**
 * This class represents {@link ScriptElement script elements} that can contain other script elements. It
 * stores a linked list of children of the element and visits each of the children when the AST is traversed
 * by a visitor.
 * 
 * 
 * @author Heila <heila@ml.sun.ac.za>
 * 
 */
public class ScriptElementContainer extends ScriptElement {

  /** Linked list of children */
  private ScriptElement children;

  public ScriptElementContainer(int linenumber, ScriptElementContainer parent) {
    super(linenumber, parent);
  }

  public ScriptElement getChildren() {
    return children;
  }

  public void addChild(ScriptElement element) {

    ScriptElement pointer = children;

    if (pointer != null) {
      while (pointer.getNextSibling() != null) {
        pointer = pointer.getNextSibling();
      }
      pointer.setNextSibling(element);
    } else {
      children = element;
    }

  }

  public void replaceChild(Event e1, Any e2) {
    ScriptElement pointer = children;

    if (pointer != null) {

      if (pointer instanceof Event && pointer.equals(e1)) {
        e2.setNextSibling(pointer.getNextSibling());
        children = e2;
      } else {
        while (pointer.getNextSibling() != null) {
          if (pointer.getNextSibling().equals(e1)) {
            e2.setNextSibling(pointer.getNextSibling().getNextSibling());
            pointer.setNextSibling(e2);
            return;
          }
          pointer = pointer.getNextSibling();
        }

      }
    }

  }

  /**
   * 
   * This is used to traverse the Abstract Syntax Tree (AST) of the script and visit each node of the tree.
   * 
   * @param v
   * @throws Exception
   */
  public void accept(Visitor v) {
    v.visit(this);
  }

}
