package gov.nasa.jpf.util.script.parser;

public class Token {

  private TokenType type;
  private String id;
  private Double value = null;
  private int linenumber = 0;

  public Token(TokenType type) {
    this.type = type;
  }

  public Token(TokenType type, String id) {
    this.type = type;
    this.id = id;
  }

  public Token(TokenType type, double value) {
    this.type = type;
    this.value = value;
  }

  public TokenType getType() {
    return type;
  }

  public String getId() {
    return id;
  }

  public double getValue() {
    return value;
  }

  public int getLinenumber() {
    return linenumber;
  }

  public void setLinenumber(int linenumber) {
    this.linenumber = linenumber;
  }

  @Override
  public String toString() {
    return "Token [type=" + type.name() + ((id != null) ? ", id=" + id : "")
        + ((value != null) ? ", value=" + value : "") + ", linenumber=" + linenumber + "]";
  }

}
