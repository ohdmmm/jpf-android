package gov.nasa.jpf.util.script.parser;

import java.io.IOException;
import java.io.Reader;
import java.io.StreamTokenizer;

public class ScriptScanner {

  private StreamTokenizer tokenizer;

  public ScriptScanner(Reader r) {

    setupScanner(r);

    try {
      tokenizer.nextToken();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private StreamTokenizer setupScanner(Reader r) {

    tokenizer = new StreamTokenizer(r);

    // enable # comment
    tokenizer.commentChar('#');
    tokenizer.quoteChar('"');

    tokenizer.eolIsSignificant(false);

    //tokenizer.lowerCaseMode(true);

    tokenizer.parseNumbers();

    tokenizer.quoteChar('"');

    tokenizer.slashSlashComments(true);
    tokenizer.slashStarComments(true);

    // ID's
    tokenizer.wordChars('0', '9');

    tokenizer.wordChars('a', 'Z');
    tokenizer.wordChars('_', '_');
    tokenizer.wordChars('$', '$');
    tokenizer.wordChars('@', '@');
    tokenizer.wordChars('.', '.');

    // Expanding sequences
    tokenizer.wordChars('<', '<');
    tokenizer.wordChars('[', '[');
    tokenizer.wordChars(']', ']');
    tokenizer.wordChars('|', '|');
    tokenizer.wordChars('>', '>');

    tokenizer.whitespaceChars(',', ',');
    tokenizer.whitespaceChars(';', ';');

    //tokenizer.wordChars('"', '"');

//    tokenizer.ordinaryChars('0', '9');

    return tokenizer;

  }

  public Token getToken() throws ParseException {

    Token token = null;

    if (tokenizer.ttype == StreamTokenizer.TT_WORD) {

      token = TokenType.getToken(tokenizer.sval);
      token.setLinenumber(tokenizer.lineno());

    } else if (tokenizer.ttype == StreamTokenizer.TT_NUMBER) {

      token = new Token(TokenType.NUMBER, tokenizer.nval);
      token.setLinenumber(tokenizer.lineno());

    } else if (tokenizer.ttype == StreamTokenizer.TT_EOF) {
      token = new Token(TokenType.EOF, tokenizer.nval);
      token.setLinenumber(tokenizer.lineno());
    } else if (tokenizer.ttype == '"') {
      token = TokenType.getToken(tokenizer.sval);
      token.setLinenumber(tokenizer.lineno());
    } else {
      token = TokenType.getToken(String.valueOf(((char) tokenizer.ttype)));
      token.setLinenumber(tokenizer.lineno());
    }
    try {
      tokenizer.nextToken();
    } catch (IOException e) {
      throw new ParseException("Could not read  next token");
    }
    return token;
  }

}
