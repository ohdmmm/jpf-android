package gov.nasa.jpf.util.script.impl;


public class Repeat extends ScriptElementContainer {

  int num = 0;

  int count = 0;

  public Repeat(int linenumber, ScriptElementContainer parent) {
    super(linenumber, parent);
  }

  public int getNumber() {
    return this.num;
  }

  public void setNumber(int number) {
    this.num = number;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public boolean done() {
    return num <= count;
  }
  
  public void reset(){
    count = 0;
  }
  
  public void inc(){
    count++;
  }

}
