package gov.nasa.jpf.util.script.visitor;

import gov.nasa.jpf.util.script.impl.Any;
import gov.nasa.jpf.util.script.impl.Any.Choice;
import gov.nasa.jpf.util.script.impl.Event;
import gov.nasa.jpf.util.script.impl.Repeat;
import gov.nasa.jpf.util.script.impl.Script;
import gov.nasa.jpf.util.script.impl.ScriptElement;
import gov.nasa.jpf.util.script.impl.ScriptElementContainer;
import gov.nasa.jpf.util.script.impl.Section;
import gov.nasa.jpf.util.script.tools.StringExpander;

import java.util.List;
import java.util.Stack;

/**
 * This ExpandVisitor is used to traverse the tree of ScriptElements that represents the script and write out
 * all paths in the script that can be traversed to form a new script.
 * 
 * @author Heila Botha
 * 
 */
public class AllExpandVisitor extends ExpandVisitor {

  /** Stores the FILO queue of ANY script elements that need to be expanded */
  Stack<AnyNode> anyStack = new Stack<AnyNode>();

  int numTraversal = 0;

  private static class AnyNode {
    Any any;
    int choice;
    int numTraversal;

    public AnyNode(Any any) {
      this.any = any;
      this.choice = 0;
      this.numTraversal = -1;
    }

    public AnyNode(Any any2, int numtraversal) {
      this.any = any2;
      this.choice = 0;
      this.numTraversal = numtraversal;
    }

    @Override
    public boolean equals(Object obj) {
      return any.hashCode() == ((AnyNode) obj).any.hashCode();
    }

    public boolean finished() {
      return choice >= getLength(any.getChildren()) - 1;
    }

  }

  /**
   * Concatenate a ScriptElement to the expanded script. TODO what if this element is a contracted event, will
   * this be handled by parser?
   * 
   */
  public void visit(ScriptElement element) {

    if (element instanceof Event) {
      Event e = (Event) element;

      String eventString = e.getTarget() + "." + e.getAction() + "(";
      if (e.getParams() != null) {
        for (Object s : e.getParams()) {
          if (s != null)
            if (s instanceof String) {
              eventString += "\"" + s.toString() + "\" , ";
            } else {
              eventString += s.toString() + ", ";
            }
        }
        eventString = eventString.substring(0, eventString.length() - 3);
      }

      eventString += ")";
      StringExpander se = new StringExpander(eventString);

      List<String> ex = se.expand();

      if (ex.size() > 1) {
        Any any = new Any(e.getLine(), null);

        for (String s : ex) {
          parseEvent(any, s);
        }
        e.getParent().replaceChild(e, any);
        any.setParent(e.getParent());
        this.visit((ScriptElementContainer) any);

      } else {
        String elementString = element.toString() + "\n";
        stringScript.append(elementString);
      }

    }
    
  }

  private Choice parseEvent(Any parent, String s) {

    Choice c = new Choice(parent.getLine(), parent);
    Event e = new Event(parent.getLine(), c);

    // Divide the string into the target.action | params
    String[] event = s.split("\\(");

    // Get the target and action (target may be empty)
    String[] ev = event[0].split("\\.");
    e.setTarget(ev[0]);
    e.setAction(ev[1]);

    // Parse parameters

    event[1] = event[1].substring(0, event[1].length() - 1);
    if (event[1] != null && !event[1].equals("null")) {
      for (String p : event[1].split(",")) {
        e.addParam(p);
      }
    }
    return c;
  }

  /**
   * Visit a ScriptElementContainer and print is out as necessary
   */
  public void visit(ScriptElementContainer element) {
    if (element instanceof Script) {
      visitScript((Script) element);
    } else if (element instanceof Section) {
      visitSection((Section) element);
    } else if (element instanceof Repeat) {
      visitRepeat((Repeat) element);
    } else if (element instanceof Any) {
      visitAny((Any) element);
    } else if (element instanceof Any.Choice) {
      visitChoice((Choice) element);
    } else {
      throw new RuntimeException();
    }
  }

  private void visitScript(Script script) {

    do {
      stringScript = new StringBuilder();
      stringScript.append("script {\n");

      // contains many sections
      visitChildren(script);

      stringScript.append("}\n");

      writeScript(stringScript.toString(), numTraversal);
      numTraversal++;
    } while (moreChoices());

  }

  private boolean moreChoices() {
    if (anyStack != null && !anyStack.isEmpty()) {
      while (!anyStack.isEmpty()) {
        AnyNode node = anyStack.peek();

        if (!node.finished()) {
          return true;
        } else {
          anyStack.pop();
        }
      }
    }
    return false;
  }

  private void visitSection(Section section) {
    // contains many ScriptElements and ScriptElementContainers

    // print out Section declaration
    stringScript.append("section " + section.getName() + " {\n");

    // contains many ScriptElements and ScriptElementContainers
    visitChildren(section);

    // apppend closing brace of section
    stringScript.append("}\n");

  }

  private void visitAny(Any any) {
    int choice = -1;

    if (!anyStack.isEmpty()) {

      AnyNode peek = anyStack.peek();

      // if this any is on top and has not been incremented in this traversal
      if (peek.any.equals(any)) {

        if (peek.numTraversal != numTraversal) {
          // get next choice
          peek.choice++;
          choice = peek.choice;

          // if this is last choice
          if (choice >= getLength(any.getChildren()) - 1) {
            // pop any from stack
            // anyStack.pop();
          }
          peek.numTraversal = numTraversal;
        }
      } else {
        int index = anyStack.indexOf(new AnyNode(any));
        // if hashmap contains any
        if (index != -1) {
          // any is not being incremented so just wait and print current choice
          choice = anyStack.get(index).choice;
        }
      }
    }

    // any has not been added to the stack so push it
    if (choice == -1) {
      anyStack.push(new AnyNode(any, numTraversal));
      choice = 0;
    }

    // get the specific child
    int count = 0;

    Choice c = (Choice) any.getChildren();

    // move tp the choice
    while (c != null && count < choice) {
      c = (Choice) c.getNextSibling();
      count++;
    }

    // visit choice
    visitChildren(c);

  }

  private void visitRepeat(Repeat repeat) {

    if (mRepeat == false) {
      // append x number of the events
      for (int i = 0; i < ((Repeat) repeat).getNumber(); i++) {
        visitChildren(repeat);
      }
    } else {
      stringScript.append("repeat " + repeat.getNumber() + " {\n");

      // contains many scriptelements
      visitChildren(repeat);

      stringScript.append("}\n");
    }
  }

  private void visitChoice(Choice choices) {
    visitChildren(choices);
  }

  private void visitChildren(ScriptElementContainer parent) {

    ScriptElement element = parent.getChildren();
    while (element != null) {
      if (element instanceof ScriptElementContainer)
        this.visit((ScriptElementContainer) element);
      else
        this.visit((ScriptElement) element);
      element = element.getNextSibling();

    }

  }

}
