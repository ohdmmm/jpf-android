package gov.nasa.jpf.util.script.visitor;

import gov.nasa.jpf.util.script.impl.Any;
import gov.nasa.jpf.util.script.impl.Any.Choice;
import gov.nasa.jpf.util.script.impl.Repeat;
import gov.nasa.jpf.util.script.impl.Script;
import gov.nasa.jpf.util.script.impl.ScriptElement;
import gov.nasa.jpf.util.script.impl.ScriptElementContainer;
import gov.nasa.jpf.util.script.impl.Section;

import java.util.Random;

public class RandomExpandVisitor extends ExpandVisitor {

  Random r = new Random();

  /**
   * Concatenate a ScriptElement to the expanded script. TODO what if this element is a contracted event, will
   * this be handled by parser?
   * 
   */
  public void visit(ScriptElement element) {
    String elementString = element.toString() + "\n";
    stringScript.append(elementString);

  }

  /**
   * Visit a ScriptElementContainer and print is out as necessary
   */
  public void visit(ScriptElementContainer element) {
    if (element instanceof Script) {
      visitScript((Script) element);
    } else if (element instanceof Section) {
      visitSection((Section) element);
    } else if (element instanceof Repeat) {
      visitRepeat((Repeat) element);
    } else if (element instanceof Any) {
      visitAny((Any) element);
    } else if (element instanceof Any.Choice) {
      visitChoice((Choice) element);
    } else {
      throw new RuntimeException();
    }
  }

  private void visitScript(Script script) {

    stringScript = new StringBuilder();
    stringScript.append("script {\n");

    // contains many sections
    visitChildren(script);

    stringScript.append("}\n");

    writeScript(stringScript.toString(), 1);
  }

  private void visitSection(Section section) {
    // contains many ScriptElements and ScriptElementContainers

    // print out Section declaration
    stringScript.append("section " + section.getName() + " {\n");

    // contains many ScriptElements and ScriptElementContainers
    visitChildren(section);

    // apppend closing brace of section
    stringScript.append("}\n");

  }

  private void visitAny(Any any) {
    ScriptElement child = any.getChildren();

    // choose a random child
    int len = getLength(child);

    int random = r.nextInt(len - 1);

    // move to the child
    int count = 0;
    while (count < random) {
      child = child.getNextSibling();
      count++;
    }

    // visit child
    if (child instanceof ScriptElementContainer)
      this.visit((ScriptElementContainer) child);
    else
      this.visit((ScriptElement) child);

  }

  private void visitRepeat(Repeat repeat) {

    if (mRepeat == false) {
      // append x number of the events
      for (int i = 0; i < ((Repeat) repeat).getNumber(); i++) {
        visitChildren(repeat);
      }
    } else {
      stringScript.append("repeat " + repeat.getNumber() + " {\n");

      // contains many scriptelements
      visitChildren(repeat);

      stringScript.append("}\n");
    }
  }

  private void visitChoice(Choice choices) {
    visitChildren(choices);
  }

  private void visitChildren(ScriptElementContainer parent) {

    ScriptElement element = parent.getChildren();
    while (element != null) {
      if (element instanceof ScriptElementContainer)
        this.visit((ScriptElementContainer) element);
      else
        this.visit((ScriptElement) element);
      element = element.getNextSibling();

    }

  }
}
