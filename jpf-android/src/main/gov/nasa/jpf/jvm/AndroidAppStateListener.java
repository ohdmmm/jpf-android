package gov.nasa.jpf.jvm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.util.StateExtensionClient;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.tree.AndroidEventChoiceGenerator;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Logs the state for debugging purposes
 * 
 * @author Heila van der Merwe
 * 
 */
public class AndroidAppStateListener extends ListenerAdapter implements StateExtensionClient<String> {

  StringBuilder out = new StringBuilder();

  public AndroidAppStateListener(Config c) {
    try {
      Files.createDirectories(Paths.get("path-app-logs"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void searchStarted(Search search) {
    // this.registerListener(search.getVM().getJPF());

  }

  @Override
  public void choiceGeneratorAdvanced(VM vm, ChoiceGenerator<?> currentCG) {
    if (currentCG instanceof AndroidEventChoiceGenerator) {
      Event e = ((AndroidEventChoiceGenerator) currentCG).getNextChoice();
      out.append("\n" + e.print() + "\n");

    }
  }

  @Override
  public void methodEntered(VM vm, ThreadInfo currentThread, MethodInfo enteredMethod) {
    if (enteredMethod.getFullName().startsWith("com.mendhak.gpslogger")
        && !enteredMethod.getFullName().contains("$VRi()") && !enteredMethod.getFullName().contains("clinit")) {
      // || enteredMethod.getFullName().startsWith("android.app.Activity")
      // || enteredMethod.getFullName().startsWith("android.app.Service")) {
      out.append(enteredMethod.getFullName() + "\n");
    }
  }

  @Override
  public void stateAdvanced(Search search) {
    if (search.isErrorState()) {
      // end of the branch
      File f = new File("path-app-logs/path" + search.getStateId() + ".log");
      f.getParentFile().mkdir();
      try {
        f.createNewFile();
        boolean b = f.canWrite();
        PrintWriter w = new PrintWriter(f);
        w.print(out.toString());
        w.flush();
        w.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public String getStateExtension() {
    return out.toString();
  }

  @Override
  public void restore(String stateExtension) {
    out = new StringBuilder(stateExtension);

  }

  @Override
  public void registerListener(JPF jpf) {
    // StateExtensionListener<String> sel = new StateExtensionListener<String>(this);
    // jpf.addSearchListener(sel);
    // jpf.addSearchListener(this);
    // jpf.addVMListener(this);
  }

}