package gov.nasa.jpf.jvm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.StaticElementInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import nhandler.conversion.ConversionException;
import nhandler.conversion.ConverterBase;
import nhandler.conversion.jpf2jvm.JPF2JVMConverter;

import org.apache.commons.io.FileUtils;

import com.vladium.emma.data.mergeCommand;
import com.vladium.emma.report.reportCommand;
import com.vladium.emma.rt.RT;

/**
 * Writes out the coverage data collected at the end of each branch to the actual RT.java class of Emma. The
 * coverage.ec file calculated for each branch is then moved into the coverage folder the project directory.
 * The the end of search, these files are merged to calculate the combined coverage over all branches and a
 * report is generated in coverage/index.html.
 * 
 * @author Heila van der Merwe
 * 
 */
public class EmmaListener extends ListenerAdapter {

  private static Logger logger = JPF.getLogger(EmmaListener.class.getName());
  private static final String TAG = EmmaListener.class.getSimpleName();

  File coverageTotal;
  String projectPath;
  String outputPath;

  public EmmaListener(Config config) {
    projectPath = config.getString("projectpath", ".");
    outputPath = config.getString("jpf-android.output.run", ".");
  }

  @Override
  public void searchStarted(Search search) {
    coverageTotal = new File(outputPath + File.separator + "coverageTotal.ec");

    File folder = new File(outputPath + File.separator + "coverage");
    if (!folder.exists()) {
      folder.mkdir();
    }
  }

  /**
   * Listen for end of branch and then dump current branch's coverage data to file & merge into.
   */
  @Override
  public void stateAdvanced(Search search) {
    if (search.isEndState() || search.isErrorState() || search.isVisitedState() || search.isIgnoredState()
        || search.isDone()) {
      logger.info(TAG + ": State advanced and reached end of branch, "
          + ((search.isEndState()) ? "Reached end state." : "")
          + ((search.isErrorState()) ? "Reached error state." : "")
          + ((search.isVisitedState()) ? "Reached visited state." + search.getStateId() : ""));
    dumpBranchCoverage(search);
    }
  }

  /**
   * Collect the coverage data collected by the RT.java class and dump it to a file so that it can be merged
   * at the end of search.
   * 
   * @param search
   */
  protected void dumpBranchCoverage(Search search) {

    // delete file and start clean
    File covFile = new File("coverage.ec");
    if (covFile.exists() && covFile.isFile()) {
      try {
        FileUtils.forceDelete(covFile);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    // Get reference to RT.java class
    ClassInfo info = ClassLoaderInfo.getCurrentResolvedClassInfo("com.vladium.emma.rt.RT");
    if (info == null) {
      logger.log(Level.SEVERE, "EmmaListener: Could not resolve RT.java class.");
      return;
    }
    StaticElementInfo sei = info.getStaticElementInfo();
    if (sei == null) {
      logger.log(Level.SEVERE, "EmmaListener: Could not resolve RT.java class.");
      return;
    }

    int mapRef = sei.getReferenceField("c");
    if (mapRef != MJIEnv.NULL && search.getVM() != null && search.getVM().getCurrentThread() != null) {
      MJIEnv env = search.getVM().getCurrentThread().getEnv();
      try {
        ConverterBase.reset(env);

        @SuppressWarnings("unchecked")
        Map<String, Object[]> m = (Map<String, Object[]>) JPF2JVMConverter.obtainJVMObj(mapRef, env);
        for (Object[] coverage : m.values()) {
          RT.r((boolean[][]) coverage[0], (String) coverage[1], (long) coverage[2]);
        }

      } catch (ConversionException e) {
        logger.log(Level.SEVERE, "EmmaListener: Could not retrieve coverage information for the branch.");
        e.printStackTrace(); // make sure we don't miss this
        search.terminate();
      }

    } else {
      logger.log(Level.SEVERE, TAG + ": Could not retrieve map \"c\" from RT.java.");
      System.out.println(">>>>>>>>>>COVERAGE MAY BE INCORRECT<<<<<<<<<<<<" + search.getVM());
      search.terminate();
    }

    try {

      // dump the coverage data to a file coverage.ec
      dumpCoverageToFile();

    } catch (IOException e) {
      logger.log(Level.SEVERE, "EmmaListener: Could not dump coverage data to file coverage.ec");
      e.printStackTrace(); // make sure we don't miss this
      search.terminate();
    }
    RT.reset(true, true);
  }

  public void dumpCoverageToFile() throws IOException {
    logger.log(Level.INFO, TAG + ": >>>>>>>>>dumpCoverageToFile<<<<<<<<<<<<");

    File covFile = new File("coverage.ec");

    RT.dumpCoverageData(covFile, false, true);

    if (covFile.exists()) {
      if (!coverageTotal.exists()) {
        try {
          Path p = covFile.toPath();
          coverageTotal.createNewFile();
          Files.copy(p, coverageTotal.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
          logger.log(Level.SEVERE, TAG + ": Could not move data from coverage.ec to " + "coverageTotal.ec");
          e.printStackTrace();
        }
      } else {
        // merge coverage.ec to coverageTotal.ec
        merge(covFile, coverageTotal);
      }
    } else {
      logger.log(Level.SEVERE, TAG
          + ": Coverage file coverage.ec already exists, could not dump coverage data");
    }
  }

  /**
   * Merges rawData into aggRawData
   * 
   * @param rawData
   * @param aggRawData
   * @throws FileNotFoundException
   */
  protected void merge(File rawData, File aggRawData) {
    String[] mergeArgs = new String[] { "-in", rawData.getAbsolutePath(), "-in",
        aggRawData.getAbsolutePath(), "-in", projectPath + "/bin/coverage.em,", "-out",
        aggRawData.getAbsolutePath() };
    mergeCommand merge = new mergeCommand("emma merge", mergeArgs);
    merge.run();
  }

  /**
   * Generate report using coverage.ec and covergae.em files in root dir of project.
   * 
   */
  protected void report() {
    String[] reportArgs = new String[] { "-r", "html", "-in", projectPath + "/bin/coverage.em", "-in",
        outputPath + File.separator + "coverageTotal.ec", "-sp", projectPath + "/src",
        "-Dreport.html.out.file", outputPath + File.separator + "coverage/index.html" };
    reportCommand reportCom = new reportCommand("emma report", reportArgs);
    reportCom.run();
  }

  @Override
  public void searchFinished(Search search) {
    dumpBranchCoverage(search);
    report();
    super.searchFinished(search);
  }

  /**
   * This is a callback from JPF when a property has been violated. We use it here to dump this last (current
   * branch's) data to a file and then merge all coverage data and generate a report.
   */
  @Override
  public void propertyViolated(Search search) {
    dumpBranchCoverage(search);
    super.propertyViolated(search);
  }

}