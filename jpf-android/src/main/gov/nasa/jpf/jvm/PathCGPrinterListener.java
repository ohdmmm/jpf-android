package gov.nasa.jpf.jvm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.BooleanChoiceGenerator;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.TypedObjectSetChoice;
import gov.nasa.jpf.vm.choice.IntIntervalGenerator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 
 * Prints all the CGs in of the branch. Since AndroidVerify cache choices, we can assume that a choice will
 * only be made on ce in a branch and thus can be collected at end of brnach.
 * 
 * @author Heila van der Merwe
 * 
 */
public class PathCGPrinterListener extends ListenerAdapter {

  String outputDir;

  public PathCGPrinterListener(Config conf) {
    String output = conf.getString("jpf-android.output.run", ".");
    try {
      Path p = Files.createDirectories(Paths.get(output + File.separator + "choices"));
      outputDir = p.toFile().getAbsolutePath();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void stateAdvanced(Search search) {
    if (search.isEndState() || search.isErrorState() || (search.isVisitedState())) {

      File f = new File(outputDir + File.separator + search.getStateId() + ".log");
      try {
        f.createNewFile();
        PrintWriter w = new PrintWriter(f);
        printCGs(w, search.getVM().getChoiceGeneratorsOfType(BooleanChoiceGenerator.class));
        printCGs(w, search.getVM().getChoiceGeneratorsOfType(IntIntervalGenerator.class));
        printCGs(w, search.getVM().getChoiceGeneratorsOfType(TypedObjectSetChoice.class));
        w.flush();
        w.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public void printCGs(PrintWriter pw, ChoiceGenerator<?>[] allcgs) {
    for (ChoiceGenerator<?> cg : allcgs) {
      pw.println(cg.toString());
    }
    pw.flush();
  }

}
