package gov.nasa.jpf.jvm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

/**
 * Logs the state for debugging purposes
 * 
 * @author Heila van der Merwe
 * 
 */
public class SearchStateListener extends ListenerAdapter {

  boolean logMethods = false;
  boolean logStates = false;
  boolean logCgs = false;
  boolean logThreads = false;

  public SearchStateListener(Config c) {
    logMethods = c.getBoolean("search.log.methods", false);
    logStates = c.getBoolean("search.log.states", true);
    logCgs = c.getBoolean("search.log.cgs", true);
    logThreads = c.getBoolean("search.log.threads", false);

  }

  @Override
  public void propertyViolated(Search search) {
    super.propertyViolated(search);
  }

  @Override
  public void threadStarted(VM vm, ThreadInfo startedThread) {
    if (logThreads)
      System.out.println("~~~~~~~~~~~Thread Started~~~~~~~~~~ " + startedThread.getName());
  }

  @Override
  public void threadTerminated(VM vm, ThreadInfo terminatedThread) {
    if (logThreads)
      System.out.println("~~~~~~~~~~~Thread Terminated~~~~~~~~~~ " + terminatedThread.getName());
  }

  @Override
  public void choiceGeneratorRegistered(VM vm, ChoiceGenerator<?> nextCG, ThreadInfo currentThread,
                                        Instruction executedInstruction) {
    if (logCgs)
      System.out.println("~~~~~~~~~~~ChoiceGenerator Registered~~~~~~~~~~ " + nextCG);
  }

  @Override
  public void choiceGeneratorSet(VM vm, ChoiceGenerator<?> newCG) {
    if (logCgs)
      System.out.println("~~~~~~~~~~~ChoiceGenerator Set~~~~~~~~~~" + newCG);
  }

  @Override
  public void choiceGeneratorAdvanced(VM vm, ChoiceGenerator<?> currentCG) {
    if (logCgs)
      System.out.println("~~~~~~~~~~~ChoiceGenerator Advanced~~~~~~~~~~" + currentCG);
  }

  @Override
  public void choiceGeneratorProcessed(VM vm, ChoiceGenerator<?> processedCG) {
    if (logCgs)
      System.out.println("~~~~~~~~~~~ChoiceGenerator Processed~~~~~~~~~~" + processedCG);
  }

  @Override
  public void methodEntered(VM vm, ThreadInfo currentThread, MethodInfo enteredMethod) {
    if (logMethods) {
      if (!enteredMethod.getFullName().startsWith("java.")
          && !enteredMethod.getFullName().startsWith("sun.misc")
          && !enteredMethod.getFullName().startsWith("android")
          && !enteredMethod.getFullName().startsWith("com.android")
          && !enteredMethod.getFullName().startsWith("javax")
          && !enteredMethod.getFullName().startsWith("org.xml")
          && !enteredMethod.getFullName().startsWith("gov.nasa.jpf.ConsoleOutputStream")) {
        System.out.println("[methodEntered] " + enteredMethod.getFullName());
      }
    }
  }

  @Override
  public void methodExited(VM vm, ThreadInfo currentThread, MethodInfo exitedMethod) {
    if (logMethods) {
      if (!exitedMethod.getFullName().startsWith("java.")
          && !exitedMethod.getFullName().startsWith("sun.misc")
          && !exitedMethod.getFullName().startsWith("android")
          && !exitedMethod.getFullName().startsWith("com.android")
          && !exitedMethod.getFullName().startsWith("javax")
          && !exitedMethod.getFullName().startsWith("gov.nasa.jpf.ConsoleOutputStream")
          && !exitedMethod.getFullName().startsWith("org.xml")) {
        System.out.println("[methodExited]" + exitedMethod.getFullName());
      }
    }
  }

  @Override
  public void stateProcessed(Search search) {
    if (logStates)
      System.out.println("~~~~~~~~~~~State Processed~~~~~~~~~~ " + search.getStateId());
  }

  @Override
  public void stateBacktracked(Search search) {
    if (logStates)
      System.out.println("~~~~~~~~~~~State Backtracked~~~~~~~~~~ " + search.getStateId());
  }

  @Override
  public void stateStored(Search search) {
    if (logStates)
      System.out.println("~~~~~~~~~~~State Stored~~~~~~~~~~ " + search.getStateId());
  }

  @Override
  public void stateRestored(Search search) {
    if (logStates)
      System.out.println("~~~~~~~~~~~State Restored~~~~~~~~~~ " + search.getStateId());
  }

  @Override
  public void stateAdvanced(Search search) {
    if (logStates) {
      System.out.println("~~~~~~~~~~~State Advanced ~~~~~~~~~~ " + search.getStateId());
      if (search.isEndState() || search.isErrorState() || search.isVisitedState() || search.isDone()
          || search.isIgnoredState()) {
        // end of the branch
        System.out.println("~~~~~~~~~~~END OF BRANCH STATE REACHED~~~~~~~~~~ ID" + search.getStateId()
            + " - " + ((search.isEndState()) ? "Reached end state." : "")
            + ((search.isErrorState()) ? "Reached error state." : "")
            + ((search.isVisitedState()) ? "Reached visited state." : ""));
        // search.setIgnoredState(true);
      }
    }
  }

  @Override
  public void searchStarted(Search search) {
    System.out.println("~~~~~~~~~~~searchStarted~~~~~~~~~~");
  }

  @Override
  public void searchFinished(Search search) {
    System.out.println("~~~~~~~~~~~searchFinished~~~~~~~~~~");
  }

}