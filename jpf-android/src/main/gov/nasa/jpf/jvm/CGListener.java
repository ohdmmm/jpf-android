package gov.nasa.jpf.jvm;

import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.report.Publisher;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.choice.BreakGenerator;
import gov.nasa.jpf.vm.choice.ThreadChoiceFromSet;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Writes out the coverage data collected at the end of each branch to the actual RT.java class of Emma. The
 * coverage.ec file calculated for each branch is then moved into the coverage folder the project directory.
 * The the end of search, these files are merged to calculate the combined coverage over all branches and a
 * report is generated in coverage/index.html.
 * 
 * @author Heila van der Merwe
 * 
 */
public class CGListener extends ListenerAdapter {
  List<ChoiceGenerator<?>> allCGs = new LinkedList<>();


  /*
   * (non-Javadoc)
   * 
   * @see gov.nasa.jpf.ListenerAdapter#choiceGeneratorRegistered(gov.nasa.jpf.vm.VM,
   * gov.nasa.jpf.vm.ChoiceGenerator, gov.nasa.jpf.vm.ThreadInfo, gov.nasa.jpf.vm.Instruction)
   */
  @Override
  public void choiceGeneratorRegistered(VM vm, ChoiceGenerator<?> nextCG, ThreadInfo currentThread,
                                        Instruction executedInstruction) {
    allCGs.add(nextCG);

    super.choiceGeneratorRegistered(vm, nextCG, currentThread, executedInstruction);
  }

  /*
   * (non-Javadoc)
   * 
   * @see gov.nasa.jpf.ListenerAdapter#searchFinished(gov.nasa.jpf.search.Search)
   */
  @Override
  public void searchFinished(Search search) {
    printCGs();
    super.searchFinished(search);
  }

  @Override
  public void searchConstraintHit(Search search) {
    printCGs();
    super.searchConstraintHit(search);
  }

  @Override
  public void publishConstraintHit(Publisher publisher) {
    printCGs();
    super.publishConstraintHit(publisher);
  }

  public void printCGs() {
    int count = 0;
    int countNotBreak = 0;
    int choices = 0;
    int allchoices = 0;
    int numNotBreak = 0;
    int numNotBreakAll = 0;
    int numThreadChoice = 0;
    File f = new File("event/CGs.log");
    f.getParentFile().mkdirs();
    BufferedOutputStream fout = null;
    BufferedWriter bw = null;
    try {
      f.createNewFile();
      bw = new BufferedWriter(new FileWriter(f));
      bw.write("****************** ChoiceGenerators *****************");

      for (ChoiceGenerator<?> cg : allCGs) {
        count++;
        choices += cg.getProcessedNumberOfChoices();
        allchoices += cg.getTotalNumberOfChoices();

        bw.write(cg.toString() + "\n");
        if (!(cg instanceof BreakGenerator)) {
          countNotBreak++;
          numNotBreak += cg.getProcessedNumberOfChoices();
          numNotBreakAll += cg.getTotalNumberOfChoices();
        }
        if (cg instanceof ThreadChoiceFromSet) {
          numThreadChoice++;
        }
      }

      bw.write("Number of CGs: " + count);
      bw.write("All choices: " + allchoices);
      bw.write("Processed choices: " + choices + "\n");

      bw.write("NOT BREAK Num  CGs: " + countNotBreak);
      bw.write("NOT BREAK All choices: " + numNotBreakAll);
      bw.write("NOT BREAK Processed choices: " + numNotBreak + "\n");
      bw.write("Num Thread CG: " + numThreadChoice + "\n");

      bw.write("****************** ChoiceGenerators *****************");
      bw.close();
    } catch (IOException e) {
      e.printStackTrace();
    } 
  }

}
