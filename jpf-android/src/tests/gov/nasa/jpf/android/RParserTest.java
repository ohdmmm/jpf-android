package gov.nasa.jpf.android;

import gov.nasa.jpf.android.RParser.RParseException;
import gov.nasa.jpf.util.test.TestJPF;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class RParserTest extends TestJPF {

  RParser parser;
  RFile rfile;

  @Before
  public void setup() {
    RParser parser = RParser.getInstance();
    File f = new File("src/tests/gov/nasa/jpf/android/R.java");
    assertTrue(f.exists());
    FileInputStream fi;
    try {
      fi = new FileInputStream(f);
      rfile = parser.parse(fi);
    } catch (FileNotFoundException e1) {
      e1.printStackTrace();
      assertTrue("File not found R.java", false);
    } catch (RParseException e2) {
      assertTrue("ParseException for R.java", false);
    }
    assertTrue(rfile != null);
  }

  @Test
  public void testParseString() {
    String name = rfile.getStringNameForId(0x7f060018);
    String name2 = rfile.getStringNameForId(0x7f060029);

    assertTrue(name.equals("AboutText"));
    assertTrue(name2.equals("ClearClipboard"));
  }

  @Test
  public void testStringNotFound() {
    String name = rfile.getStringNameForId(-1);
    assertTrue(name == null);
  }

  @Test
  public void testParseArray() {
    String name = rfile.getArrayNameForId(0x7f080002);
    String name2 = rfile.getArrayNameForId(0x7f080000);

    assertTrue(name.equals("clipboard_timeout_options"));
    assertTrue(name2.equals("clipboard_timeout_values"));
  }

  @Test
  public void testParseBool() {
    String name = rfile.getBoolNameForId(0x7f070001);
    String name2 = rfile.getBoolNameForId(0x7f070000);

    assertTrue(name.equals("keyfile_default"));
    assertTrue(name2.equals("maskpass_default"));
  }

  @Test
  public void testParseViewID() {
    String name = rfile.getViewNameForId(0x7f0a005d);
    String name2 = rfile.getViewNameForId(0x7f0a0041);

    assertTrue(name.equals("IconGridView"));
    assertTrue(name2.equals("RelativeLayout"));
  }

  @Test
  public void testParseLayout() {
    String name = rfile.getLayoutNameForId(0x7f030000);
    String name2 = rfile.getLayoutNameForId(0x7f030001);

    assertTrue(name.equals("about"));
    assertTrue(name2.equals("browser_install"));
  }

  @Test
  public void testParseXML() {
    String name = rfile.getXmlNameForId(0x7f040000);
    String name2 = rfile.getXmlNameForId(0x7f040001);

    assertTrue(name.equals("preferences"));
    assertTrue(name2.equals("searchable"));
  }

  @Test
  public void testParseResource() {
    // String name = rfile.getResourceName(0x7f040000);
    // String name2 = rfile.getResourceName(0x7f030001);
    //
    // assertTrue(name.equals("preferences"));
    // assertTrue(name2.equals("browser_install"));
  }

  @Test
  public void testParseViewIDForName() {
    int id = rfile.getViewIdForName("IconGridView");
    int id2 = rfile.getViewIdForName("RelativeLayout");

    assertTrue(id == 0x7f0a005d);
    assertTrue(id2 == 0x7f0a0041);
  }

}
