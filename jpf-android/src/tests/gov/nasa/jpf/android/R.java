package gov.nasa.jpf.android;

public final class R {
  public static final class array {
    public static final int clipboard_timeout_options = 0x7f080002;
    public static final int clipboard_timeout_values = 0x7f080000;
  }

  public static final class attr {
  }

  public static final class bool {
    public static final int keyfile_default = 0x7f070001;
    public static final int maskpass_default = 0x7f070000;
  }

  public static final class color {
    public static final int blue_highlight = 0x7f050000;
    public static final int group = 0x7f050001;
  }

  public static final class drawable {
    public static final int ic00 = 0x7f020000;
    public static final int ic01 = 0x7f020001;

  }

  public static final class id {
    public static final int IconGridView = 0x7f0a005d;
    public static final int RelativeLayout = 0x7f0a0041;

  }

  public static final class layout {
    public static final int about = 0x7f030000;
    public static final int browser_install = 0x7f030001;

  }

  public static final class string {
    public static final int AboutText = 0x7f060018;
    public static final int ClearClipboard = 0x7f060029;

  }

  public static final class style {
    public static final int ElementText = 0x7f090004;
    public static final int ElementTextLarge = 0x7f090005;
  }

  public static final class xml {
    public static final int preferences = 0x7f040000;
    public static final int searchable = 0x7f040001;
  }
}
