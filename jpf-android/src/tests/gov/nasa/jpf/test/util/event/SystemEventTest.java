package gov.nasa.jpf.test.util.event;

import gov.nasa.jpf.util.event.events.SystemEvent;
import gov.nasa.jpf.util.test.TestJPF;

import org.junit.Test;

import android.content.Intent;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class SystemEventTest extends TestJPF {

  @Test
  public void testSystemEvent() {

  }

  @Test
  public void testSystemEventEquals() throws CloneNotSupportedException {
    SystemEvent event = new SystemEvent("testTarget", "testAction");
    Intent i = new Intent("testIntentAction");
    event.setIntent(i);
    SystemEvent event2 = (SystemEvent) event.clone();
    boolean r = event2.equals(event);
    assert r;
    event2.getIntent().putExtra("TestBundle", true);
    r = !event2.equals(event);
    assert r;
  }
  // public static void main(String testMethods[]) throws Throwable {
  // runTestsOfThisClass(testMethods);
  // }

}