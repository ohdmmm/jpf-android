package gov.nasa.jpf.test.util.event.generator;

import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.test.TestJPF;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * This class tests the ScriptEventGenerator. The ScriptEventGenerator returns the next event in the script.
 * We have added the method processNextScriptEvent() in the AndroidEventProducer class to test the Script
 * separately.
 * 
 * Please note that this Test class assumes we do not want state matching to happen (otherwise we will not
 * know if the elements work in the case of no state matching) It is important to realize that events do not
 * change the state of the application. That is why we do not mark StringBuilder sb as Filtered. Field sb is
 * always unique in the case that the event sequence is unique.
 * 
 * For the repeat infinite we want state matching to ensure that execution end. In this case we created
 * another sb1 with the FilterField annotation to ensure that the state does not change. A break choice
 * generator is pushed by JPF in when the transition reaches MAX length and when the transition breaks twice,
 * the states are matched and execution ended.
 * 
 * The {@link Sequencer} listener adapter retrieves the result of the getSequence() method when it returns
 * with the latest event sequence and then stores it for testing against the expected sequences for each path
 * in the application.
 * 
 * If you want to verify that state matching occurs, sb needs to by filtered and then line 86 & 102 need to be
 * uncommented. This will allow different paths in the script to match since i will be same for them - the
 * expected strings will have to be changed or lines 354 358 commented.
 * 
 * If errors are found logging can be turned on my adding "+log.level=info" to the
 * verifyNoPropertyViolation(..) method as another parameter.
 * 
 * @author Heila van der Merwe
 * @date 8 April 2015
 * 
 */
public class ScriptEventGeneratorTest extends TestJPF {

  /**
   * This field stores the id's of the next event for each Section in the script. The index of the list
   * corresponds to a specific section and the the int value maps to the ID of the next element in the script.
   * The state of the script must be included in the state to ensure that states for which the next element do
   * not match, can not be matched.
   */
  private int[] scriptState = null;

  /**
   * The repeat state stores the counter of each repeat element in the script. The reason we need to store
   * this as part of the state is that we need backtrack the state of each repeat in the case of
   * nondetermisitic application execution as well as that it forms part of the scriptstate that needs to be
   * used for state-matching.
   */
  private int[] repeatState = null;

  public native String processNextScriptEvent();

  public native void zeroSG();

  // Ensure the fact that we are storing the generated events do not influence state matching
  // @FilterField
  StringBuilder sb = null;

  // Ensure the fact that we are storing the generated events do not influence state matching
  @FilterField
  String s = null;

  // Ensure the fact that we are storing the generated events do not influence state matching
  @FilterField
  String[] expected;

  @FilterField
  String config = "+listener=,gov.nasa.jpf.test.util.event.generator.ScriptEventGeneratorTest$Sequencer,";

  public static class Sequencer extends ListenerAdapter {
    int sequenceCount = 0;

    static ArrayList<String> sequence;

    @Override
    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction nextInstruction,
                                    Instruction executedInstruction) {
      if (executedInstruction instanceof gov.nasa.jpf.jvm.bytecode.ARETURN) {
        MethodInfo mi = executedInstruction.getMethodInfo();

        if (mi.getUniqueName().equals("getSequence()Ljava/lang/String;")) {
          String a = new String(
              ((ElementInfo) ((gov.nasa.jpf.jvm.bytecode.ARETURN) executedInstruction)
                  .getReturnValue(currentThread)).getStringChars());
          if (!a.equals("")) {
            sequenceCount++;
            sequence.add("Sequence " + sequenceCount + ": ");
            sequence.add(a);
          }
        }
      }
    }
  }

  // int i = 0;

  @Before
  public void setUp() {
    if (!isJPFRun()) {
      Sequencer.sequence = new ArrayList<String>();
    } else {
      zeroSG();
    }
  }

  private String getSequence() {
    sb = new StringBuilder();
    s = processNextScriptEvent();
    System.out.println("\n$$$ GOT EVENT: " + s);
    while (s != null) {
      // i++;
      if (!s.contains("AndroidEvent"))
        sb.append(s);
      s = processNextScriptEvent();
      System.out.println("\n$$$ GOT EVENT: " + s);
    }
    System.out.print("\n$$$$$$$$$$$$$$$$$$$$$$$ Event Sequence: ");
    String s = sb.toString();
    System.out.println(s);
    return s.toString();
  }

  @FilterField
  StringBuilder sb1;

  private String getSequenceInifinite() {
    sb1 = new StringBuilder();
    s = processNextScriptEvent();
    while (s != null) {
      sb1.append(s);
      s = processNextScriptEvent();
    }
    String s = sb1.toString();
    return s.toString();
  }

  /* ***************************** ANY ******************************** */
  @Test
  public void testAny() {

    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "$button10.onClick()", "Sequence 2: ", "$button11.onClick()" };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestAny.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testAnyChoiceEmpty() {

    if (!isJPFRun()) {
      String[] e = {};
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestAnyChoiceEmpty.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testAnyComplex() {

    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ",
          "$button1.onClick()$button1.onClick()$button1.onClick()$button9.onClick()", "Sequence 2: ",
          "$button2.onClick()$button2.onClick()$button9.onClick()", "Sequence 3: ",
          "$button3.onClick()$button9.onClick()", "Sequence 4: ",
          "$buttonNext.onClick()$buttonClear.onClick()" };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestAnyComplex.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testAnyAny() {

    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "$button9.onClick()$button1.onClick()", "Sequence 2: ",
          "$button9.onClick()$button2.onClick()", "Sequence 3: ", "$button9.onClick()$button3.onClick()",
          "Sequence 4: ", "$buttonNext.onClick()$buttonClear.onClick()$button1.onClick()", "Sequence 5: ",
          "$buttonNext.onClick()$buttonClear.onClick()$button2.onClick()", "Sequence 6: ",
          "$buttonNext.onClick()$buttonClear.onClick()$button3.onClick()" };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestAnyAny.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testAnyNoChoices() {

    if (!isJPFRun()) {
      String[] e = {};
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestAnyNoChoices.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  /* ***************************** REPEAT ******************************** */

  @Test
  public void testSimpleRepeat() {

    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "$button2.onClick()$button2.onClick()" };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestRepeat.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testRepeatComplex() {

    if (!isJPFRun()) {
      String[] e = {
          "Sequence 1: ",
          "$button0.onClick()$button1.onClick()$button2.onClick()$button3.onClick()$button3.onClick()$button4.onClick()$button2.onClick()$button3.onClick()$button3.onClick()$button4.onClick()$button5.onClick()$button5.onClick()$button5.onClick()$button2.onClick()$button3.onClick()$button3.onClick()$button4.onClick()$button2.onClick()$button3.onClick()$button3.onClick()$button4.onClick()$button5.onClick()$button5.onClick()$button5.onClick()" };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestRepeatComplex.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testRepeat0() {

    if (!isJPFRun()) {
      String[] e = {};
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestRepeat0.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testRepeatEmpty() {

    if (!isJPFRun()) {
      String[] e = {};
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestRepeatEmpty.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @FilterField
  String[] inf = {};

  @Test
  public void testRepeatInfinite() {

    if (!isJPFRun()) {
      expected = inf;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestRepeatInfinite.es",
        "+event.strategy=script", config)) {
      getSequenceInifinite();
    }
  }

  /* ***************************** OTHER ******************************** */

  @Test
  public void testSimpleEvent() {

    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "$button10.onClick()$button11.onClick()$button12.onClick()" };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestEvent.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testAnyRepeat() {

    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "$button0.onClick()", "Sequence 2: ", "$button1.onClick()",
          "Sequence 3: ", "$button2.onClick()", "Sequence 4: ", "$button3.onClick()", "Sequence 5: ",
          "$button4.onClick()$button4.onClick()$button4.onClick()$button4.onClick()" };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestAnyRepeat.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testRepeatAny() {

    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ",
          "$button1.onClick()$button4.onClick()$button1.onClick()$button4.onClick()", "Sequence 2: ",
          "$button1.onClick()$button4.onClick()$button1.onClick()$button5.onClick()", "Sequence 3: ",
          "$button1.onClick()$button4.onClick()$button2.onClick()$button4.onClick()", "Sequence 4: ",
          "$button1.onClick()$button4.onClick()$button2.onClick()$button5.onClick()", "Sequence 5: ",
          "$button1.onClick()$button5.onClick()$button1.onClick()$button4.onClick()", "Sequence 6: ",
          "$button1.onClick()$button5.onClick()$button1.onClick()$button5.onClick()", "Sequence 7: ",
          "$button1.onClick()$button5.onClick()$button2.onClick()$button4.onClick()", "Sequence 8: ",
          "$button1.onClick()$button5.onClick()$button2.onClick()$button5.onClick()", "Sequence 9: ",
          "$button2.onClick()$button4.onClick()$button1.onClick()$button4.onClick()", "Sequence 10: ",
          "$button2.onClick()$button4.onClick()$button1.onClick()$button5.onClick()", "Sequence 11: ",
          "$button2.onClick()$button4.onClick()$button2.onClick()$button4.onClick()", "Sequence 12: ",
          "$button2.onClick()$button4.onClick()$button2.onClick()$button5.onClick()", "Sequence 13: ",
          "$button2.onClick()$button5.onClick()$button1.onClick()$button4.onClick()", "Sequence 14: ",
          "$button2.onClick()$button5.onClick()$button1.onClick()$button5.onClick()", "Sequence 15: ",
          "$button2.onClick()$button5.onClick()$button2.onClick()$button4.onClick()", "Sequence 16: ",
          "$button2.onClick()$button5.onClick()$button2.onClick()$button5.onClick()", };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestRepeatAny.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testRepeatAnySimple() {

    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "$button1.onClick()$button1.onClick()", "Sequence 2: ",
          "$button1.onClick()$button2.onClick()", "Sequence 3: ", "$button2.onClick()$button1.onClick()",
          "Sequence 4: ", "$button2.onClick()$button2.onClick()", };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestRepeatAnySimple.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  /* ***************************** SYSTEM EVENTS ******************************** */

  @Test
  public void testSystemEvent() {

    if (!isJPFRun()) {
      String[] e = {
          "Sequence 1: ",
          "SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: null , component:null, extra: no extras ]]SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: null , component:null, extra:  (Test2, String) ]]SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: null , component:null, extra:  (Test2, String) (TestInt, 2) ]]SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: null , component:null, extra:  (TestBool, true) (Test2, String) (TestInt, 2) ]]SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: null , component:null, extra:  (TestBool, true) (Test2, String) (TestInt, 2) (TestFloat, 2.2) ]]SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: null , component:null, extra:  (TestBool, true) (TestDouble, 333.0) (Test2, String) (TestInt, 2) (TestFloat, 2.2) ]]SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: null , component:null, extra:  (TestBool, true) (TestByte, 1) (TestDouble, 333.0) (Test2, String) (TestInt, 2) (TestFloat, 2.2) ]]SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: null , component:null, extra:  (TestBool, true) (TestByte, 1) (TestChar, c) (TestDouble, 333.0) (Test2, String) (TestInt, 2) (TestFloat, 2.2) ]]SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: null , component:null, extra:  (TestBool, true) (TestByte, 1) (TestChar, c) (TestDouble, 333.0) (Test2, String) (TestInt, 2) (TestFloat, 2.2) (TestShort, 11) ]]SystemEvent [target=null, action=startActivity, arguments=@intent3, intent=[ action:null, cat: null , component:ComponentInfo{com.android.test/Tester}, extra: no extras ]]SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:Test, cat: {CATEGORY} , component:null, extra:  (TestBool, true) (TestByte, 1) (TestChar, c) (TestDouble, 333.0) (Test2, String) (TestInt, 2) (TestFloat, 2.2) (TestShort, 11) ]]", };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestSystemEvent.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testSystemEventAny() {

    if (!isJPFRun()) {
      String[] e = {
          "Sequence 1: ",
          "SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:A, cat: null , component:null, extra: no extras ]]",
          "Sequence 2: ",
          "SystemEvent [target=null, action=startActivity, arguments=@intent2, intent=[ action:B, cat: null , component:null, extra: no extras ]]", };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestSystemEventAny.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testSystemEventWrongComponentName() {

    if (!isJPFRun()) {
      String[] e = {};
      expected = e;
    }

    if (verifyUnhandledException(
        "gov.nasa.jpf.util.event.InvalidEventException",
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestSystemEventWrongComponentName.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testSystemEventMethodNotFound() {

    if (!isJPFRun()) {
      String[] e = {};
      expected = e;
    }

    if (verifyUnhandledExceptionDetails("gov.nasa.jpf.util.event.InvalidEventException",
        "ScriptEventGenerator: Could not find method \"doesNotExist\" on Intent.java for parameters null",
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestSystemEventMethodNotFound.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testSystemEventNoIntentExists() {

    if (!isJPFRun()) {
      String[] e = {};
      expected = e;
    }

    if (verifyUnhandledExceptionDetails("gov.nasa.jpf.util.event.InvalidEventException",
        "ScriptEventGenerator: No Intent found in map for @intent3",
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestSystemEventNoIntentExists.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testSystemEventWrongNumArgs() {

    if (!isJPFRun()) {
      String[] e = {};
      expected = e;
    }

    if (verifyUnhandledException("gov.nasa.jpf.util.event.InvalidEventException",
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestSystemEventWrongNumArgs.es",
        "+event.strategy=script", config)) {
      getSequence();
    }
  }

  @Test
  public void testSystemEventWrongNumArgs2() {

    if (!isJPFRun()) {
      String[] e = {};
      expected = e;
    }

    if (verifyUnhandledException("gov.nasa.jpf.util.event.InvalidEventException",
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestSystemEventWrongNumArgs2.es",
        "+event.strategy=script", config)) {
      getSequence();
    }

  }

  @After
  public void takeDown() {
    if (!isJPFRun()) {
      if (expected.length != 0) {
        assertEquals(expected.length, Sequencer.sequence.size());
      }
      int i = 0;
      for (String s : Sequencer.sequence) {
        System.out.println("\"" + s + "\", ");
        if (expected.length != 0) {
          assertEquals("given: \"" + s + "\", expected: " + expected[i], expected[i], s);
        }
        i++;
      }
    }
  }
}