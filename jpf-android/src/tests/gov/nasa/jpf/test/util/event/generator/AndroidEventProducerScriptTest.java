package gov.nasa.jpf.test.util.event.generator;

import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.AndroidEventProducer;
import gov.nasa.jpf.util.event.InvalidEventException;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.test.TestJPF;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AndroidEventProducerScriptTest extends TestJPF {

  public class TestAndroidEventProducer extends AndroidEventProducer {

    public TestAndroidEventProducer() {

      // Hack for now to ensure class has been loaded
      // Event[] events = new Event[1];
      // events[0] = new UIEvent("button0", "onClick", null);
    }

    @Override
    public int processEvent(Event event) throws InvalidEventException {
      // we do not want to process the event
      if (event != null) {
        System.out.println(event);
        return 2;
      } else {
        System.out.println("NO MORE EVENTS");
        return 0;
      }
    }

  }

  public static class Sequencer extends ListenerAdapter {
    int sequenceCount = 0;

    static ArrayList<String> sequence;

    @Override
    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction nextInstruction,
                                    Instruction executedInstruction) {
      if (executedInstruction instanceof gov.nasa.jpf.jvm.bytecode.NATIVERETURN) {
        MethodInfo mi = executedInstruction.getMethodInfo();
        if (mi.getUniqueName().equals("getEventTreeString()Ljava/lang/String;")) {
          Integer istr = (Integer) ((gov.nasa.jpf.jvm.bytecode.NATIVERETURN) executedInstruction)
              .getReturnValue(currentThread);
          char[] s = vm.getHeap().get(istr).getStringChars();
          if (!s.equals("")) {
            sequenceCount++;
            sequence.add("Sequence " + sequenceCount + ": ");
            sequence.add(new String(s));
          }
        }
      }
    }
  }

  // Ensure the fact that we are storing the generated events do not influence state matching
  // @FilterField
  StringBuilder sb = null;

  // Ensure the fact that we are storing the generated events do not influence state matching
  @FilterField
  String[] expected;

  @FilterField
  String config = "+listener+=,gov.nasa.jpf.test.util.event.generator.AndroidEventProducerScriptTest$Sequencer,";

  @Before
  public void setUp() {
    if (isJPFRun()) {
      AndroidEventProducer.reset();
    } else {
      Sequencer.sequence = new ArrayList<String>();
    }
  }

  @Test
  public void testAny() {
    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "$button10.onClick()", "Sequence 2: ", "$button11.onClick()", };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestAny.es",
        "+event.strategy=script", config)) {

      AndroidEventProducer producer = new TestAndroidEventProducer();
      int b = producer.processNextEvent();
      while (b != 0) {
        b = producer.processNextEvent();
      }
      producer.getEventTreeString();
    }
  }

  @Test
  public void testAnyComplex() {
    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ",
          "$button1.onClick()$button1.onClick()$button1.onClick()$button9.onClick()", "Sequence 2: ",
          "$button2.onClick()$button2.onClick()$button9.onClick()", "Sequence 3: ",
          "$button3.onClick()$button9.onClick()", "Sequence 4: ",
          "$buttonNext.onClick()$buttonClear.onClick()" };
      expected = e;
    }

    if (verifyNoPropertyViolation(
        "+android.script=src/tests/gov/nasa/jpf/test/util/event/generator/TestAnyComplex.es",
        "+event.strategy=script", config)) {
      AndroidEventProducer producer = new TestAndroidEventProducer();
      int b = producer.processNextEvent();
      while (b != 0) {
        b = producer.processNextEvent();
      }
      producer.getEventTreeString();
    }
  }

  @After
  public void takeDown() {
    if (!isJPFRun()) {
      // assertEquals(expected.length, Sequencer.sequence.size());

      int i = 0;
      for (String s : Sequencer.sequence) {
        System.out.println("\"" + s + "\", ");
        // assertEquals("given: \"" + s + "\", expected: " + expected[i], expected[i], s);
        i++;
      }
    }
  }
}
