package gov.nasa.jpf.test.util.script;

import gov.nasa.jpf.util.script.impl.Script;
import gov.nasa.jpf.util.script.parser.ParseException;
import gov.nasa.jpf.util.script.parser.ScriptParser;
import gov.nasa.jpf.util.script.visitor.PrintVisitor;
import gov.nasa.jpf.util.test.TestJPF;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.junit.Test;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class ScriptStateTest extends TestJPF {

  @Test
  public void testSimpleTree2() {
    ScriptParser parser;
    try {
      parser = new ScriptParser(new FileReader("src/tests/gov/nasa/jpf/test/util/script/TestAny.es"));

      Script s = parser.parse();
      PrintVisitor p = new PrintVisitor();
      p.visit(s);

    } catch (FileNotFoundException | ParseException e) {
      e.printStackTrace();
    }
  }
}
