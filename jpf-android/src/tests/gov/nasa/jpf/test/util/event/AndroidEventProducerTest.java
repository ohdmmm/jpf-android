package gov.nasa.jpf.test.util.event;

import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.AndroidEventProducer;
import gov.nasa.jpf.util.event.InvalidEventException;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.UIEvent;
import gov.nasa.jpf.util.test.TestJPF;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.Verify;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AndroidEventProducerTest extends TestJPF {

  public static class Sequencer extends ListenerAdapter {
    int sequenceCount = 0;

    static ArrayList<String> sequence;

    @Override
    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction nextInstruction,
                                    Instruction executedInstruction) {
      if (executedInstruction instanceof gov.nasa.jpf.jvm.bytecode.NATIVERETURN) {
        MethodInfo mi = executedInstruction.getMethodInfo();
        if (mi.getUniqueName().equals("getEventTreeString()Ljava/lang/String;")) {
          Integer istr = (Integer) ((gov.nasa.jpf.jvm.bytecode.NATIVERETURN) executedInstruction)
              .getReturnValue(currentThread);
          char[] s = vm.getHeap().get(istr).getStringChars();
          if (!s.equals("")) {
            sequenceCount++;
            sequence.add("Sequence " + sequenceCount + ": ");
            sequence.add(new String(s));
          }
        }
      }
    }
  }

  public static class TestSingleAndroidEventProducer extends AndroidEventProducer {

    @Override
    public Event[] getEvents() {
      Event[] events = new Event[1];
      events[0] = new UIEvent("button0", "onClick", null);
      return events;
    }

    @Override
    public int processEvent(Event event) throws InvalidEventException {
      return 2;
    }

  }

  public static class TestAndroidEventProducer extends AndroidEventProducer {

    @Override
    public Event[] getEvents() {
      Event[] events = new Event[2];
      events[0] = new UIEvent("button1", "onClick", null);
      events[1] = new UIEvent("button22", "onClick2", null);
      return events;
    }

    @Override
    public int processEvent(Event event) throws InvalidEventException {
      return 2;
    }

  }

  public static class TestIncAndroidEventProducer extends AndroidEventProducer {

    public TestIncAndroidEventProducer() {
      super();
      Verify.setCounter(1, 2);
    }

    @Override
    public Event[] getEvents() {
      // for each manager in servicemanager

      Event[] events = new UIEvent[26];
      events[0] = new UIEvent("button0", "onClick", null);
      events[1] = new UIEvent("button1", "onClick", null);
      events[2] = new UIEvent("button2", "onClick", null);
      events[3] = new UIEvent("button3", "onClick", null);
      events[4] = new UIEvent("button4", "onClick", null);
      events[5] = new UIEvent("button5", "onClick", null);
      events[6] = new UIEvent("button6", "onClick", null);
      events[7] = new UIEvent("button7", "onClick", null);
      events[8] = new UIEvent("button8", "onClick", null);
      events[9] = new UIEvent("button9", "onClick", null);
      events[10] = new UIEvent("button10", "onClick", null);
      events[11] = new UIEvent("button11", "onClick", null);
      events[12] = new UIEvent("button12", "onClick", null);
      events[13] = new UIEvent("button13", "onClick", null);
      events[14] = new UIEvent("button14", "onClick", null);
      events[15] = new UIEvent("button15", "onClick", null);
      events[16] = new UIEvent("button16", "onClick", null);
      events[17] = new UIEvent("button17", "onClick", null);
      events[18] = new UIEvent("button18", "onClick", null);
      events[19] = new UIEvent("button19", "onClick", null);
      events[20] = new UIEvent("button20", "onClick", null);
      events[21] = new UIEvent("button21", "onClick", null);
      events[22] = new UIEvent("button22", "onClick", null);
      events[23] = new UIEvent("button23", "onClick", null);
      events[24] = new UIEvent("button24", "onClick", null);
      events[25] = new UIEvent("button25", "onClick", null);

      System.out.println("COunter: " + Verify.getCounter(1));
      events = Arrays.copyOfRange(events, 0, Verify.getCounter(1));
      Verify.incrementCounter(1);
      return events;
    }

    @Override
    public int processEvent(Event event) throws InvalidEventException {
      return 2;
    }

  }

  // Ensure the fact that we are storing the generated events do not influence state matching
  // @FilterField
  StringBuilder sb = null;

  // Ensure the fact that we are storing the generated events do not influence state matching
  @FilterField
  String[] expected;

  @FilterField
  String config = "+listener+=,gov.nasa.jpf.test.util.event.AndroidEventProducerTest$Sequencer,";
  String config2 = "+testing=true";

  @Before
  public void setUp() {
    if (isJPFRun()) {
      AndroidEventProducer.reset();
    } else {
      Sequencer.sequence = new ArrayList<String>();
    }
  }

  @Test
  public void testTest0() {
    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "button0.onClick()" };
      expected = e;
    }
    if (verifyNoPropertyViolation("+event.strategy=default", config, config2)) {
      AndroidEventProducer producer = new TestSingleAndroidEventProducer();
      producer.processNextEvent();
      producer.getEventTreeString();
    }
  }

  @Test
  public void testTest00() {
    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "button0.onClick()button0.onClick()button0.onClick()", };
      expected = e;
    }
    if (verifyNoPropertyViolation("+event.strategy=default", config, config2)) {
      sb = new StringBuilder();
      AndroidEventProducer producer = new TestSingleAndroidEventProducer();
      producer.processNextEvent();
      producer.processNextEvent();
      producer.processNextEvent();
      producer.getEventTreeString();
    }
  }

  @Test
  public void testTest1() {
    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "button1.onClick()", "Sequence 2: ", "button22.onClick2()" };
      expected = e;
    }
    if (verifyNoPropertyViolation("+event.strategy=default", config, config2)) {
      AndroidEventProducer producer = new TestAndroidEventProducer();
      producer.processNextEvent();
      producer.getEventTreeString();

    }
  }

  @Test
  public void testTest2() {
    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "button1.onClick()button1.onClick()", "Sequence 2: ",
          "button1.onClick()button22.onClick2()", "Sequence 3: ", "button22.onClick2()button1.onClick()",
          "Sequence 4: ", "button22.onClick2()button22.onClick2()" };
      expected = e;
    }
    if (verifyNoPropertyViolation("+event.strategy=default", config, config2)) {
      Verify.setCounter(2, 0);
      sb = new StringBuilder();

      AndroidEventProducer producer = new TestAndroidEventProducer();

      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));

      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));
      String s = sb.toString();

      producer.getEventTreeString();
    }
  }

  @Test
  public void testTest3() {
    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "button0.onClick()button0.onClick()", "Sequence 2: ",
          "button0.onClick()button1.onClick()", "Sequence 3: ", "button0.onClick()button2.onClick()",
          "Sequence 4: ", "button1.onClick()button0.onClick()", "Sequence 5: ",
          "button1.onClick()button1.onClick()", "Sequence 6: ", "button1.onClick()button2.onClick()",
          "Sequence 7: ", "button1.onClick()button3.onClick()", };
      expected = e;
    }
    if (verifyNoPropertyViolation("+event.strategy=default", config, config2)) {
      AndroidEventProducer producer = new TestIncAndroidEventProducer();
      sb = new StringBuilder();
      Verify.setCounter(1, 2);
      Verify.setCounter(2, 0);

      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));

      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));

      producer.getEventTreeString();
    }
  }

  @Test
  public void testTest4() {
    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "button0.onClick()button0.onClick()button0.onClick()", "Sequence 2: ",
          "button0.onClick()button0.onClick()button1.onClick()", "Sequence 3: ",
          "button0.onClick()button0.onClick()button2.onClick()", "Sequence 4: ",
          "button0.onClick()button0.onClick()button3.onClick()", "Sequence 5: ",
          "button0.onClick()button1.onClick()button0.onClick()", "Sequence 6: ",
          "button0.onClick()button1.onClick()button1.onClick()", "Sequence 7: ",
          "button0.onClick()button1.onClick()button2.onClick()", "Sequence 8: ",
          "button0.onClick()button1.onClick()button3.onClick()", "Sequence 9: ",
          "button0.onClick()button1.onClick()button4.onClick()", "Sequence 10: ",
          "button0.onClick()button2.onClick()button0.onClick()", "Sequence 11: ",
          "button0.onClick()button2.onClick()button1.onClick()", "Sequence 12: ",
          "button0.onClick()button2.onClick()button2.onClick()", "Sequence 13: ",
          "button0.onClick()button2.onClick()button3.onClick()", "Sequence 14: ",
          "button0.onClick()button2.onClick()button4.onClick()", "Sequence 15: ",
          "button0.onClick()button2.onClick()button5.onClick()", "Sequence 16: ",
          "button1.onClick()button0.onClick()button0.onClick()", "Sequence 17: ",
          "button1.onClick()button0.onClick()button1.onClick()", "Sequence 18: ",
          "button1.onClick()button0.onClick()button2.onClick()", "Sequence 19: ",
          "button1.onClick()button0.onClick()button3.onClick()", "Sequence 20: ",
          "button1.onClick()button0.onClick()button4.onClick()", "Sequence 21: ",
          "button1.onClick()button0.onClick()button5.onClick()", "Sequence 22: ",
          "button1.onClick()button0.onClick()button6.onClick()", "Sequence 23: ",
          "button1.onClick()button0.onClick()button7.onClick()", "Sequence 24: ",
          "button1.onClick()button1.onClick()button0.onClick()", "Sequence 25: ",
          "button1.onClick()button1.onClick()button1.onClick()", "Sequence 26: ",
          "button1.onClick()button1.onClick()button2.onClick()", "Sequence 27: ",
          "button1.onClick()button1.onClick()button3.onClick()", "Sequence 28: ",
          "button1.onClick()button1.onClick()button4.onClick()", "Sequence 29: ",
          "button1.onClick()button1.onClick()button5.onClick()", "Sequence 30: ",
          "button1.onClick()button1.onClick()button6.onClick()", "Sequence 31: ",
          "button1.onClick()button1.onClick()button7.onClick()", "Sequence 32: ",
          "button1.onClick()button1.onClick()button8.onClick()", "Sequence 33: ",
          "button1.onClick()button2.onClick()button0.onClick()", "Sequence 34: ",
          "button1.onClick()button2.onClick()button1.onClick()", "Sequence 35: ",
          "button1.onClick()button2.onClick()button2.onClick()", "Sequence 36: ",
          "button1.onClick()button2.onClick()button3.onClick()", "Sequence 37: ",
          "button1.onClick()button2.onClick()button4.onClick()", "Sequence 38: ",
          "button1.onClick()button2.onClick()button5.onClick()", "Sequence 39: ",
          "button1.onClick()button2.onClick()button6.onClick()", "Sequence 40: ",
          "button1.onClick()button2.onClick()button7.onClick()", "Sequence 41: ",
          "button1.onClick()button2.onClick()button8.onClick()", "Sequence 42: ",
          "button1.onClick()button2.onClick()button9.onClick()", "Sequence 43: ",
          "button1.onClick()button3.onClick()button0.onClick()", "Sequence 44: ",
          "button1.onClick()button3.onClick()button1.onClick()", "Sequence 45: ",
          "button1.onClick()button3.onClick()button2.onClick()", "Sequence 46: ",
          "button1.onClick()button3.onClick()button3.onClick()", "Sequence 47: ",
          "button1.onClick()button3.onClick()button4.onClick()", "Sequence 48: ",
          "button1.onClick()button3.onClick()button5.onClick()", "Sequence 49: ",
          "button1.onClick()button3.onClick()button6.onClick()", "Sequence 50: ",
          "button1.onClick()button3.onClick()button7.onClick()", "Sequence 51: ",
          "button1.onClick()button3.onClick()button8.onClick()", "Sequence 52: ",
          "button1.onClick()button3.onClick()button9.onClick()", "Sequence 53: ",
          "button1.onClick()button3.onClick()button10.onClick()", "Sequence 54: ",
          "button1.onClick()button4.onClick()button0.onClick()", "Sequence 55: ",
          "button1.onClick()button4.onClick()button1.onClick()", "Sequence 56: ",
          "button1.onClick()button4.onClick()button2.onClick()", "Sequence 57: ",
          "button1.onClick()button4.onClick()button3.onClick()", "Sequence 58: ",
          "button1.onClick()button4.onClick()button4.onClick()", "Sequence 59: ",
          "button1.onClick()button4.onClick()button5.onClick()", "Sequence 60: ",
          "button1.onClick()button4.onClick()button6.onClick()", "Sequence 61: ",
          "button1.onClick()button4.onClick()button7.onClick()", "Sequence 62: ",
          "button1.onClick()button4.onClick()button8.onClick()", "Sequence 63: ",
          "button1.onClick()button4.onClick()button9.onClick()", "Sequence 64: ",
          "button1.onClick()button4.onClick()button10.onClick()", "Sequence 65: ",
          "button1.onClick()button4.onClick()button11.onClick()", "Sequence 66: ",
          "button1.onClick()button5.onClick()button0.onClick()", "Sequence 67: ",
          "button1.onClick()button5.onClick()button1.onClick()", "Sequence 68: ",
          "button1.onClick()button5.onClick()button2.onClick()", "Sequence 69: ",
          "button1.onClick()button5.onClick()button3.onClick()", "Sequence 70: ",
          "button1.onClick()button5.onClick()button4.onClick()", "Sequence 71: ",
          "button1.onClick()button5.onClick()button5.onClick()", "Sequence 72: ",
          "button1.onClick()button5.onClick()button6.onClick()", "Sequence 73: ",
          "button1.onClick()button5.onClick()button7.onClick()", "Sequence 74: ",
          "button1.onClick()button5.onClick()button8.onClick()", "Sequence 75: ",
          "button1.onClick()button5.onClick()button9.onClick()", "Sequence 76: ",
          "button1.onClick()button5.onClick()button10.onClick()", "Sequence 77: ",
          "button1.onClick()button5.onClick()button11.onClick()", "Sequence 78: ",
          "button1.onClick()button5.onClick()button12.onClick()", "Sequence 79: ",
          "button1.onClick()button6.onClick()button0.onClick()", "Sequence 80: ",
          "button1.onClick()button6.onClick()button1.onClick()", "Sequence 81: ",
          "button1.onClick()button6.onClick()button2.onClick()", "Sequence 82: ",
          "button1.onClick()button6.onClick()button3.onClick()", "Sequence 83: ",
          "button1.onClick()button6.onClick()button4.onClick()", "Sequence 84: ",
          "button1.onClick()button6.onClick()button5.onClick()", "Sequence 85: ",
          "button1.onClick()button6.onClick()button6.onClick()", "Sequence 86: ",
          "button1.onClick()button6.onClick()button7.onClick()", "Sequence 87: ",
          "button1.onClick()button6.onClick()button8.onClick()", "Sequence 88: ",
          "button1.onClick()button6.onClick()button9.onClick()", "Sequence 89: ",
          "button1.onClick()button6.onClick()button10.onClick()", "Sequence 90: ",
          "button1.onClick()button6.onClick()button11.onClick()", "Sequence 91: ",
          "button1.onClick()button6.onClick()button12.onClick()", "Sequence 92: ",
          "button1.onClick()button6.onClick()button13.onClick()" };
      expected = e;
    }
    if (verifyNoPropertyViolation("+event.strategy=default", config, config2)) {
      sb = new StringBuilder();
      Verify.setCounter(1, 2);
      Verify.setCounter(2, 0);

      AndroidEventProducer producer = new TestIncAndroidEventProducer();

      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));

      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));

      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));

      producer.getEventTreeString();

    }
  }

  @Test
  public void testTest5() {
    if (!isJPFRun()) {
      String[] e = { "Sequence 1: ", "button0.onClick()button0.onClick()", "Sequence 2: ",
          "button0.onClick()button1.onClick()", "Sequence 3: ", "button0.onClick()button2.onClick()",
          "Sequence 4: ", "button1.onClick()button0.onClick()", "Sequence 5: ",
          "button1.onClick()button1.onClick()", "Sequence 6: ", "button1.onClick()button2.onClick()",
          "Sequence 7: ", "button1.onClick()button3.onClick()" };
      expected = e;
    }
    // test stop due to mac depth reached
    if (verifyNoPropertyViolation("+event.strategy=default", config, "+event.max_depth=2", config2)) {
      sb = new StringBuilder();
      Verify.setCounter(1, 2);
      Verify.setCounter(2, 0);

      AndroidEventProducer producer = new TestIncAndroidEventProducer();
      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));

      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));

      producer.processNextEvent();
      Verify.incrementCounter(2);
      sb.append(Verify.getCounter(2));

      producer.getEventTreeString();

    }
  }

  @After
  public void takeDown() {
    if (!isJPFRun()) {
      // assertEquals(expected.length, Sequencer.sequence.size());

      int i = 0;
      for (String s : Sequencer.sequence) {
        System.out.println("\"" + s + "\", ");
        assertEquals("given: \"" + s + "\", expected: " + expected[i], expected[i], s);
        i++;
      }
    }
  }
}
