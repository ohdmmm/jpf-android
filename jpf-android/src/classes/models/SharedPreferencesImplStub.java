package models;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

public class SharedPreferencesImplStub implements android.content.SharedPreferences {
  public final WeakHashMap<OnSharedPreferenceChangeListener, Object> mListeners = new WeakHashMap<OnSharedPreferenceChangeListener, Object>();

  public SharedPreferencesImplStub(File f, int mode) {
  }

  public SharedPreferencesImplStub() {
  }

  @Override
  public boolean getBoolean(java.lang.String param0, boolean param1) {
    return param1;
  }

  @Override
  public long getLong(java.lang.String param0, long param1) {
    return param1;
  }

  @Override
  public android.content.SharedPreferences.Editor edit() {
    return new EditorImpl();
  }

  public void startReloadIfChangedUnexpectedly() {
    // do nothing
  }

  @Override
  public int getInt(java.lang.String param0, int param1) {
    return param1;
  }

  @Override
  public Map<String, ?> getAll() {
    return null;
  }

  @Override
  public String getString(String key, String defValue) {
    return defValue;
  }

  @Override
  public Set<String> getStringSet(String key, Set<String> defValues) {
    return defValues;
  }

  @Override
  public float getFloat(String key, float defValue) {
    return defValue;
  }

  @Override
  public boolean contains(String key) {
    return true;
  }

  @FilterField
  @NeverBreak
  private static final Object mContent = new Object();

  @Override
  public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.put(listener, mContent);
    }

  }

  @Override
  public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.remove(listener);
    }
  }

  public final class EditorImpl implements Editor {

    @Override
    public Editor putString(String key, String value) {
      return this;
    }

    @Override
    public Editor putStringSet(String key, Set<String> values) {
      return this;
    }

    @Override
    public Editor putInt(String key, int value) {
      return this;
    }

    @Override
    public Editor putLong(String key, long value) {
      return this;
    }

    @Override
    public Editor putFloat(String key, float value) {
      return this;
    }

    @Override
    public Editor putBoolean(String key, boolean value) {
      return this;
    }

    @Override
    public Editor remove(String key) {
      return this;
    }

    @Override
    public Editor clear() {
      return this;
    }

    @Override
    public void apply() {
    }

    @Override
    public boolean commit() {
      return true;
    }

  }

  @Override
  public Map<OnSharedPreferenceChangeListener, Object> getListeners() {
    return mListeners;
  }

}