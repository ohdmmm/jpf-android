/* Copyright (C) 2003 Vladimir Roubtsov. All rights reserved.
 * 
 * This program and the accompanying materials are made available under
 * the terms of the Common Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/cpl-v10.html
 * 
 * $Id: RT.java,v 1.2.2.3 2004/07/16 23:32:03 vlad_r Exp $
 */
package com.vladium.emma.rt;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.vladium.emma.data.ICoverageData;
import com.vladium.util.IProperties;

/**
 * The class models the RT.class called from applications instrumented with EMMA
 * Coverage Analyzer. It stores the coverage data in a map "c". The map maps a
 * classname to a boolean array storing the coverage of each basic block in the
 * class.
 * 
 * @author Heila vd Merwe
 * @version
 *
 */
public abstract class RT {

  @FilterField
  @NeverBreak
  private static final Map<String, Object[]> c = new HashMap<String, Object[]>();

  private RT() {
  } // prevent subclassing

  public static synchronized void dumpCoverageData(File outFile, final boolean merge,
                                                   final boolean stopDataCollection) {
    throw new UnsupportedOperationException("Not modeled yet");
  }

  public static synchronized void r(final boolean[][] coverage, final String classVMName,
                                    final long stamp) {
    Object[] params = { coverage, classVMName, stamp };
    c.put(classVMName, params);
  }

  public static synchronized ICoverageData reset(final boolean createCoverageData,
                                                 final boolean createExitHook) {
   // c = new HashMap<String, Object[]>();
    return null;
  }

  public static synchronized ICoverageData getCoverageData() {
    throw new UnsupportedOperationException("Not modeled yet");
  }

  public static synchronized IProperties getAppProperties() {
    throw new UnsupportedOperationException("Not modeled yet");
  }

  public static synchronized void dumpCoverageData(File outFile, final boolean stopDataCollection) {
    throw new UnsupportedOperationException("Not modeled yet");
  }

  private static File getCoverageOutFile() {
    throw new UnsupportedOperationException("Not modeled yet");
  }

  private static boolean getCoverageOutMerge() {
    throw new UnsupportedOperationException("Not modeled yet");
  }

}