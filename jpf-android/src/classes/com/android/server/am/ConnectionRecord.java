package com.android.server.am;

import android.content.Intent;
import android.content.ServiceConnection;

class ConnectionRecord {
  final ActivityRecord activity; // If non-null, the owning activity.
  final ServiceConnection conn; // The client connection.
  final int flags; // Binding options.
  final int clientLabel; // String resource labeling this client.
  final Intent clientIntent; // How to launch the client.
  String stringName; // Caching of toString.
  boolean serviceDead; // Well is it?
  /** Binder published from service. */
  ServiceRecord service;

  public ConnectionRecord(ActivityRecord activity, ServiceConnection conn, int flags, int clientLabel,
      Intent clientIntent, String stringName, boolean serviceDead, ServiceRecord service) {
    super();
    this.activity = activity;
    this.conn = conn;
    this.flags = flags;
    this.clientLabel = clientLabel;
    this.clientIntent = clientIntent;
    this.stringName = stringName;
    this.serviceDead = serviceDead;
    this.service = service;
  }

  @Override
  public String toString() {
    return "ConnectionRecord [activity=" + activity + ", conn=" + conn + ", service=" + service + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((activity == null) ? 0 : activity.hashCode());
    result = prime * result + ((conn == null) ? 0 : conn.hashCode());
    result = prime * result + ((service == null) ? 0 : service.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ConnectionRecord other = (ConnectionRecord) obj;
    if (activity == null) {
      if (other.activity != null)
        return false;
    } else if (!activity.equals(other.activity))
      return false;
    if (conn == null) {
      if (other.conn != null)
        return false;
    } else if (!conn.equals(other.conn))
      return false;
    if (service == null) {
      if (other.service != null)
        return false;
    } else if (!service.equals(other.service))
      return false;
    return true;
  }

}
