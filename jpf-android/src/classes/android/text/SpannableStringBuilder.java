package android.text;

import android.graphics.Canvas;
import android.graphics.Paint;

public class SpannableStringBuilder implements CharSequence, GetChars, Spannable, Editable, Appendable,
    GraphicsOperations {

  public static final SpannableStringBuilder TOP = new SpannableStringBuilder();

  CharSequence seq;
  /**
   * Create a new SpannableStringBuilder with empty contents
   */
  public SpannableStringBuilder() {
      this("");
  }

  /**
   * Create a new SpannableStringBuilder containing a copy of the
   * specified text, including its spans if any.
   */
  public SpannableStringBuilder(CharSequence text) {
    this.seq = text;
  }

  
  @Override
  public <T> T[] getSpans(int start, int end, Class<T> type) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public int getSpanStart(Object tag) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getSpanEnd(Object tag) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getSpanFlags(Object tag) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int nextSpanTransition(int start, int limit, Class type) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public void drawText(Canvas c, int start, int end, float x, float y, Paint p) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void drawTextRun(Canvas c, int start, int end, int contextStart, int contextEnd, float x, float y,
                          int flags, Paint p) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public float measureText(int start, int end, Paint p) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int getTextWidths(int start, int end, float[] widths, Paint p) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public float getTextRunAdvances(int start, int end, int contextStart, int contextEnd, int flags,
                                  float[] advances, int advancesIndex, Paint paint) {
    // TODO Auto-generated method stub
    return 0;
  }



  @Override
  public int getTextRunCursor(int contextStart, int contextEnd, int flags, int offset, int cursorOpt, Paint p) {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public Editable replace(int st, int en, CharSequence source, int start, int end) {
    return TOP;
  }

  @Override
  public Editable replace(int st, int en, CharSequence text) {
    return TOP;
  }

  @Override
  public Editable insert(int where, CharSequence text, int start, int end) {
    return TOP;
  }

  @Override
  public Editable insert(int where, CharSequence text) {
    return TOP;
  }

  @Override
  public Editable delete(int st, int en) {
    return TOP;
  }

  @Override
  public Editable append(CharSequence text) {
    return TOP;
  }

  @Override
  public Editable append(CharSequence text, int start, int end) {
    return TOP;
  }

  @Override
  public Editable append(char text) {
    return TOP;
  }

  @Override
  public void clear() {
  }

  @Override
  public void clearSpans() {
  }

  @Override
  public void setFilters(InputFilter[] filters) {
  }

  @Override
  public InputFilter[] getFilters() {
    return null;
  }

  @Override
  public void setSpan(Object what, int start, int end, int flags) {
    
  }

  @Override
  public void removeSpan(Object what) {
    
  }

  @Override
  public void getChars(int start, int end, char[] dest, int destoff) {
    
  }

  @Override
  public int length() {
    return 0;
  }

  @Override
  public char charAt(int index) {
    return 0;
  }

  @Override
  public CharSequence subSequence(int start, int end) {
    return null;
  }

  @Override
  public String toString() {
    return seq.toString();
  }

  
}
