package android.app;

public final class PendingIntent {
  public static android.app.PendingIntent TOP = new android.app.PendingIntent();


  public PendingIntent(){
  }

  public static android.app.PendingIntent getBroadcast(android.content.Context param0, int param1, android.content.Intent param2, int param3){
    return android.app.PendingIntent.TOP;
  }

  public static android.app.PendingIntent getActivity(android.content.Context param0, int param1, android.content.Intent param2, int param3){
    return android.app.PendingIntent.TOP;
  }
}