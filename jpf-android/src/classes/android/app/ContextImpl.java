/*
 * Copyright (C) 2006 The Android Open Source Project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import models.DefaultSharedPreferences;
import models.SharedPreferencesImplStub;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.ReceiverCallNotAllowedException;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.util.AndroidRuntimeException;
import android.util.AttributeSetImpl;
import android.util.Log;
import android.view.Display;
import android.view.DisplayAdjustments;
import android.view.LayoutInflater;

import com.android.internal.util.Preconditions;

/**
 * Restricted Context given to BroadcastReceivers to make sure they can't bind to a service or register a
 * receiver using a handler
 * 
 * 
 */
class ReceiverRestrictedContext extends ContextWrapper {
  ReceiverRestrictedContext(Context base) {
    super(base);
  }

  @Override
  public Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter) {
    return registerReceiver(receiver, filter, null, null);
  }

  @Override
  public Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter, String broadcastPermission, Handler scheduler) {
    throw new ReceiverCallNotAllowedException("IntentReceiver components are not allowed to register to receive intents");
  }

  @Override
  public boolean bindService(Intent service, ServiceConnection conn, int flags) {
    throw new ReceiverCallNotAllowedException("IntentReceiver components are not allowed to bind to services");
  }
}

/**
 * Common implementation of Context API, which provides the base context object for Activity and other
 * application components.
 */
public class ContextImpl extends Context {
  private final static String TAG = "ContextImpl";
  private final static boolean DEBUG_CONTEXT = true;

  private SharedPreferences preferences;

  /* package */LoadedApk mLoadedPackageInfo;
  private String mBasePackageName;
  private Resources mResources;
  /* package */ActivityThread mMainThread;
  private Context mOuterContext;
  private IBinder mActivityToken = null;
  // private int mThemeResource = 0;
  @FilterField
  @NeverBreak
  private Resources.Theme mTheme = null;
  private PackageManager mPackageManager;
  @FilterField
  private Context mReceiverRestrictedContext = null;

  // private boolean mRestricted;

  // lock for shared prefs, files and database
  private ContentResolver mContentResolver;

  // private File mDatabasesDir;
  // private File mPreferencesDir;
  // private File mFilesDir;
  // private File mCacheDir;
  // private File mObbDir;
  // private File mExternalFilesDir;
  // private File mExternalCacheDir;
  //
  // private static final String[] EMPTY_FILE_LIST = {};

  public ContextImpl() {
    mOuterContext = this;

    if (DEBUG_CONTEXT)
      Log.i(TAG, "Creating new Context.");
  }

  final void init(LoadedApk packageInfo, IBinder activityToken, ActivityThread mainThread) {
    init(packageInfo, activityToken, mainThread, null, null);
  }

  final void init(LoadedApk loadedPackageInfo, IBinder activityToken, ActivityThread mainThread, Resources container, String basePackageName) {
    String useDefaultPrefs = AndroidVerify.getConfigString("jpf-android.prefs", "normal");
    if (useDefaultPrefs.equals("normal")) {
      preferences = new SharedPreferencesImpl();
    } else if (useDefaultPrefs.equals("stub")) {
      preferences = new SharedPreferencesImplStub();
    } else { // default
      preferences = new DefaultSharedPreferences();
    }

    mLoadedPackageInfo = loadedPackageInfo;
    mBasePackageName = basePackageName != null ? basePackageName : loadedPackageInfo.mPackageName;
    mResources = mLoadedPackageInfo.getResources(mainThread);

    // if (mResources != null
    // && container != null
    // && container.getCompatibilityInfo().applicationScale !=
    // mResources.getCompatibilityInfo().applicationScale) {
    // if (DEBUG) {
    // Log.d(TAG, "loaded context has different scaling. Using container's" +
    // " compatiblity info:"
    // + container.getDisplayMetrics());
    // }
    // // mResources = mainThread
    // // .getTopLevelResources(mPackageInfo.getResDir(),
    // container.getCompatibilityInfo());
    // }
    mMainThread = mainThread;
    mContentResolver = new ApplicationContentResolver(this, mainThread, null);

    setActivityToken(activityToken);
    if (DEBUG_CONTEXT)
      Log.i(TAG, "Setting up Context with Resources=" + mResources);

  }

  public final void init(Resources resources, ActivityThread mainThread) {
    mLoadedPackageInfo = null;
    mBasePackageName = null;
    mResources = resources;
    mMainThread = mainThread;
    // mContentResolver = new ApplicationContentResolver(this, mainThread);
  }

  final void scheduleFinalCleanup(String who, String what) {
    mMainThread.scheduleContextCleanup(this, who, what);
  }

  final void performFinalCleanup(String who, String what) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "Cleanup up context: " + this);
    mLoadedPackageInfo.removeContextRegistrations(getOuterContext(), who, what);
  }

  final Context getReceiverRestrictedContext() {
    if (mReceiverRestrictedContext != null) {
      return mReceiverRestrictedContext;
    }
    return mReceiverRestrictedContext = new ReceiverRestrictedContext(getOuterContext());
  }

  final void setActivityToken(IBinder token) {
    mActivityToken = token;
  }

  public final void setOuterContext(Context context) {
    mOuterContext = context;
  }

  final Context getOuterContext() {
    return mOuterContext;
  }

  final IBinder getActivityToken() {
    return mActivityToken;
  }

  @Override
  public AssetManager getAssets() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }

  @Override
  public Resources getResources() {
    return mResources;
  }

  @Override
  public PackageManager getPackageManager() {
    if (mPackageManager == null) {
      mPackageManager = ActivityThread.getPackageManager();
    }
    return mPackageManager;

  }

  @Override
  public ContentResolver getContentResolver() {
    return mContentResolver;
  }

  @Override
  public Looper getMainLooper() {
    return mMainThread.getLooper();
  }

  @Override
  public Context getApplicationContext() {
    return (mLoadedPackageInfo != null) ? mLoadedPackageInfo.getApplication() : mMainThread.getApplication();
  }

  @Override
  public void setTheme(int resid) {
    // throw new UnsupportedOperationException();
  }

  @Override
  public Theme getTheme() {
    if (mTheme == null) {
      mTheme = mResources.newTheme();
    }
    System.out.println("Resources.getTheme:" + mTheme);
    mTheme.obtainStyledAttributes(new AttributeSetImpl(), null, 0, 0);
    return mTheme;
  }

  // TODO @Override
  // public Resources.Theme getTheme() {
  // if (mTheme == null) {
  // mThemeResource = Resources.selectDefaultTheme(mThemeResource,
  // getOuterContext().getApplicationInfo().targetSdkVersion);
  // mTheme = mResources.newTheme();
  // mTheme.applyStyle(mThemeResource, true);
  // }
  // return mTheme;
  // }

  @Override
  public ClassLoader getClassLoader() {
    return mLoadedPackageInfo != null ? mLoadedPackageInfo.getClassLoader() : ClassLoader.getSystemClassLoader();
  }

  @Override
  public String getPackageName() {
    if (mLoadedPackageInfo != null) {
      return mLoadedPackageInfo.getPackageName();
    }
    throw new RuntimeException("Not supported in system context");
  }

  @Override
  public ApplicationInfo getApplicationInfo() {
    if (mLoadedPackageInfo != null) {
      return mLoadedPackageInfo.getApplicationInfo();
    }
    throw new RuntimeException("Not supported in system context");
  }

  @Override
  public String getPackageResourcePath() {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getPackageCodePath() {
    // if (mPackageInfo != null) {
    // return mPackageInfo.getAppDir();
    // }
    throw new UnsupportedOperationException();
  }

  @Override
  public File getSharedPrefsFile(String name) {
    return null;// TODO makeFilename(getPreferencesDir(), name + ".xml");
  }

  @Override
  public SharedPreferences getSharedPreferences(String name, int mode) {
    // SharedPreferencesImpl sp;
    // synchronized (sSharedPrefs) {
    // sp = sSharedPrefs.get(name);
    // if (sp == null) {
    // sp = new SharedPreferencesImpl(null, mode);
    // sSharedPrefs.put(name, sp);
    // if (DEBUG_CONTEXT)
    // Log.i(TAG, "Returning Shared Prefs (name=" + name + " found=" + (sp != null) + ")");
    // }
    // }
    //
    // if (DEBUG_CONTEXT)
    // Log.i(TAG, "Returning Shared Prefs (name=" + name + " found=" + (sp != null) + ")");
    return preferences;
  }

  private File getPreferencesDir() {
    return new File("/storage/SharedPrefs");
  }

  @Override
  public FileOutputStream openFileOutput(String name, int mode) throws FileNotFoundException {
    return new FileOutputStream("/storage/cache/" + name);

  }

  @Override
  public boolean deleteFile(String name) {
    throw new UnsupportedOperationException();
  }

  @Override
  public File getFileStreamPath(String name) {
    throw new UnsupportedOperationException();
  }

  @Override
  public File getFilesDir() {
    return new File("/storage/cache");

  }

  @Override
  public FileInputStream openFileInput(String name) throws FileNotFoundException {
    return new FileInputStream("/storage/cache/" + name);
  }

  @Override
  public File getExternalFilesDir(String type) {
    throw new UnsupportedOperationException();
  }

  @Override
  public File getObbDir() {
    throw new UnsupportedOperationException();
  }

  @Override
  public File getCacheDir() {
    File f = new File("/storage/cache");
    if (!f.exists()) {
      f.mkdir();
    }
    return f;
  }

  @Override
  public File getExternalCacheDir() {
    File f = new File("/storage/cache");
    if (!f.exists()) {
      f.mkdir();
    }
    return f;
  }

  @Override
  public String[] fileList() {
    throw new UnsupportedOperationException();
  }

  @Override
  public File getDir(String name, int mode) {
    throw new UnsupportedOperationException();
  }

  @Override
  public SQLiteDatabase openOrCreateDatabase(String name, int mode, CursorFactory factory) {
    SQLiteDatabase sqd = new SQLiteDatabase();
    return sqd;
  }

  @Override
  public SQLiteDatabase openOrCreateDatabase(String name, int mode, CursorFactory factory, DatabaseErrorHandler errorHandler) {
    SQLiteDatabase sqd = new SQLiteDatabase();
    return sqd;

  }

  @Override
  public boolean deleteDatabase(String name) {
    throw new UnsupportedOperationException();

  }

  @Override
  public File getDatabasePath(String name) {
    throw new UnsupportedOperationException();

  }

  @Override
  public String[] databaseList() {
    throw new UnsupportedOperationException();

  }

  @Override
  @Deprecated
  public Drawable getWallpaper() {
    throw new UnsupportedOperationException();
  }

  @Override
  @Deprecated
  public Drawable peekWallpaper() {
    throw new UnsupportedOperationException();
  }

  @Override
  @Deprecated
  public int getWallpaperDesiredMinimumWidth() {
    throw new UnsupportedOperationException();
  }

  @Override
  @Deprecated
  public int getWallpaperDesiredMinimumHeight() {
    throw new UnsupportedOperationException();
  }

  @Override
  @Deprecated
  public void setWallpaper(Bitmap bitmap) throws IOException {
    throw new UnsupportedOperationException();

  }

  @Override
  @Deprecated
  public void setWallpaper(InputStream data) throws IOException {
    throw new UnsupportedOperationException();

  }

  @Override
  public void startActivity(Intent intent) {
    if (true) { // TODO (intent.getFlags() & Intent.FLAG_ACTIVITY_NEW_TASK) == 0
      throw new AndroidRuntimeException("Calling startActivity() from outside of an Activity " + " context requires the FLAG_ACTIVITY_NEW_TASK flag."
          + " Is this really what you want? This is not supported in jpf-android");
    }
    // mMainThread.getInstrumentation().execStartActivity(getOuterContext(),
    // null, null, (Activity) null,
    // intent, -1);

  }

  @Override
  public void startActivities(Intent[] intents) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void startIntentSender(IntentSender intent, Intent fillInIntent, int flagsMask, int flagsValues, int extraFlags)
      throws IntentSender.SendIntentException {
    throw new UnsupportedOperationException();
  }

  @Override
  public void sendBroadcast(Intent intent) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "sendBroadcast(intent=" + intent + ")");
    // TODO String resolvedType =
    // intent.resolveTypeIfNeeded(getContentResolver());
    try {
      intent.setAllowFds(false);
      ActivityManagerNative.getDefault().broadcastIntent(intent, null, null, Activity.RESULT_OK, null, null, null, false, false);
    } catch (RemoteException e) {
    }
  }

  @Override
  public void sendBroadcast(Intent intent, String receiverPermission) {
    // String resolvedType = intent.resolveTypeIfNeeded(getContentResolver());
    if (DEBUG_CONTEXT)
      Log.i(TAG, "sendBroadcast(intent=" + intent + " permission=" + receiverPermission + ")");
    try {
      intent.setAllowFds(false);
      ActivityManagerNative.getDefault().broadcastIntent(intent, null, null, Activity.RESULT_OK, null, null, receiverPermission, false, false);
    } catch (RemoteException e) {
    }
  }

  @Override
  public void sendOrderedBroadcast(Intent intent, String receiverPermission) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "sendOrderedBroadcast(intent=" + intent + " permission=" + receiverPermission + ")");
    // String resolvedType = intent.resolveTypeIfNeeded(getContentResolver());
    try {
      intent.setAllowFds(false);
      ActivityManagerNative.getDefault().broadcastIntent(intent, null, null, Activity.RESULT_OK, null, null, receiverPermission, true, false);
    } catch (RemoteException e) {
    }
  }

  @Override
  public void sendOrderedBroadcast(Intent intent, String receiverPermission, BroadcastReceiver resultReceiver, Handler scheduler, int initialCode,
                                   String initialData, Bundle initialExtras) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "sendOrderedBroadcast(intent=" + intent + " permission=" + receiverPermission + ")");
    IIntentReceiver rd = null;
    if (resultReceiver != null) {
      if (mLoadedPackageInfo != null) {
        if (scheduler == null) {
          scheduler = mMainThread.getHandler();
        }
        rd = mLoadedPackageInfo.getReceiverDispatcher(resultReceiver, getOuterContext(), scheduler, mMainThread.getInstrumentation(), false);
      } else {
        if (scheduler == null) {
          scheduler = mMainThread.getHandler();
        }
        rd = new LoadedApk.ReceiverDispatcher(resultReceiver, getOuterContext(), scheduler, null, false).getIIntentReceiver();
      }
    }
    String resolvedType = intent.resolveTypeIfNeeded(getContentResolver());
    try {
      intent.setAllowFds(false);
      ActivityManagerNative.getDefault().broadcastIntent(intent, resolvedType, rd, initialCode, initialData, initialExtras, receiverPermission, true,
          false);
    } catch (RemoteException e) {
    }
  }

  @Override
  public void sendStickyBroadcast(Intent intent) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "sendStickyBroadcast(intent=" + intent + ")");
    try {
      intent.setAllowFds(false);
      ActivityManagerNative.getDefault().broadcastIntent(intent, null, null, Activity.RESULT_OK, null, null, null, false, true);
    } catch (RemoteException e) {
    }
  }

  @Override
  public void sendStickyOrderedBroadcast(Intent intent, BroadcastReceiver resultReceiver, Handler scheduler, int initialCode, String initialData,
                                         Bundle initialExtras) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "sendStickyOrderedBroadcast(intent=" + intent + ")");

    IIntentReceiver rd = null;
    if (resultReceiver != null) {
      if (mLoadedPackageInfo != null) {
        if (scheduler == null) {
          scheduler = mMainThread.getHandler();
        }
        rd = mLoadedPackageInfo.getReceiverDispatcher(resultReceiver, getOuterContext(), scheduler, mMainThread.getInstrumentation(), false);
      } else {
        if (scheduler == null) {
          scheduler = mMainThread.getHandler();
        }
        rd = new LoadedApk.ReceiverDispatcher(resultReceiver, getOuterContext(), scheduler, null, false).getIIntentReceiver();
      }
    }
    String resolvedType = intent.resolveTypeIfNeeded(getContentResolver());
    try {
      intent.setAllowFds(false);
      ActivityManagerNative.getDefault().broadcastIntent(intent, resolvedType, rd, initialCode, initialData, initialExtras, null, true, true);
    } catch (RemoteException e) {
    }
  }

  @Override
  public void removeStickyBroadcast(Intent intent) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "removeStickyBroadcast(intent=" + intent + ")");

    String resolvedType = intent.resolveTypeIfNeeded(getContentResolver());
    if (resolvedType != null) {
      intent = new Intent(intent);
      intent.setDataAndType(intent.getData(), resolvedType);
    }
    try {
      intent.setAllowFds(false);
      ActivityManagerNative.getDefault().unbroadcastIntent(mMainThread.getApplicationThread(), intent);
    } catch (RemoteException e) {
    }
  }

  @Override
  public Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter) {
    return registerReceiver(receiver, filter, null, null);
  }

  @Override
  public Intent registerReceiver(BroadcastReceiver receiver, IntentFilter filter, String broadcastPermission, Handler scheduler) {
    return registerReceiverInternal(receiver, filter, broadcastPermission, scheduler, getOuterContext());
  }

  private Intent registerReceiverInternal(BroadcastReceiver receiver, IntentFilter filter, String broadcastPermission, Handler scheduler,
                                          Context context) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "registerReceiver(receiver=" + receiver + " filter=" + filter + " permission=" + broadcastPermission + ")");

    IIntentReceiver rd = null;
    if (receiver != null) {
      if (mLoadedPackageInfo != null && context != null) {
        if (scheduler == null) {
          scheduler = mMainThread.getHandler();
        }
        rd = mLoadedPackageInfo.getReceiverDispatcher(receiver, context, scheduler, mMainThread.getInstrumentation(), true);
      } else {
        if (scheduler == null) {
          scheduler = mMainThread.getHandler();
        }
        rd = new LoadedApk.ReceiverDispatcher(receiver, context, scheduler, null, true).getIIntentReceiver();
      }
    }
    try {
      return ActivityManagerNative.getDefault().registerReceiver(mMainThread.getApplicationThread(), mBasePackageName, rd, filter,
          broadcastPermission);
    } catch (RemoteException e) {
      return null;
    }
  }

  @Override
  public void unregisterReceiver(BroadcastReceiver receiver) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "unregisterReceiver(receiver=" + receiver + ")");

    if (mLoadedPackageInfo != null) {
      IIntentReceiver rd = mLoadedPackageInfo.forgetReceiverDispatcher(getOuterContext(), receiver);
      try {
        ActivityManagerNative.getDefault().unregisterReceiver(rd);
      } catch (RemoteException e) {
      }
    } else {
      throw new RuntimeException("Not supported in system context");
    }
  }

  @Override
  public ComponentName startService(Intent service) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, mOuterContext.getClass().getSimpleName() + " startService(Intent=" + service + ")");

    service.setAllowFds(false);
    ComponentName cn = ActivityManagerNative.getDefault().startService(service, "");
    if (cn != null && cn.getPackageName().equals("!")) {
      throw new SecurityException("Not allowed to start service " + service + " without permission " + cn.getClassName());
    }
    return cn;
  }

  @Override
  public boolean stopService(Intent service) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, " stopService(Intent=" + service + ")");

    service.setAllowFds(false);
    int res = ActivityManagerNative.getDefault().stopService(service, "");
    if (res < 0) {
      throw new SecurityException("Not allowed to stop service " + service);
    }
    return res != 0;

  }

  @Override
  public boolean bindService(Intent service, ServiceConnection conn, int flags) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, mOuterContext.getClass().getSimpleName() + " bindService(Intent=" + service + ")");

    // ServiceConnection sd;
    // if (mPackageInfo != null) {
    // sd = mPackageInfo.getServiceDisPapatcher(conn, getOuterContext(),
    // mMainThread.getHandler(), flags);
    // } else {
    // throw new RuntimeException("Not supported in system context");
    // }
    // IBinder token = getActivityToken();
    // if (token == null
    // && (flags & BIND_AUTO_CREATE) == 0
    // && mPackageInfo != null
    // && mPackageInfo.getApplicationInfo().targetSdkVersion <
    // android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
    // flags |= BIND_WAIVE_PRIORITY;
    // }
    // service.setAllowFds(false);
    int res = ActivityManagerNative.getDefault().bindService(getActivityToken(), service, conn, flags);
    // if (res < 0) {
    // throw new SecurityException("Not allowed to bind to service " + service);
    // }
    // return res != 0;
    return true;
  }

  @Override
  public void unbindService(ServiceConnection conn) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, mOuterContext.getClass().getSimpleName() + " unbindService(ServiceConnection=" + conn + ")");

    if (mLoadedPackageInfo != null) {
      // IServiceConnection sd = mPackageInfo.forgetServiceDispatcher(
      // getOuterContext(), conn);
      ActivityManagerNative.getDefault().unbindService(conn);
    } else {
      throw new RuntimeException("Not supported in system context");
    }
  }

  @Override
  public boolean startInstrumentation(ComponentName className, String profileFile, Bundle arguments) {
    // TODO
    return false;
  }

  @FilterField
  @NeverBreak
  LayoutInflater l = new LayoutInflater(this);

  @Override
  public Object getSystemService(String name) {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "getSystemService(name=" + name + ")");
    if (name.equals(Context.LAYOUT_INFLATER_SERVICE))
      return l;
    return ServiceManager.getSystemService(name);
  }

  @Override
  public int checkPermission(String permission, int pid, int uid) {
    throw new UnsupportedOperationException();
  }

  @Override
  public int checkCallingPermission(String permission) {
    throw new UnsupportedOperationException();
  }

  @Override
  public int checkCallingOrSelfPermission(String permission) {
    return PackageManager.PERMISSION_GRANTED;
  }

  @Override
  public void enforcePermission(String permission, int pid, int uid, String message) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void enforceCallingPermission(String permission, String message) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void enforceCallingOrSelfPermission(String permission, String message) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void grantUriPermission(String toPackage, Uri uri, int modeFlags) {
    throw new UnsupportedOperationException();

  }

  @Override
  public void revokeUriPermission(Uri uri, int modeFlags) {
    throw new UnsupportedOperationException();
  }

  @Override
  public int checkUriPermission(Uri uri, int pid, int uid, int modeFlags) {
    throw new UnsupportedOperationException();
  }

  @Override
  public int checkCallingUriPermission(Uri uri, int modeFlags) {
    throw new UnsupportedOperationException();
  }

  @Override
  public int checkCallingOrSelfUriPermission(Uri uri, int modeFlags) {
    throw new UnsupportedOperationException();
  }

  @Override
  public int checkUriPermission(Uri uri, String readPermission, String writePermission, int pid, int uid, int modeFlags) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void enforceUriPermission(Uri uri, int pid, int uid, int modeFlags, String message) {
    throw new UnsupportedOperationException();

  }

  @Override
  public void enforceCallingUriPermission(Uri uri, int modeFlags, String message) {
    throw new UnsupportedOperationException();

  }

  @Override
  public void enforceCallingOrSelfUriPermission(Uri uri, int modeFlags, String message) {
    throw new UnsupportedOperationException();

  }

  @Override
  public void enforceUriPermission(Uri uri, String readPermission, String writePermission, int pid, int uid, int modeFlags, String message) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Context createPackageContext(String packageName, int flags) throws PackageManager.NameNotFoundException {
    if (DEBUG_CONTEXT)
      Log.i(TAG, "createPackageContext(packageName=" + packageName + " flags=" + flags + ")");

    LoadedApk pi = mMainThread.getPackageInfo(packageName);
    if (pi != null) {
      ContextImpl c = new ContextImpl();
      // c.mRestricted = (flags & CONTEXT_RESTRICTED) == CONTEXT_RESTRICTED;
      c.init(pi, null, mMainThread, mResources, mBasePackageName);
      if (c.mResources != null) {
        return c;
      }
    }

    // Should be a better exception.
    throw new PackageManager.NameNotFoundException("Application package " + packageName + " not found");
  }

  /**
   * Creates a context for the Android OS. The main thead is the main thread of the system to schedule
   * messages in etc.
   * 
   * @param mainThread
   * @return
   */
  public static ContextImpl createSystemContext(ActivityThread thread) {
    ContextImpl context = new ContextImpl();
    return context;
  }

  @Override
  public void clearWallpaper() throws IOException {
    // TODO Auto-generated method stub

  }

  @Override
  public Context createConfigurationContext(Configuration arg0) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Context createDisplayContext(Display arg0) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Context createPackageContextAsUser(String arg0, int arg1, UserHandle arg2) throws NameNotFoundException {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getBasePackageName() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public DisplayAdjustments getDisplayAdjustments(int arg0) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public File[] getExternalCacheDirs() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public File[] getExternalFilesDirs(String arg0) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public File[] getObbDirs() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getOpPackageName() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public int getUserId() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public Intent registerReceiverAsUser(BroadcastReceiver arg0, UserHandle arg1, IntentFilter arg2, String arg3, Handler arg4) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void removeStickyBroadcastAsUser(Intent arg0, UserHandle arg1) {
    // TODO Auto-generated method stub

  }

  @Override
  public void sendBroadcast(Intent arg0, String arg1, int arg2) {
    // TODO Auto-generated method stub

  }

  @Override
  public void sendBroadcastAsUser(Intent arg0, UserHandle arg1) {
    // TODO Auto-generated method stub

  }

  @Override
  public void sendBroadcastAsUser(Intent arg0, UserHandle arg1, String arg2) {
    // TODO Auto-generated method stub

  }

  @Override
  public void sendOrderedBroadcast(Intent arg0, String arg1, int arg2, BroadcastReceiver arg3, Handler arg4, int arg5, String arg6, Bundle arg7) {
    // TODO Auto-generated method stub

  }

  @Override
  public void sendOrderedBroadcastAsUser(Intent arg0, UserHandle arg1, String arg2, BroadcastReceiver arg3, Handler arg4, int arg5, String arg6,
                                         Bundle arg7) {
    // TODO Auto-generated method stub

  }

  @Override
  public void sendStickyBroadcastAsUser(Intent arg0, UserHandle arg1) {
    // TODO Auto-generated method stub

  }

  @Override
  public void sendStickyOrderedBroadcastAsUser(Intent arg0, UserHandle arg1, BroadcastReceiver arg2, Handler arg3, int arg4, String arg5, Bundle arg6) {
    // TODO Auto-generated method stub

  }

  @Override
  public void startActivities(Intent[] arg0, Bundle arg1) {
    // TODO Auto-generated method stub

  }

  @Override
  public void startActivity(Intent arg0, Bundle arg1) {
    // TODO Auto-generated method stub

  }

  @Override
  public void startIntentSender(IntentSender arg0, Intent arg1, int arg2, int arg3, int arg4, Bundle arg5) throws SendIntentException {
    // TODO Auto-generated method stub

  }

  @Override
  public ComponentName startServiceAsUser(Intent arg0, UserHandle arg1) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean stopServiceAsUser(Intent arg0, UserHandle arg1) {
    // TODO Auto-generated method stub
    return false;
  }

  // ----------------------------------------------------------------------
  // ----------------------------------------------------------------------
  // ----------------------------------------------------------------------

  private static final class ApplicationContentResolver extends ContentResolver {
    private final ActivityThread mMainThread;
    private UserHandle mUser;

    public ApplicationContentResolver(Context context, ActivityThread mainThread, UserHandle user) {
      super(context);
      mMainThread = Preconditions.checkNotNull(mainThread);
    }

    @Override
    public ContentProvider acquireProvider(String auth) {
      return mMainThread.acquireProvider(mContext, auth);
    }

    @Override
    public ContentProvider acquireProvider(Uri param0) {
      return mMainThread.acquireProvider(mContext, param0.getAuthority());
    }

  }

}
