package android.app;

import gov.nasa.jpf.annotation.FilterField;


public class ActivityManagerNative {

  @FilterField
  public static IActivityManager mgr;

  public static IActivityManager getDefault() {
    
    return mgr;
  }

}
