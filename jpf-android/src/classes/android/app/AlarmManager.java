package android.app;

import gov.nasa.jpf.util.event.EventProcessor;
import gov.nasa.jpf.util.event.InvalidEventException;
import gov.nasa.jpf.util.event.events.Event;

import java.util.HashMap;
import java.util.Map;

public class AlarmManager implements EventProcessor {
  private static final String TAG = "AlarmManager";

  /**
   * Alarm time in {@link System#currentTimeMillis System.currentTimeMillis()} (wall clock time in UTC), which
   * will wake up the device when it goes off.
   */
  public static final int RTC_WAKEUP = 0;
  /**
   * Alarm time in {@link System#currentTimeMillis System.currentTimeMillis()} (wall clock time in UTC). This
   * alarm does not wake the device up; if it goes off while the device is asleep, it will not be delivered
   * until the next time the device wakes up.
   */
  public static final int RTC = 1;
  /**
   * Alarm time in {@link android.os.SystemClock#elapsedRealtime SystemClock.elapsedRealtime()} (time since
   * boot, including sleep), which will wake up the device when it goes off.
   */
  public static final int ELAPSED_REALTIME_WAKEUP = 2;
  /**
   * Alarm time in {@link android.os.SystemClock#elapsedRealtime SystemClock.elapsedRealtime()} (time since
   * boot, including sleep). This alarm does not wake the device up; if it goes off while the device is
   * asleep, it will not be delivered until the next time the device wakes up.
   */
  public static final int ELAPSED_REALTIME = 3;

  public Map<Object, Object> alarms = new HashMap<Object, Object>();

  public AlarmManager() {
  }

  public void cancel(android.app.PendingIntent param0) {
  }

  public void setRepeating(int param0, long param1, long param2, android.app.PendingIntent param3) {
  }

  @Override
  public Event[] getEvents() {
    return null;
  }

  @Override
  public void processEvent(Event event) throws InvalidEventException {
  }

  public void set(int param0, long param1, android.app.PendingIntent param2) {
  }
}