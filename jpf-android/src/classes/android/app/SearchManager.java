package android.app;

import android.content.Intent;

public class SearchManager {

  public void search(Activity c) {
    // get activity

    String searchClass = "com.keepassdroid.search.SearchResults";

    Intent i = null;
    try {
      i = new Intent(c, Class.forName(searchClass));
      // and launch
      c.startActivity(i);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }


}
