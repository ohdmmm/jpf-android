package android.app;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.UIEvent;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class AlertDialog extends Dialog {
  public static final int THEME_TRADITIONAL = 1;
  public static final int THEME_HOLO_DARK = 2;
  public static final int THEME_HOLO_LIGHT = 3;
  public static final int THEME_DEVICE_DEFAULT_DARK = 4;
  public static final int THEME_DEVICE_DEFAULT_LIGHT = 5;

  public AlertButton buttonPositive;
  public AlertButton buttonNegative;
  public AlertButton buttonNeutral;

  protected AlertDialog(Context context) {
    super(context, false, (DialogInterface.OnCancelListener) null);
  }

  protected AlertDialog(Context context, int theme) {
    super(context, false, (DialogInterface.OnCancelListener) null);
  }

  protected AlertDialog(Context context, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
    super(context, cancelable, cancelListener);
  }

  public Button getButton(int whichButton) {
    Button b = null;
    switch (whichButton) {
    case DialogInterface.BUTTON_POSITIVE: {
      b = buttonPositive;
      break;
    }
    case DialogInterface.BUTTON_NEGATIVE: {
      b = buttonNegative;
      break;
    }
    case DialogInterface.BUTTON_NEUTRAL: {
      b = buttonNeutral;
      break;
    }
    }
    return b;
  }

  public ListView getListView() {
    // not yet supported
    throw new UnsupportedOperationException();
  }

  public void setButton(int whichButton, CharSequence text, Message msg) {
    AlertButton b = new AlertButton(mContext, this, whichButton);
    b.setText(text);
    // TODO b.onClickListener = listener;
    ((ViewGroup) mWindow.getDecorView()).addView(b);

  }

  public void setButton(int whichButton, CharSequence text, DialogInterface.OnClickListener listener) {
    AlertButton b = new AlertButton(mContext, this, whichButton);
    b.setText(text);
    b.onClickListener = listener;
    switch (whichButton) {
    case DialogInterface.BUTTON_POSITIVE: {
      buttonPositive = b;
      System.out.println("Window " + mWindow + " " + mWindow.getDecorView());
      ((ViewGroup) mWindow.getDecorView()).addView(buttonPositive);
      break;
    }
    case DialogInterface.BUTTON_NEGATIVE: {
      buttonNegative = b;
      ((ViewGroup) mWindow.getDecorView()).addView(buttonNegative);
      break;
    }
    case DialogInterface.BUTTON_NEUTRAL: {
      buttonNeutral = b;
      ((ViewGroup) mWindow.getDecorView()).addView(buttonNeutral);
      break;
    }
    }
  }

  @Deprecated
  public void setButton(CharSequence text, Message msg) {
    // TODO
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public void setButton2(CharSequence text, Message msg) {
    // TODO
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public void setButton3(CharSequence text, Message msg) {
    // TODO
    throw new UnsupportedOperationException();
  }

  @Deprecated
  public void setButton(CharSequence text, DialogInterface.OnClickListener listener) {
    AlertButton b = new AlertButton(mContext, this, DialogInterface.BUTTON_POSITIVE);
    b.setText(text);
    b.onClickListener = listener;
    buttonPositive = b;
    System.out.println("Window " + mWindow + " " + mWindow.getDecorView());
    ((ViewGroup) mWindow.getDecorView()).addView(buttonPositive);
  }

  @Deprecated
  public void setButton2(CharSequence text, DialogInterface.OnClickListener listener) {
    AlertButton b = new AlertButton(mContext, this, DialogInterface.BUTTON_NEGATIVE);
    b.setText(text);
    b.onClickListener = listener;
    buttonNegative = b;
    System.out.println("Window " + mWindow + " " + mWindow.getDecorView());
    ((ViewGroup) mWindow.getDecorView()).addView(buttonNegative);

  }

  @Deprecated
  public void setButton3(CharSequence text, DialogInterface.OnClickListener listener) {
    // TODO
    throw new UnsupportedOperationException();
  }

  public void setIcon(int resId) {
    // we don't care about the icon on the dialog
  }

  public void setIcon(Drawable icon) {
    // we don't care about the icon on the dialog
  }

  public void setIconAttribute(int attrId) {
    // we don't care about the icon on the dialog
  }

  public void setInverseBackgroundForced(boolean forceInverseBackground) {
    // we don't care about the background of the dialog
  }

  public void setMessage(CharSequence message) {
  }

  public void setView(View view) {
    mWindow.addContentView(view, null);
  }

  public static class Builder {
    Context context;

    AlertDialog dialog;

    public Builder(Context context) {
      dialog = new AlertDialog(context);
      this.context = context;
    }

    public Builder(Context context, int theme) {
      dialog = new AlertDialog(context);
      this.context = context;
    }

    public Context getContext() {
      return context;
    }

    public Builder setTitle(int titleId) {
      dialog.setTitle(titleId);
      return this;
    }

    public Builder setTitle(CharSequence title) {
      dialog.setTitle(title);
      return this;
    }

    public Builder setCustomTitle(View customTitleView) {
      // we don't care about the title on the dialog

      return this;
    }

    public Builder setMessage(int messageId) {
      // we don't care about the message on the dialog
      return this;
    }

    public Builder setMessage(CharSequence message) {
      // we don't care about the message on the dialog
      return this;
    }

    public Builder setIcon(int iconId) {
      // we don't care about the icon on the dialog
      return this;
    }

    public Builder setIcon(Drawable icon) {
      // we don't care about the icon on the dialog
      return this;
    }

    public Builder setIconAttribute(int attrId) {
      // we don't care about the icon on the dialog
      return this;
    }

    public Builder setPositiveButton(int textId, DialogInterface.OnClickListener listener) {
      dialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getText(textId), listener);
      return this;
    }

    public Builder setPositiveButton(CharSequence text, DialogInterface.OnClickListener listener) {
      dialog.setButton(DialogInterface.BUTTON_POSITIVE, text, listener);

      return this;
    }

    public Builder setNegativeButton(int textId, DialogInterface.OnClickListener listener) {
      dialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getText(textId), listener);

      return this;
    }

    public Builder setNegativeButton(CharSequence text, DialogInterface.OnClickListener listener) {
      dialog.setButton(DialogInterface.BUTTON_NEGATIVE, text, listener);

      return this;
    }

    public Builder setNeutralButton(int textId, DialogInterface.OnClickListener listener) {
      dialog.setButton(DialogInterface.BUTTON_NEUTRAL, context.getText(textId), listener);

      return this;
    }

    public Builder setNeutralButton(CharSequence text, DialogInterface.OnClickListener listener) {
      dialog.setButton(DialogInterface.BUTTON_NEUTRAL, text, listener);

      return this;
    }

    public Builder setCancelable(boolean cancelable) {
      dialog.setCancelable(cancelable);
      return this;
    }

    public Builder setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
      dialog.setOnCancelListener(onCancelListener);
      return this;
    }

    public Builder setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
      dialog.setOnKeyListener(onKeyListener);
      return this;
    }

    public Builder setItems(int itemsId, DialogInterface.OnClickListener listener) {
      return this;
    }

    public Builder setItems(CharSequence[] items, final DialogInterface.OnClickListener listener) {
      ListView list = new ListView(context);
      list.setName("items");
      ArrayAdapter<CharSequence> adap = new ArrayAdapter<CharSequence>(context,
          android.R.layout.simple_list_item_1, items);
      list.setAdapter(adap);
      list.setOnItemClickListener(new OnItemClickListener() {

        DialogInterface.OnClickListener mListener = listener;

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          mListener.onClick(dialog, (int) id);
        }
      });

      ((ViewGroup) dialog.mWindow.getDecorView()).addView(list);
      return this;
    }

    public Builder setAdapter(ListAdapter adapter, DialogInterface.OnClickListener listener) {
      return this;
    }

    public Builder setCursor(Cursor cursor, DialogInterface.OnClickListener listener, String labelColumn) {
      return this;
    }

    public Builder setMultiChoiceItems(int itemsId, boolean[] checkedItems,
                                       DialogInterface.OnMultiChoiceClickListener listener) {
      return this;
    }

    public Builder setMultiChoiceItems(CharSequence[] items, boolean[] checkedItems,
                                       DialogInterface.OnMultiChoiceClickListener listener) {
      return this;
    }

    public Builder setMultiChoiceItems(Cursor cursor, String isCheckedColumn, String labelColumn,
                                       DialogInterface.OnMultiChoiceClickListener listener) {
      return this;
    }

    public Builder setSingleChoiceItems(int itemsId, int checkedItem, DialogInterface.OnClickListener listener) {
      return this;
    }

    public Builder setSingleChoiceItems(Cursor cursor, int checkedItem, String labelColumn,
                                        DialogInterface.OnClickListener listener) {
      return this;
    }

    public Builder setSingleChoiceItems(CharSequence[] items, int checkedItem,
                                        DialogInterface.OnClickListener listener) {
      return this;
    }

    public Builder setSingleChoiceItems(ListAdapter adapter, int checkedItem,
                                        DialogInterface.OnClickListener listener) {
      return this;
    }

    public Builder setOnItemSelectedListener(AdapterView.OnItemSelectedListener listener) {
      return this;

    }

    public Builder setView(View view) {
      dialog.mWindow.setContentView(view);
      return this;
    }

    public Builder setInverseBackgroundForced(boolean useInverseBackground) {
      return this;

    }

    public AlertDialog create() {
      return dialog;
    }

    public AlertDialog show() {
      dialog.show();
      return dialog;
    }
  }

  protected static class AlertButton extends Button {
    public DialogInterface.OnClickListener onClickListener;
    public DialogInterface dialogInterface;
    public int which;

    public AlertButton(Context context, DialogInterface dialog, int which) {
      super(context);
      this.dialogInterface = dialog;
      this.which = which;
    }

    @Override
    public boolean callOnClick() {
      super.callOnClick();
      onClickListener.onClick(dialogInterface, this.which);
      dialogInterface.dismiss();
      return true;
    }

    @Override
    public List<Event> collectEvents() {
      System.out.println("Alert Dialog collecting events");
      List<Event> events = new LinkedList<Event>();
      if (onClickListener != null) {
        UIEvent uiEvent = new UIEvent("$" + this.getName(), "onClick");
        events.add(uiEvent);
      }
      System.out.println("AlertDialog: " + events.toString());
      return events;
    }

  }

}