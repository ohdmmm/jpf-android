/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app;

import gov.nasa.jpf.annotation.FilterField;

import java.lang.ref.WeakReference;

import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.JPFWindow;
import android.view.JPFWindowManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.widget.TextView;

import com.android.internal.app.ActionBarImpl;
import com.android.internal.policy.PolicyManager;

/**
 * This is the base dialg class. It implements the original dialog interface class that defines callbacks that
 * can be fire on the dialog and constants used in the dialog. A dialog is started by an currenlty visible
 * Activity. This is its owner Activity and also becomes the main context the dialog uses. The Activity
 * creates the dialog on the onCreateDialog() method and then puts it in 'n list of managed dialogs. The
 * Activity only creates the dialog once - next time the showDialog method it called on it, it loads the
 * already constructed dialog and calls the onPrepareDialog method to allow the developer a chance to update
 * the contents of the dialog.
 * 
 * An Activity has its own window linked to it. This window is stacked on the Activity window in the
 * WindowManager. The Window has a root view called the decorview that contains all menu's, actionbar and
 * title bars as well as the contentParent (the base class of the dialog contents created by the developer.)
 * 
 * If a dialog is cancelable it means the user can click the back button and the dialog will be canceled
 * (dismissed).
 * 
 * To show a dialog the show method is called by the Activity. The show method adds the Window of the dialog
 * to the stack of windows in the windowmanager. When dialog is showing, it will receive any keyevents and
 * window events from the windowManager.
 * 
 * 
 * The name of the Window of a dialig is: context.getClass().getName() + "$" + this.getClass().getSimpleName()
 * + count++ This enables us to distinuous between different dialogs in the script and buttons called the same
 * in different dialogs. otherwise the event tree gets confused.
 * 
 * TODO save instance not checked yet, Actionbar, count will not be the same second time dialog constructed
 * 
 * 
 * @author Heila
 *
 */
public class Dialog implements DialogInterface, Window.Callback, KeyEvent.Callback,
    View.OnCreateContextMenuListener {

  private static final String TAG = "Dialog";
  private Activity mOwnerActivity;

  public Context mContext;
  final WindowManager mWindowManager;
  protected Window mWindow;
  public View mDecor; // stores the other view elements such as edit text etc
  private ActionBarImpl mActionBar;

  protected boolean mCancelable = true;

  private String mCancelAndDismissTaken;
  private Message mCancelMessage;
  private Message mDismissMessage;
  private Message mShowMessage;

  private OnKeyListener mOnKeyListener;

  private boolean mCreated = false;
  private boolean mShowing = false;
  private boolean mCanceled = false;

  private final Handler mHandler = new Handler();

  private static final int DISMISS = 0x43;
  private static final int CANCEL = 0x44;
  private static final int SHOW = 0x45;

  private Handler mListenersHandler;
  private ActionMode mActionMode;

  TextView mMessage;

  @FilterField
  // private static int count = 0;
  private final Runnable mDismissAction = new Runnable() {
    @Override
    public void run() {
      dismissDialog();
    }
  };
  public int mId = -1;

  // @FilterField
  // public String mTitle = "";

  public Dialog(Context context) {
    this(context, 0, true);
  }

  public Dialog(Context context, int theme) {
    this(context, theme, true);
  }

  Dialog(Context context, int theme, boolean createContextThemeWrapper) {

    this.mContext = context;
    this.mOwnerActivity = ((Activity) context);
    mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    Window w = PolicyManager.makeNewWindow(mContext);
    ((JPFWindow) w).setName(context.getClass().getName() + "$" + this.getClass().getSimpleName());
    mWindow = w;
    ((JPFWindow) mWindow).mDialog = this;
    w.setCallback(this);
    w.setWindowManager(mWindowManager, null, null);
    // w.setGravity(Gravity.CENTER);
    mListenersHandler = new ListenersHandler(this);
    mMessage = new TextView(context);
    mMessage.setId(android.R.id.message);
    ((JPFWindow) mWindow).addContentView(mMessage);

    System.out.println("Dialog constructed " + mWindow);

  }

  public void setID(int id) {
    mId = id;
    // if (((JPFWindow) mWindow).getName().endsWith(this.getClass().getSimpleName())) {
    ((JPFWindow) mWindow)
        .setName(mContext.getClass().getName() + "$" + this.getClass().getSimpleName() + mId);
    // }
  }

  protected Dialog(Context context, boolean cancelable, Message cancelCallback) {
    this(context);
    mCancelable = cancelable;
    mCancelMessage = cancelCallback;
  }

  protected Dialog(Context context, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
    this(context);
    mCancelable = cancelable;
    setOnCancelListener(cancelListener);
  }

  public final Context getContext() {
    return mContext;
  }

  public ActionBar getActionBar() {
    return mActionBar;
  }

  public final void setOwnerActivity(Activity activity) {
    mOwnerActivity = activity;
  }

  public final Activity getOwnerActivity() {
    return mOwnerActivity;
  }

  public boolean isShowing() {
    return mShowing;
  }

  public void show() {
    // if (((Activity) this.mContext).mResumed)
    // ((Activity) this.mContext).performPause();

    if (mShowing) {
      if (mDecor != null) {
        if (mWindow.hasFeature(Window.FEATURE_ACTION_BAR)) {
          mWindow.invalidatePanelMenu(Window.FEATURE_ACTION_BAR);
        }
        mDecor.setVisibility(View.VISIBLE);
      }
      return;
    }

    mCanceled = false;
    if (!mCreated) {
      dispatchOnCreate(null);
    }
    onStart();
    mDecor = mWindow.getDecorView();

    if (mActionBar == null && mWindow.hasFeature(Window.FEATURE_ACTION_BAR)) {
      final ApplicationInfo info = mContext.getApplicationInfo();
      mActionBar = new ActionBarImpl(this);
    }

    try {
      ((JPFWindowManager) mWindowManager).addWindow(this.getWindow(), null);
      mShowing = true;

      sendShowMessage();
    } finally {
    }
  }

  public void hide() {
    if (mDecor != null) {
      mDecor.setVisibility(View.GONE);
    }
  }

  @Override
  public void dismiss() {
    if (Looper.myLooper() == mHandler.getLooper()) {
      dismissDialog();
    } else {
      mHandler.post(mDismissAction);
    }
  }

  void dismissDialog() {
    if (mDecor == null || !mShowing) {
      return;
    }

    try {
      ((JPFWindowManager) mWindowManager).removeWindowImmediate(this.mWindow);
      mShowing = false;

    } finally {
      if (mActionMode != null) {
        mActionMode.finish();
      }
      mDecor = null;
      mWindow.closeAllPanels();
      onStop();
      if (mId > -1)
        ((Activity) mContext).dismissDialog(mId);
      // if (!((Activity) this.mContext).mResumed)
      // ((Activity) this.mContext).performResume();

      sendDismissMessage();
    }
  }

  private void sendDismissMessage() {
    if (mDismissMessage != null) {
      // Obtain a new message so this dialog can be re-used
      Message.obtain(mDismissMessage).sendToTarget();
    }
  }

  private void sendShowMessage() {
    if (mShowMessage != null) {
      // Obtain a new message so this dialog can be re-used
      Message.obtain(mShowMessage).sendToTarget();
    }
  }

  void dispatchOnCreate(Bundle savedInstanceState) {
    if (!mCreated) {
      onCreate(savedInstanceState);
      mCreated = true;
    }
  }

  protected void onCreate(Bundle savedInstanceState) {
  }

  protected void onStart() {
    if (mActionBar != null)
      mActionBar.setShowHideAnimationEnabled(true);

  }

  protected void onStop() {
    if (mActionBar != null)
      mActionBar.setShowHideAnimationEnabled(false);

  }

  private static final String DIALOG_SHOWING_TAG = "android:dialogShowing";
  private static final String DIALOG_HIERARCHY_TAG = "android:dialogHierarchy";

  public Bundle onSaveInstanceState() {
    Bundle bundle = new Bundle();
    bundle.putBoolean(DIALOG_SHOWING_TAG, mShowing);
    if (mCreated) {
      bundle.putBundle(DIALOG_HIERARCHY_TAG, mWindow.saveHierarchyState());
    }
    return bundle;
  }

  public void onRestoreInstanceState(Bundle savedInstanceState) {
    final Bundle dialogHierarchyState = savedInstanceState.getBundle(DIALOG_HIERARCHY_TAG);
    if (dialogHierarchyState == null) {
      // dialog has never been shown, or onCreated, nothing to restore.
      return;
    }
    dispatchOnCreate(savedInstanceState);
    mWindow.restoreHierarchyState(dialogHierarchyState);
    if (savedInstanceState.getBoolean(DIALOG_SHOWING_TAG)) {
      show();
    }
  }

  public Window getWindow() {
    return mWindow;

  }

  public View getCurrentFocus() {
    return mWindow != null ? mWindow.getCurrentFocus() : null;
  }

  public View findViewById(int id) {
    return mWindow.findViewById(id);

  }

  public void setContentView(int layoutResID) {
    mWindow.setContentView(layoutResID);

  }

  public void setContentView(View v) {
    mWindow.setContentView(v);
  }

  public void setContentView(View view, ViewGroup.LayoutParams params) {
    mWindow.setContentView(view, params);
  }

  public void addContentView(View view, ViewGroup.LayoutParams params) {
    mWindow.addContentView(view, params);
  }

  public void setTitle(CharSequence title) {
    if (((JPFWindow) mWindow).getName().endsWith(this.getClass().getSimpleName()) && title != null) {
      ((JPFWindow) mWindow).setName(mContext.getClass().getName() + "$" + title.toString());
    }
    // this.mTitle = title.toString();
  }

  public void setTitle(int titleId) {
    setTitle(mContext.getText(titleId));

  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      event.startTracking();
      return true;
    }

    return false;
  }

  @Override
  public boolean onKeyLongPress(int keyCode, KeyEvent event) {
    return false;
  }

  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK && event.isTracking() && !event.isCanceled()) {
      onBackPressed();
      return true;
    }
    return false;
  }

  @Override
  public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
    return false;
  }

  public void onBackPressed() {
    if (mCancelable) {
      cancel();
    }
  }

  public boolean onKeyShortcut(int keyCode, KeyEvent event) {
    return false;
  }

  public boolean onTouchEvent(MotionEvent event) {
    if (mCancelable && mShowing && mWindow.shouldCloseOnTouch(mContext, event)) {
      cancel();
      return true;
    }

    return false;
  }

  public boolean onTrackballEvent(MotionEvent event) {
    return false;
  }

  public boolean onGenericMotionEvent(MotionEvent event) {
    return false;
  }

  @Override
  public void onWindowAttributesChanged(WindowManager.LayoutParams params) {
    if (mDecor != null) {
      ((JPFWindowManager) mWindowManager).updateWindowLayout(this.getWindow(), params);
    }
  }

  @Override
  public void onContentChanged() {
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
  }

  @Override
  public void onAttachedToWindow() {
  }

  @Override
  public void onDetachedFromWindow() {
  }

  @Override
  public boolean dispatchKeyEvent(KeyEvent event) {

    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
      dismiss();
      return true;
    }

    if ((mOnKeyListener != null) && (mOnKeyListener.onKey(this, event.getKeyCode(), event))) {
      return true;
    }
    if (mWindow.superDispatchKeyEvent(event)) {
      return true;
    }

    // if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
    return false; // TODO does not handle keyevent listener on view
    // return event.dispatch(this, mDecor != null ? mDecor.getKeyDispatcherState() : null, this);
  }

  @Override
  public boolean dispatchKeyShortcutEvent(KeyEvent event) {
    return false;
  }

  @Override
  public boolean dispatchTouchEvent(MotionEvent ev) {
    return false;
  }

  @Override
  public boolean dispatchTrackballEvent(MotionEvent ev) {
    return false;
  }

  @Override
  public boolean dispatchGenericMotionEvent(MotionEvent ev) {
    return false;
  }

  @Override
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent event) {
    return false;
  }

  /**
   * @see Activity#onCreatePanelView(int)
   */
  @Override
  public View onCreatePanelView(int featureId) {
    return null;
  }

  /**
   * @see Activity#onCreatePanelMenu(int, Menu)
   */
  @Override
  public boolean onCreatePanelMenu(int featureId, Menu menu) {
    if (featureId == Window.FEATURE_OPTIONS_PANEL) {
      return onCreateOptionsMenu(menu);
    }

    return false;
  }

  /**
   * @see Activity#onPreparePanel(int, View, Menu)
   */
  @Override
  public boolean onPreparePanel(int featureId, View view, Menu menu) {
    if (featureId == Window.FEATURE_OPTIONS_PANEL && menu != null) {
      boolean goforit = onPrepareOptionsMenu(menu);
      return goforit; // && menu.hasVisibleItems();
    }
    return true;
  }

  /**
   * @see Activity#onMenuOpened(int, Menu)
   */
  @Override
  public boolean onMenuOpened(int featureId, Menu menu) {
    if (featureId == Window.FEATURE_ACTION_BAR) {
      mActionBar.dispatchMenuVisibilityChanged(true);
    }
    return true;
  }

  /**
   * @see Activity#onMenuItemSelected(int, MenuItem)
   */
  @Override
  public boolean onMenuItemSelected(int featureId, MenuItem item) {
    return false;
  }

  /**
   * @see Activity#onPanelClosed(int, Menu)
   */
  @Override
  public void onPanelClosed(int featureId, Menu menu) {
    if (featureId == Window.FEATURE_ACTION_BAR) {
      mActionBar.dispatchMenuVisibilityChanged(false);
    }
  }

  /**
   * It is usually safe to proxy this call to the owner activity's {@link Activity#onCreateOptionsMenu(Menu)}
   * if the client desires the same menu for this Dialog.
   * 
   * @see Activity#onCreateOptionsMenu(Menu)
   * @see #getOwnerActivity()
   */
  public boolean onCreateOptionsMenu(Menu menu) {
    return true;
  }

  /**
   * It is usually safe to proxy this call to the owner activity's {@link Activity#onPrepareOptionsMenu(Menu)}
   * if the client desires the same menu for this Dialog.
   * 
   * @see Activity#onPrepareOptionsMenu(Menu)
   * @see #getOwnerActivity()
   */
  public boolean onPrepareOptionsMenu(Menu menu) {
    return true;
  }

  /**
   * @see Activity#onOptionsItemSelected(MenuItem)
   */
  public boolean onOptionsItemSelected(MenuItem item) {
    return false;
  }

  /**
   * @see Activity#onOptionsMenuClosed(Menu)
   */
  public void onOptionsMenuClosed(Menu menu) {
  }

  /**
   * @see Activity#openOptionsMenu()
   */
  public void openOptionsMenu() {
    mWindow.openPanel(Window.FEATURE_OPTIONS_PANEL, null);
  }

  /**
   * @see Activity#closeOptionsMenu()
   */
  public void closeOptionsMenu() {
    mWindow.closePanel(Window.FEATURE_OPTIONS_PANEL);
  }

  /**
   * @see Activity#invalidateOptionsMenu()
   */
  public void invalidateOptionsMenu() {
    mWindow.invalidatePanelMenu(Window.FEATURE_OPTIONS_PANEL);
  }

  /**
   * @see Activity#onCreateContextMenu(ContextMenu, View, ContextMenuInfo)
   */
  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
  }

  /**
   * @see Activity#registerForContextMenu(View)
   */
  public void registerForContextMenu(View view) {
    view.setOnCreateContextMenuListener(this);
  }

  /**
   * @see Activity#unregisterForContextMenu(View)
   */
  public void unregisterForContextMenu(View view) {
    view.setOnCreateContextMenuListener(null);
  }

  /**
   * @see Activity#openContextMenu(View)
   */
  public void openContextMenu(View view) {
    view.showContextMenu();
  }

  /**
   * @see Activity#onContextItemSelected(MenuItem)
   */
  public boolean onContextItemSelected(MenuItem item) {
    return false;
  }

  /**
   * @see Activity#onContextMenuClosed(Menu)
   */
  public void onContextMenuClosed(Menu menu) {
  }

  @Override
  public boolean onSearchRequested() {
    return false;
  }

  @Override
  public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
    if (mActionBar != null) {
      return mActionBar.startActionMode(callback);
    }
    return null;
  }

  @Override
  public void onActionModeStarted(ActionMode mode) {
    mActionMode = mode;
  }

  @Override
  public void onActionModeFinished(ActionMode mode) {
    if (mode == mActionMode) {
      mActionMode = null;
    }
  }

  private ComponentName getAssociatedActivity() {
    Activity activity = mOwnerActivity;
    Context context = getContext();
    while (activity == null && context != null) {
      if (context instanceof Activity) {
        activity = (Activity) context; // found it!
      } else {
        context = (context instanceof ContextWrapper) ? ((ContextWrapper) context).getBaseContext() : // unwrap
                                                                                                      // one
                                                                                                      // level
            null; // done
      }
    }
    return activity == null ? null : activity.getComponentName();
  }

  public void takeKeyEvents(boolean get) {
    mWindow.takeKeyEvents(get);
  }

  public final boolean requestWindowFeature(int featureId) {
    return getWindow().requestFeature(featureId);
  }

  public final void setFeatureDrawableResource(int featureId, int resId) {
  }

  public final void setFeatureDrawableUri(int featureId, Uri uri) {
  }

  public final void setFeatureDrawable(int featureId, Drawable drawable) {
  }

  public final void setFeatureDrawableAlpha(int featureId, int alpha) {
  }

  public LayoutInflater getLayoutInflater() {
    return getWindow().getLayoutInflater();
  }

  public void setCancelable(boolean flag) {
    mCancelable = flag;
  }

  public boolean isCancelable() {
    return mCancelable;
  }

  public void setCanceledOnTouchOutside(boolean cancel) {
    if (cancel && !mCancelable) {
      mCancelable = true;
    }

    mWindow.setCloseOnTouchOutside(cancel);
  }

  @Override
  public void cancel() {
    if (!mCanceled && mCancelMessage != null) {
      mCanceled = true;
      // Obtain a new message so this dialog can be re-used
      Message.obtain(mCancelMessage).sendToTarget();
    }
    dismiss();

  }

  public void setOnCancelListener(DialogInterface.OnCancelListener listener) {
    if (mCancelAndDismissTaken != null) {
      throw new IllegalStateException("OnCancelListener is already taken by " + mCancelAndDismissTaken
          + " and can not be replaced.");
    }
    if (listener != null) {
      mCancelMessage = mListenersHandler.obtainMessage(CANCEL, listener);
    } else {
      mCancelMessage = null;
    }
  }

  public void setCancelMessage(Message msg) {
    mCancelMessage = msg;

  }

  public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
    if (mCancelAndDismissTaken != null) {
      throw new IllegalStateException("OnDismissListener is already taken by " + mCancelAndDismissTaken
          + " and can not be replaced.");
    }
    if (listener != null) {
      mDismissMessage = mListenersHandler.obtainMessage(DISMISS, listener);
    } else {
      mDismissMessage = null;
    }
  }

  public void setOnShowListener(DialogInterface.OnShowListener listener) {
    if (listener != null) {
      mShowMessage = mListenersHandler.obtainMessage(SHOW, listener);
    } else {
      mShowMessage = null;
    }
  }

  public void setDismissMessage(Message msg) {
    mDismissMessage = msg;
  }

  public final void setVolumeControlStream(int streamType) {
    throw new RuntimeException("Stub!");
  }

  public final int getVolumeControlStream() {
    throw new RuntimeException("Stub!");
  }

  public void setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
    this.mOnKeyListener = onKeyListener;
  }

  private static final class ListenersHandler extends Handler {
    private WeakReference<DialogInterface> mDialog;

    public ListenersHandler(Dialog dialog) {
      mDialog = new WeakReference<DialogInterface>(dialog);
    }

    @Override
    public void handleMessage(Message msg) {
      switch (msg.what) {
      case DISMISS:
        ((OnDismissListener) msg.obj).onDismiss(mDialog.get());
        break;
      case CANCEL:
        ((OnCancelListener) msg.obj).onCancel(mDialog.get());
        break;
      case SHOW:
        ((OnShowListener) msg.obj).onShow(mDialog.get());
        break;
      }
    }
  }
}
