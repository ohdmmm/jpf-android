package android.view;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.xml.sax.SAXException;

import android.content.Context;
import android.util.AttributeSet;
import android.util.AttributeSetImpl;
import android.util.Log;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * Inflates a layout file given its resource id into a {@link View} hierarchy.
 * 
 * @author "Heila van der Merwe"
 * 
 */
public class LayoutInflater {
  public static final String TAG = "LayoutInflater";
  @FilterField
  @NeverBreak
  private static final boolean DEBUG = false;

  /** The package where Android Widgets are stored. */
  @FilterField
  private static String WIDGET_PACKAGE = "android.widget";

  private static final int TYPE = 0;
  private static final int ID = 1;
  private static final int NAME = 2;
  private static final int HASHCODE = 3;
  private static final int TEXT = 4;
  private static final int LISTENER = 5;

  /** Not sure what context to store here TODO */
  Context c;

  /**
   * Used to create an unique name and id field for components that are not named in the R.java file. Window
   * has count 0.
   */
  // private static int count = 1;

  public LayoutInflater(Context c) {
    this.c = c;

  }

  /**
   * Obtains the LayoutInflater from the given context.
   */
  public static LayoutInflater from(Context context) {
    LayoutInflater layoutInflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    if (layoutInflater == null) {
      throw new AssertionError("LayoutInflater not found.");
    }
    layoutInflater.c = context;

    return layoutInflater;
  }

  /**
   * Inflate a new view hierarchy from the specified xml resource. Throws {@link InflateException} if there is
   * an error.
   * 
   * @param resource
   *          ID for an XML layout resource to load (e.g., <code>R.layout.main_page</code>)
   * @param root
   *          Optional view to be the parent of the generated hierarchy.
   * @return The root View of the inflated hierarchy. If root was supplied, this is the root View; otherwise
   *         it is the root of the inflated XML file.
   */
  public View inflate(int resourceID, ViewGroup root) {
    try {
      String filename = loadLayout(resourceID);
      int rootNativeHash = getRootHash(resourceID);

      View view = visit(rootNativeHash, resourceID);
      // print(view, "\t");

      if (root != null && view != null)
        root.addView(view);
      else
        return view;

    } catch (Exception e) {
      Log.e(TAG, "Error inflating layout file " + e.getMessage());
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    return root;

  }

  public View inflate(int resourceID, ViewGroup root, boolean attachToRoot) {
    if (attachToRoot)
      return inflate(resourceID, root);
    else
      return inflate(resourceID, null);

  }

  private native int getRValue(String name);

  private native int getRootHash(int resourceID);

  private native String loadLayout(int resourceID);

  public View visit(int nodeCode, int resourceID) throws SAXException {
    if (nodeCode == -1)
      return null;

    // set the info of the view
    String[] node = getNodeInfo(nodeCode, resourceID);
    Object[] nodeAttr = getNodeAttributes(nodeCode, resourceID);

    View view = null;
    try {
      // inflate the view
      view = inflateView(node, nodeAttr);
      if (view != null) {
        // visit the children of the view
        int[] childrenIds = getChildren(nodeCode, resourceID);
        View child = null;
        if (childrenIds != null) {
          for (int id : childrenIds) {
            // visit the child
            child = visit(id, resourceID);
            if (child != null) {
              // add child to parent
              ((ViewGroup) view).addView(child);
              Log.i("LayoutInflator", "Adding view=" + child + " to view=" + view);
            }
          }
        }
      }
    } catch (Exception e) {
      Log.e(TAG, "Error inflating view " + e.getMessage());
      e.printStackTrace();
      return null;
    }
    return view;
  }

  public native String[] getNodeInfo(int hash, int resourceID);

  public native Object[] getNodeAttributes(int hash, int resourceID);

  public native int[] getChildren(int hash, int resourceID);

  private void print(View v, String space) {
    if (v == null) {
      System.out.println("NULL VIEW");
      return;
    }
    System.out.print(v.toString() + "{");
    if (v instanceof ViewGroup) {
      for (View c : ((ViewGroup) v).getChildren()) {
        if (c != null) {
          System.out.print("\n");
          System.out.print(space);
          print(c, space + "\t");
        }
      }
    }

    System.out.print("}");
  }

  /**
   * Makes use of Java reflection to create an instance of a {@link View} object.
   * 
   * @param type
   *          The type of the {@link View}
   * @return The {@link View} object
   * @throws ClassNotFoundException
   * @throws NoSuchMethodException
   * @throws IllegalArgumentException
   * @throws InstantiationException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   */
  public View inflateView(final String[] info, Object[] attributes) throws ClassNotFoundException,
      NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException,
      InvocationTargetException {

    View view = null;
    if (info[TYPE].contains("include")) {
      int layoutID = -1;
      String id = null;

//      System.out.println(Arrays.toString(attributes));
      for (Object att : attributes) {
        Object[] attribute = (Object[]) att;
//        System.out.println(Arrays.toString(attribute));
        if (attribute == null)
          continue;
        switch (((String) attribute[0])) {
        case "layout": {
          String layout = ((String) attribute[1]);
          layoutID = getRValue(layout);
          break;
        }
        case "android:id": {
          id = ((String) attribute[1]);
          break;
        }
        }
      }

      View stub = inflate(layoutID, null);
      if (id != null)
        stub.setId(getRValue(id));
      return stub;

    } else if (info[TYPE].equals("requestFocus")) {
      return null;
    } else if (info[TYPE].equals("View")) {
      return new View(c);
    } else if (info[TYPE].equals("merge")) {
      return new ViewGroup(c);
    } else if (info[TYPE].equals("ViewStub")) {
      String newID = "";
      String newLayout = "";
      for (Object att : attributes) {
        Object[] attribute = (Object[]) att;
        switch (((String) attribute[0])) {
        case "android:inflatedId": {
          newID = ((String) attribute[1]);
          break;
        }
        case "android:layout": {
          newLayout = ((String) attribute[1]);
          break;
        }
        }
      }
      View stub = inflate(getRValue(newLayout), null);
      stub.setId(getRValue(newID));
      return stub;
    } else if (info[TYPE].equals("WebView")) {
      // inflate the view
      Class<? extends View> cls = (Class<? extends View>) Class.forName("android.webkit.WebView");
      Class[] intArgsClass = new Class[] { Context.class };
      Object[] intArgs = new Object[] { c };
      Constructor intArgsConstructor = cls.getConstructor(intArgsClass);
      view = (View) intArgsConstructor.newInstance(intArgs);

      // setGenericProperties
      view.setName(info[NAME]);
      view.setId(Integer.parseInt(info[ID]));
      view.setNativeHashCode(Integer.parseInt(info[HASHCODE]));
      if (!info[TEXT].equals(""))
        ((TextView) view).setText(info[TEXT]);

      if (view instanceof Button && info[LISTENER] != null && info[LISTENER].length() > 0) {

        view.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Context c = v.mContext;
            Class<? extends Context> act = c.getClass();
            try {
              Method m = act.getMethod(info[LISTENER], new Class[] { View.class });
              m.invoke(v.mContext, new Object[] { v });
            } catch (Exception e) {
              System.out.println("Could not execute onclick method added from xml");
              e.printStackTrace();
            }
          }
        });

      }
      if (DEBUG)
        Log.v(TAG, "Inflated " + view.toString());

    } else if (info[TYPE].startsWith("com.keepassdroid.view")) {
      // inflate the view
      Class<? extends View> cls = (Class<? extends View>) Class.forName(info[TYPE]);
      Class[] intArgsClass = new Class[] { Context.class, AttributeSet.class };
      Object[] intArgs = new Object[] { c, null };
      Constructor intArgsConstructor = cls.getConstructor(intArgsClass);
      view = (View) intArgsConstructor.newInstance(intArgs);

      // setGenericProperties
      view.setName(info[NAME]);
      view.setId(Integer.parseInt(info[ID]));
      view.setNativeHashCode(Integer.parseInt(info[HASHCODE]));
      if (!info[TEXT].equals(""))
        ((TextView) view).setText(info[TEXT]);

      if (view instanceof Button && info[LISTENER] != null && info[LISTENER].length() > 0) {

        view.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Context c = v.mContext;
            Class<? extends Context> act = c.getClass();
            try {
              Method m = act.getMethod(info[LISTENER], new Class[] { View.class });
              m.invoke(v.mContext, new Object[] { v });
            } catch (Exception e) {
              System.out.println("Could not execute onclick method added from xml");
              e.printStackTrace();
            }
          }
        });

      }
      if (DEBUG)
        Log.v(TAG, "Inflated " + view.toString());
    } else if (info[TYPE].equals("view")) {
      String classname = "com.nloko.android.syncmypix.graphics.CropImage$CropImageView";
      // Object[] atts = attributes;
      // for (int i = 0; i < atts.length; i++) {
      // Object[] att = (Object[]) atts[i];
      // if (((String) att[0]).equals("class")) {
      // classname = ((String) att[1]);
      // }
      // }

      // inflate the view
      Class<? extends View> cls = (Class<? extends View>) Class.forName(classname);
      Class[] intArgsClass = new Class[] { Context.class, AttributeSet.class };
      Object[] intArgs = new Object[] { c, null };
      Constructor intArgsConstructor = cls.getConstructor(intArgsClass);
      view = (View) intArgsConstructor.newInstance(intArgs);

      // setGenericProperties
      view.setName(info[NAME]);
      view.setId(Integer.parseInt(info[ID]));
      view.setNativeHashCode(Integer.parseInt(info[HASHCODE]));
      if (!info[TEXT].equals(""))
        ((TextView) view).setText(info[TEXT]);

      if (view instanceof Button && info[LISTENER] != null && info[LISTENER].length() > 0) {

        view.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Context c = v.mContext;
            Class<? extends Context> act = c.getClass();
            try {
              Method m = act.getMethod(info[LISTENER], new Class[] { View.class });
              m.invoke(v.mContext, new Object[] { v });
            } catch (Exception e) {
              System.out.println("Could not execute onclick method added from xml");
              e.printStackTrace();
            }
          }
        });

      }
      if (DEBUG)
        Log.v(TAG, "Inflated " + view.toString());
    } else if (info[TYPE].contains(".")) {
      // inflate the view
      Class<? extends View> cls = (Class<? extends View>) Class.forName(info[TYPE]);
      Class[] intArgsClass = new Class[] { Context.class, AttributeSet.class };
      Object[] intArgs = new Object[] { c, new AttributeSetImpl() };
      Constructor intArgsConstructor = cls.getConstructor(intArgsClass);
      view = (View) intArgsConstructor.newInstance(intArgs);

      // setGenericProperties
      view.setName(info[NAME]);
      view.setId(Integer.parseInt(info[ID]));
      view.setNativeHashCode(Integer.parseInt(info[HASHCODE]));
      if (!info[TEXT].equals(""))
        ((TextView) view).setText(info[TEXT]);

      if (view instanceof Button && info[LISTENER] != null && info[LISTENER].length() > 0) {

        view.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Context c = v.mContext;
            Class<? extends Context> act = c.getClass();
            try {
              Method m = act.getMethod(info[LISTENER], new Class[] { View.class });
              m.invoke(v.mContext, new Object[] { v });
            } catch (Exception e) {
              System.out.println("Could not execute onclick method added from xml");
              e.printStackTrace();
            }
          }
        });

      }
      if (DEBUG)
        Log.v(TAG, "Inflated " + view.toString());
    } else {
      // inflate the widget class
      Class<? extends View> cls = (Class<? extends View>) Class.forName(WIDGET_PACKAGE + "." + info[TYPE]);
      Class[] intArgsClass = new Class[] { Context.class };
      Object[] intArgs = new Object[] { c };
      Constructor intArgsConstructor = cls.getConstructor(intArgsClass);
      view = (View) intArgsConstructor.newInstance(intArgs);

      // setGenericProperties
      view.setName(info[NAME]);
      view.setId(Integer.parseInt(info[ID]));
      view.setNativeHashCode(Integer.parseInt(info[HASHCODE]));
      if (!info[TEXT].equals(""))
        ((TextView) view).setText(info[TEXT]);

      if (view instanceof Button && info[LISTENER] != null && info[LISTENER].length() > 0) {

        view.setOnClickListener(new OnClickListener() {

          @Override
          public void onClick(View v) {
            Context c = v.mContext;
            Class<? extends Context> act = c.getClass();
            try {
              Method m = act.getMethod(info[LISTENER], new Class[] { View.class });
              m.invoke(v.mContext, new Object[] { v });
            } catch (Exception e) {
              System.out.println("Could not execute onclick method added from xml");
              e.printStackTrace();
            }
          }
        });

      }
      if (DEBUG)
        Log.v(TAG, "Inflated " + view.toString());
    }
    return view;
  }
}