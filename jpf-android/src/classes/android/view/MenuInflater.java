package android.view;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;

import org.xml.sax.SAXException;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.SearchView;

/** Inflated menu from XML */
public class MenuInflater {
  public static final String TAG = "MenuInflater";
  private static final boolean DEBUG = false;

  /** The package where Android Widgets are stored. */
  @FilterField
  private static final String PACKAGE = "android.view";
  private static final int TYPE = 0;
  private static final int ID = 1;
  private static final int NAME = 2;
  private static final int HASHCODE = 3;
  private static final int TEXT = 4;
  private static final int LISTENER = 5;

  /**
   * Context of the Activity
   */
  @FilterField
  @NeverBreak
  Context c;

  /**
   * Used to create an unique name and id field for components that are not named in the R.java file. Window
   * has count 0.
   */
  @FilterField
  @NeverBreak
  private static int count = 1;

  public MenuInflater(Context c) {
    this.c = c;
  }

  public MenuInflater(Context themedContext, Activity activity) {
  }

  public void inflate(int resourceID, android.view.Menu root) {
    if (root == null) {
      Log.e(TAG, "Menu of " + c + "was null.");
    }

    String filename = null;
    try {
      filename = loadXMLFile(resourceID);
      int rootNativeHash = getRootHash(resourceID);

      visit(rootNativeHash, resourceID, root);
      // print(view, "\t");

      // if (root != null && pref != null)
      // root.
      // else
      // return pref;

    } catch (Exception e) {
      Log.e(TAG, "Error inflating preference file \"" + filename + "\":" + e.getMessage());
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private native int getRootHash(int resourceID);

  private native String loadXMLFile(int resourceID);

  public void visit(int nodeCode, int resourceID, Menu parent) throws SAXException {
    if (nodeCode == -1)
      return;
    // when is parent inflated?

    // set the info of the view
    String[] node = getNodeInfo(nodeCode, resourceID);
    Object[] nodeAttr = getNodeAttributes(nodeCode, resourceID);

    try {
      // visit the children of the view
      int[] childrenIds = getChildren(nodeCode, resourceID);
      if (childrenIds != null) {
        for (int id : childrenIds) {
          String[] info = getNodeInfo(id, resourceID);
          Object[] atts = getNodeAttributes(id, resourceID);
          inflateM(info, atts, parent);
        }
      }
    } catch (Exception e) {
      Log.e(TAG, "Error inflating preference " + e.getMessage());
      return;
    }
  }

  public native String[] getNodeInfo(int hash, int resourceID);

  public native Object[] getNodeAttributes(int hash, int resourceID);

  public native int[] getChildren(int hash, int resourceID);

  // private void print(Menu m, String space) {
  // if (v == null) {
  // System.out.println("NULL VIEW");
  // return;
  // }
  // System.out.print(v.toString() + "{");
  // if (v instanceof PreferenceGroup) {
  // for (int i = 0; i < ((PreferenceGroup) v).getPreferenceCount(); i++) {
  // Preference c = ((PreferenceGroup) v).getPreference(i);
  // if (c != null) {
  // System.out.print("\n");
  // System.out.print(space);
  // print(c, space + "\t");
  // }
  // }
  // }
  //
  // System.out.print("}");
  // }

  /**
   * Makes use of Java reflection to create an instance of a Preference object.
   * 
   */
  public void inflateM(final String[] info, Object[] attributes, Menu parent) {

    String type = info[TYPE];
    int itemId = Integer.parseInt(info[ID]);
    int groupId = 0;
    String title = "";
    boolean enabled = true;
    View actionViewClass = null;

    if (type.equals("item")) {

      for (Object att : attributes) {
        Object[] attribute = (Object[]) att;
        // System.out.println(Arrays.toString(attribute));
        switch (((String) attribute[0])) {
        case "android:title": {
          title = ((String) attribute[1]);
          break;
        }
        case "android:actionViewClass": {
          String cls = ((String) attribute[1]);
          LayoutInflater l = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          actionViewClass = new SearchView(c);
          break;
        }
        default: {
          break;
        }
        }
      }
      MenuItem item = parent.add(groupId, itemId, 0, title);
      if (actionViewClass != null) {
        item.setActionView(actionViewClass);
      }
    } else if (type.equals("menu")) {
      // todo
    }

  }

  private native String loadPreferences(int resourceID);

}