package android.view;

import gov.nasa.jpf.util.event.EventProcessor;
import gov.nasa.jpf.util.event.InvalidEventException;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.KeyPressEvent;
import gov.nasa.jpf.util.event.events.UIEvent;

import java.util.Stack;

import android.content.Context;
import android.os.ServiceManager;
import android.util.Log;

import com.android.server.am.ActivityManagerService;

public class JPFWindowManager implements WindowManager, EventProcessor {
  private final static String TAG = "WindowManager";

  private static JPFWindow currentWindow = null;
  private static final Stack<JPFWindow> mWindows = new Stack<JPFWindow>();

  public JPFWindowManager() {
    init0();
    Log.i(TAG, "Ready!");
  }

  public static native void init0();

  public static native void setNewWindow(String windowName);

  public static void setWindow(Window window) {
    Log.i(TAG, "Setting window:" + currentWindow + " ----> " + window);
    currentWindow = (JPFWindow) window;
    mWindows.push(currentWindow);
    setNewWindow(currentWindow.getName());
  }

  /**
   * Dispatch event to current Window
   * 
   * @param name
   * @param action
   * @throws InvalidEventException
   */
  @Override
  public void processEvent(Event e) throws InvalidEventException {

    if (e instanceof UIEvent) {
      UIEvent uievent = (UIEvent) e;
      currentWindow.processViewEvent(uievent);

    } else if (e instanceof KeyPressEvent) {
      Log.i(TAG, "Processing " + e);
      boolean handled = currentWindow.processKeyEvent((KeyPressEvent) e);
      if (!handled) {
        ActivityManagerService ams = (ActivityManagerService) ServiceManager
            .getSystemService(Context.ACTIVITY_SERVICE);
        ams.processEvent(e);
      }

    } else {
      Log.i(TAG, "Cannot process event " + e);
    }
  }

  public static WindowManager getInstance() {
    return (WindowManager) ServiceManager.getSystemService(Context.WINDOW_SERVICE);
  }

  @Override
  public Event[] getEvents() {
    Log.i(TAG, "Getting events from  ----> " + currentWindow);

    if (currentWindow != null) {
      return currentWindow.getNextEvents();
    } else {
      return new Event[0];
    }
  }

  @Override
  public void addView(View view, android.view.ViewGroup.LayoutParams params) {
    throw new RuntimeException("WindowManager: Can only add a Window to WindowManager");
  }

  public void addWindow(Window w, android.view.ViewGroup.LayoutParams params) {
    Log.i(TAG, "Adding window: " + currentWindow + " ----> " + w);
    mWindows.push((JPFWindow) w);
    currentWindow = (JPFWindow) w;
    setNewWindow(currentWindow.getName());

  }

  @Override
  public void removeViewImmediate(View view) {
    throw new RuntimeException("WindowManager: Can only remove a Window from WindowManager");

  }

  public void removeWindowImmediate(Window w) {
    Log.i(TAG, "currrentWindow: " + currentWindow + " removingWindow: " + w);
    // System.out.println(mWindows.size());
    if (currentWindow != null && currentWindow.equals(w)) {
      JPFWindow ww = mWindows.pop();
      if (ww.mDialog == null)
        ww.destroy();

      if (mWindows.isEmpty()) {
        currentWindow = null;
        Log.i(TAG, "WindowManager: no more windows!");

      } else {
        currentWindow = mWindows.peek();
        Log.i(TAG, "currrentWindow: " + mWindows.peek());
        // System.out.println(mWindows.size());
      }
    } else {
      Log.i(TAG, "WIndowManager removing window - its not currently the top window :" + (mWindows.size() - 1)
          + "/" + mWindows.indexOf(w));
      if (((JPFWindow) w).mDialog == null)
        w.destroy();
      mWindows.remove(w);

    }
    setNewWindow((currentWindow != null) ? currentWindow.getName() : null);

  }

  @Override
  public void updateViewLayout(View view, android.view.ViewGroup.LayoutParams params) {
    throw new RuntimeException("WindowManager: Can only remove a Window from WindowManager");
  }

  public void updateWindowLayout(Window w, android.view.ViewGroup.LayoutParams params) {
    if (currentWindow.equals(w)) {
      Window ww = this.mWindows.peek();
      if (ww != null) {
        ww.getCallback().onContentChanged();
      } else
        throw new RuntimeException("WindowManager: Can only update view since it does not exist.");

    }
  }

  @Override
  public void removeView(View view) {
    throw new RuntimeException("WindowManager: Can only remove a Window from WindowManager");

  }

  public String getCurrentWindow() {
    if (currentWindow != null)
      return currentWindow.getName();
    else
      return "default";
  }

  @Override
  public Display getDefaultDisplay() {
    return Display.TOP;
  }

}