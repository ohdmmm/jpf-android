package android.view;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.KeyPressEvent;

import java.util.Collection;
import java.util.List;

public interface Menu {

  public abstract android.view.MenuItem add(int groupId, int itemId, int order, int titleRes);

  public abstract android.view.SubMenu addSubMenu(int param0, int param1, int param2, int param3);

  public abstract android.view.MenuItem add(int groupId, int itemId, int order, CharSequence title);

  public abstract void setVisible(boolean b);

  public boolean isVisible();

  public abstract List<Event> getNextEvents();

  public abstract void processEvent(KeyPressEvent keyEvent);

  public android.view.MenuItem findItem(int param0);

  public boolean process(KeyPressEvent keyEvent);

  public int size();

  public Collection<MenuItem> getItems();
}