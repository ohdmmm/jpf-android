package android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;

public class ContextMenuImpl extends MenuImpl implements ContextMenu {


  public ContextMenuImpl(Context c) {
    super(c);

  }

  @Override
  public ContextMenu setHeaderTitle(int titleRes) {
    return this;
  }

  @Override
  public ContextMenu setHeaderTitle(CharSequence title) {
    return this;
  }

  @Override
  public ContextMenu setHeaderIcon(int iconRes) {
    // TODO Auto-generated method stub
    return this;
  }

  @Override
  public ContextMenu setHeaderIcon(Drawable icon) {
    // TODO Auto-generated method stub
    return this;
  }

  @Override
  public ContextMenu setHeaderView(View view) {
    // TODO Auto-generated method stub
    return this;
  }

  @Override
  public void clearHeader() {
    // TODO Auto-generated method stub

  }


}
