package android.view;

import gov.nasa.jpf.vm.Abstraction;

public final class Display {
  public static final int DEFAULT_DISPLAY = 0;
  public static final int FLAG_PRESENTATION = 8;
  public static final int FLAG_PRIVATE = 4;
  public static final int FLAG_SECURE = 2;
  public static final int FLAG_SUPPORTS_PROTECTED_BUFFERS = 1;
  public static final int STATE_DOZE = 3;
  public static final int STATE_DOZE_SUSPEND = 4;
  public static final int STATE_OFF = 1;
  public static final int STATE_ON = 2;
  public static final int STATE_UNKNOWN = 0;
  public static android.view.Display TOP = new android.view.Display();

  public Display() {
  }

  public int getDisplayId() {
    return Abstraction.TOP_INT;
  }

  public boolean isValid() {
    return Abstraction.TOP_BOOL;
  }

  public int getFlags() {
    return Abstraction.TOP_INT;
  }

  public java.lang.String getName() {
    return Abstraction.TOP_STRING;
  }

  public void getSize(android.graphics.Point param0) {
  }

  public void getRectSize(android.graphics.Rect param0) {
  }

  public void getCurrentSizeRange(android.graphics.Point param0, android.graphics.Point param1) {
  }

  public int getWidth() {
    return Abstraction.TOP_INT;
  }

  public int getHeight() {
    return Abstraction.TOP_INT;
  }

  public int getRotation() {
    return Abstraction.TOP_INT;
  }

  public int getOrientation() {
    return Abstraction.TOP_INT;
  }

  public int getPixelFormat() {
    return Abstraction.TOP_INT;
  }

  public float getRefreshRate() {
    return Abstraction.TOP_FLOAT;
  }

  public float[] getSupportedRefreshRates() {
    return ((float[]) Abstraction.randomObject("float[]"));
  }

  public long getAppVsyncOffsetNanos() {
    return Abstraction.TOP_LONG;
  }

  public long getPresentationDeadlineNanos() {
    return Abstraction.TOP_LONG;
  }

  public void getMetrics(android.util.DisplayMetrics param0) {
  }

  public void getRealSize(android.graphics.Point param0) {
  }

  public void getRealMetrics(android.util.DisplayMetrics param0) {
  }

  public int getState() {
    return Abstraction.TOP_INT;
  }

  @Override
  public java.lang.String toString() {
    return Abstraction.TOP_STRING;
  }
}