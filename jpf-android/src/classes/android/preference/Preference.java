package android.preference;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.SharedPreferenceEvent;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;

public class Preference implements Comparable<Preference> {
  public static android.preference.Preference TOP = new android.preference.Preference();

  public Context mContext;

  protected OnPreferenceChangeListener mOnChangeListener;
  private OnPreferenceClickListener mOnClickListener;

  public String mKey;
  private String title;

  protected Object mDefaultValue;

  private List<Preference> mDependents;

  /**
   * Interface definition for a callback to be invoked when the value of this {@link Preference} has been
   * changed by the user and is about to be set and/or persisted. This gives the client a chance to prevent
   * setting and/or persisting the value.
   */
  public interface OnPreferenceChangeListener {
    /**
     * Called when a Preference has been changed by the user. This is called before the state of the
     * Preference is about to be updated and before the state is persisted.
     * 
     * @param preference
     *          The changed Preference.
     * @param newValue
     *          The new value of the Preference.
     * @return True to update the state of the Preference with the new value.
     */
    boolean onPreferenceChange(Preference preference, Object newValue);
  }

  /**
   * Interface definition for a callback to be invoked when a {@link Preference} is clicked.
   */
  public interface OnPreferenceClickListener {
    /**
     * Called when a Preference has been clicked.
     *
     * @param preference
     *          The Preference that was clicked.
     * @return True if the click was handled.
     */
    boolean onPreferenceClick(Preference preference);
  }

  public Preference() {
    // context will be null
  }

  public Preference(Context context) {
    this(context, null);
  }

  public Preference(Context context, AttributeSet attrs) {
    mContext = context;

    // for (Object att : attrs) {
    // Object[] attribute = (Object[]) att;
    // switch (((String) attribute[0])) {
    // case "android:title": {
    //
    // }
    // case "android:defaultValue": {
    // mDefaultValue = null; // should be overwritten
    // }
    // case "android:summary": {
    //
    // }
    //
    // case "android:key": {
    // mKey = (String) attribute[1];
    // }
    // default: {
    //
    // }
    //
    // }
    // }
  }

  public void setOnPreferenceChangeListener(Preference.OnPreferenceChangeListener listener) {
    this.mOnChangeListener = listener;
  }

  protected boolean callChangeListener(Object newValue) {
    return mOnChangeListener == null ? true : mOnChangeListener.onPreferenceChange(this, newValue);
  }

  public void setTitle(int resid) {

  }

  public void addPreferencesFromResource(int resId) {

  }

  public void setPersistent(boolean persistent) {
    // mPersistent = persistent;
  }

  public Preference findPreference(java.lang.CharSequence param0) {
    if (param0.equals(mKey))
      return this;
    else
      return null;
  }

  @Override
  public int compareTo(Preference o) {
    return 0;
  }

  /**
   * @return the mKey
   */
  public String getKey() {
    return mKey;
  }

  /**
   * @param mKey
   *          the mKey to set
   */
  public void setKey(String mKey) {
    this.mKey = mKey;
  }

  /**
   * @return the mDefaultValue
   */
  public Object getDefaultValue() {
    return mDefaultValue;
  }

  /**
   * @param mDefaultValue
   *          the mDefaultValue to set
   */
  public void setDefaultValue(Object mDefaultValue) {
    this.mDefaultValue = mDefaultValue;
  }

  /**
   * @return the mOnChangeListener
   */
  public OnPreferenceChangeListener getOnChangeListener() {
    return mOnChangeListener;
  }

  /**
   * @param mOnChangeListener
   *          the mOnChangeListener to set
   */
  public void setOnChangeListener(OnPreferenceChangeListener mOnChangeListener) {
    this.mOnChangeListener = mOnChangeListener;
  }

  /**
   * @return the mOnClickListener
   */
  public OnPreferenceClickListener getOnClickListener() {
    return mOnClickListener;
  }

  public void setOnPreferenceClickListener(OnPreferenceClickListener mOnClickListener) {
    this.mOnClickListener = mOnClickListener;
  }

  /**
   * @param mOnClickListener
   *          the mOnClickListener to set
   */
  public void setOnClickListener(OnPreferenceClickListener mOnClickListener) {
    this.mOnClickListener = mOnClickListener;
  }

  public OnPreferenceChangeListener getOnPreferenceChangeListener() {
    return this.mOnChangeListener;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title
   *          the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  public void setSummary(CharSequence s) {

  }

  public Context getContext() {
    return mContext;

  }

  public void setSummary(int res) {

  }

  protected int getPersistedInt(int defaultReturnValue) {
    return defaultReturnValue;
  }

  public float getPersistedFloat(float defaultReturnValue) {
    return defaultReturnValue;
  }

  public String getPersistedString(String defaultReturnValue) {
    return defaultReturnValue;
  }

  public boolean persistFloat(float f) {
    return true;
  }

  public boolean persistInt(int f) {
    return true;
  }

  public boolean shouldPersist() {
    return true;
  }

  boolean mEnabled = true;

  public void setEnabled(boolean enabled) {
    mEnabled = enabled;
  }

  public List<Event> getEvents() {
    List<Event> ret = new LinkedList<Event>();

    if (!mEnabled)
      return ret;

    SharedPreferenceEvent e = null;
    // if (this.mOnChangeListener != null) {
    // e = new SharedPreferenceEvent(this.mKey);
    // e.setListener("mOnChangeListener");
    // ret.add(e);
    // }

    if (this.mOnClickListener != null) {
      e = new SharedPreferenceEvent(this.mKey);
      e.setListener("mOnClickListener");
      ret.add(e);
    }

    if (this instanceof PreferenceGroup) {
      final int preferenceCount = ((PreferenceGroup) this).getPreferenceCount();
      for (int i = 0; i < preferenceCount; i++) {
        final Preference preference = ((PreferenceGroup) this).getPreference(i);
        List<Event> events = preference.getEvents();
        if (events.size() > 0)
          ret.addAll(events);
      }
    }

    return ret;
  }

  public void processEvent(SharedPreferenceEvent event) {
    if (event.getListener() != null) {
      if (event.getListener().equals("mOnChangeListener") && this.mOnChangeListener != null) {
        this.mOnChangeListener.onPreferenceChange(this, getDefaultValue());
      } else if (event.getListener().equals("mOnClickListener") && this.mOnClickListener != null) {
        this.mOnClickListener.onPreferenceClick(this);
      }
    }
  }

}
