package android.preference;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.SharedPreferenceEvent;
import gov.nasa.jpf.util.event.events.UIEvent;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DialogPreference extends Preference implements DialogInterface.OnClickListener,
    DialogInterface.OnDismissListener {

  /** The dialog, if it is showing. */
  // private Dialog mDialog;
  @FilterField
  private AlertDialog.Builder mBuilder;
  @FilterField
  View v = null;
  @FilterField
  int layoutRes = -1;

  public DialogPreference(Context c, AttributeSet attrs) {
    super(c, attrs);
  }

  public DialogPreference(Context c, AttributeSet attrs, int param2) {
    this(c, attrs);

  }

  public void setView(View v) {
    this.v = v;
  }

  /**
   * Prepares the dialog builder to be shown when the preference is clicked. Use this to set custom properties
   * on the dialog.
   * <p>
   * Do not {@link AlertDialog.Builder#create()} or {@link AlertDialog.Builder#show()}.
   */
  protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
  }

  protected android.view.View onCreateDialogView() {
    mBuilder = new AlertDialog.Builder(mContext).setPositiveButton("OK", this).setNegativeButton("Cancel",
        this);
    onPrepareDialogBuilder(mBuilder);
    if (layoutRes != -1) {
      LayoutInflater l = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      this.v = l.inflate(layoutRes, null, false);
    }
    return this.v;

  }

  public void onBindDialogView(View v) {
  }

  public void onDialogClosed(boolean param0) {
  }

  public void onSetInitialValue(boolean restore, Object defaultValue) {
  }

  @Override
  public void setDefaultValue(Object mDefaultValue) {
    onSetInitialValue(false, mDefaultValue);
    super.setDefaultValue(mDefaultValue);
  }

  @Override
  public List<Event> getEvents() {
    View v = onCreateDialogView();
    setView(v);
    onBindDialogView(v);

    List<Event> ret = super.getEvents();
    SharedPreferenceEvent e = null;
    if (v != null) {
      List<Event> viewEvents = v.collectEvents();
      for (Event ev : viewEvents) {
        e = new SharedPreferenceEvent(this.getKey());
        e.setEvent(ev);
        ret.add(e);
      }
    }
    e = new SharedPreferenceEvent(this.getKey());
    e.setListener("onDismiss");
    ret.add(e);
    return ret;
  }

  @Override
  public void processEvent(SharedPreferenceEvent event) {
    System.out.println("DialogPreference searching for preference: " + event.getKey() + "==" + getKey()
        + event.getEvent());
    super.processEvent(event);

    if (event.getListener() != null && event.getListener().equals("onDismiss")) {
      this.onDismiss(null);
      return;
    }

    if (event.getEvent() != null) {
      // find the view object by name
      if (v instanceof View && v.getName() != null
          && v.getName().equals(((UIEvent) event.getEvent()).getTarget().substring(1))) {
        v.processEvent(event.getEvent());
      } else if (v instanceof ViewGroup) {
        View vFound = ((ViewGroup) v)
            .findViewTraversal(((UIEvent) event.getEvent()).getTarget().substring(1));
        vFound.processEvent(event.getEvent());
        return;
      } else {
        throw new RuntimeException("Could not process event" + event);
      }
    }

  }

  public void setLayout(int rValue) {
    layoutRes = rValue;
  }

  @Override
  public void onDismiss(DialogInterface dialog) {
    onDialogClosed(true);
  }

  @Override
  public void onClick(DialogInterface dialog, int which) {

  }

}