package android.preference;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.EventProcessor;
import gov.nasa.jpf.util.event.InvalidEventException;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.SharedPreferenceEvent;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

public class PreferenceManager implements EventProcessor {

  public interface OnPreferenceTreeClickListener {

  }

  /**
   * @see #getActivity()
   */
  // private Activity mActivity;

  /**
   * Fragment that owns this instance.
   */
  // private PreferenceFragment mFragment;

  /**
   * Cached shared preferences.
   */
  // private SharedPreferences mSharedPreferences;

  /**
   * The {@link PreferenceScreen} at the root of the preference hierarchy.
   */
  @FilterField
  private static HashMap<Integer, PreferenceScreen> mPreferenceScreens = new HashMap<Integer, PreferenceScreen>();
  @FilterField
  static PreferenceInflater inflator;
  @FilterField
  public static PreferenceScreen currentScreen;
  @FilterField
  Context mContext;

  public PreferenceManager() {
  }

  public PreferenceManager(Context context) {
  }

  void init(Context context) {
    // inflate hierarchy
    inflator = PreferenceInflater.from(context);
    this.mContext = context;
  }

  public static android.content.SharedPreferences getDefaultSharedPreferences(Context context) {
    return context.getSharedPreferences(null, 0);
  }

  public SharedPreferences getSharedPreferences() {
    return getDefaultSharedPreferences(mContext);
  }

  private static String getDefaultSharedPreferencesName(Context context) {
    return "default_preferences";
  }

  private static int getDefaultSharedPreferencesMode() {
    return Context.MODE_PRIVATE;
  }

  public static void setDefaultValues(Activity context, int resourceID, boolean set) {
    // read preferences from file, otherwise default prefs are not set
    // SharedPreferences sp = context.getSharedPreferences(getDefaultSharedPreferencesName(context),
    // getDefaultSharedPreferencesMode());
    //
    // PreferenceScreen preferenceScreen = mPreferenceScreens.get(resourceID);
    // if (preferenceScreen == null) {
    // preferenceScreen = (PreferenceScreen) inflator.inflate(resourceID, null);
    // mPreferenceScreens.put(resourceID, preferenceScreen);
    // }
    // setSharedPreferences(preferenceScreen);
    // // TODO
  }

  public PreferenceScreen createPreferenceScreen(Context context) {
    final PreferenceScreen preferenceScreen = new PreferenceScreen(context);
    // preferenceScreen.onAttachedToHierarchy(this);
    return preferenceScreen;
  }

  public void setPreferenceScreen(PreferenceScreen preferenceScreen) {
    currentScreen = preferenceScreen;
  }

  public PreferenceScreen inflate(int resourceID) {
    PreferenceScreen preferenceScreen = mPreferenceScreens.get(resourceID);
    if (preferenceScreen == null) {
      preferenceScreen = (PreferenceScreen) inflator.inflate(resourceID, null);
      mPreferenceScreens.put(resourceID, preferenceScreen);
      // setSharedPreferences(preferenceScreen);
    }
    preferenceScreen.setEnabled(true);
    currentScreen = preferenceScreen;
    return preferenceScreen;
  }

  public static void setSharedPreferences(PreferenceScreen screen) {
    // save default values in shared prefs

  }

  public void setOnPreferenceTreeClickListener(PreferenceActivity preferenceActivity) {

  }

  @Override
  public Event[] getEvents() {
    List<Event> ret = new LinkedList<Event>();

    System.out.println("Getting preference Events");
    // only fire if preference screen is open
    if (mContext != null && mContext instanceof PreferenceActivity && !((Activity) mContext).isStopped()) {
      if (currentScreen != null) {
        try {
          ret.addAll(currentScreen.getEvents());
        } catch (Exception e) {
          // ignore
        }
        System.out.println("returning events : " + ret);
      }
      SharedPreferences pref = getSharedPreferences();
      if (pref != null && pref.getListeners() != null && pref.getListeners().size() > 0) {
        for (OnSharedPreferenceChangeListener l : pref.getListeners().keySet()) {
          SharedPreferenceEvent e = new SharedPreferenceEvent();
          e.setListener("onSharedPrefChange");
          ret.add(e); // custom for autoanswer
        }
      }
    }
    return ret.toArray(new Event[ret.size()]);
  }

  @Override
  public void processEvent(Event event) throws InvalidEventException {
    // sanity check
    if (event instanceof SharedPreferenceEvent) {
      // only if preference activity is visisble
      if (mContext != null && mContext instanceof PreferenceActivity && !((Activity) mContext).isStopped()) {
        SharedPreferences pref = getSharedPreferences();
        if (pref.getListeners() != null && pref.getListeners().size() > 0
            && ((SharedPreferenceEvent) event).getListener() != null
            && ((SharedPreferenceEvent) event).getListener().equals("onSharedPrefChange")) {
          String key = ((SharedPreferenceEvent) event).getKey();
          for (OnSharedPreferenceChangeListener l : pref.getListeners().keySet()) {
            l.onSharedPreferenceChanged(pref, (key == null) ? "enabled" : key);
          }
          return;
        }
        Preference p = currentScreen.findPreference(((SharedPreferenceEvent) event).getKey());
        if (p != null) {
          p.processEvent((SharedPreferenceEvent) event);
        } else {
          throw new RuntimeException("Cound not find preference for "
              + ((SharedPreferenceEvent) event).getKey());
        }
      }
    } else {
      // we have a problem
    }
  }

}