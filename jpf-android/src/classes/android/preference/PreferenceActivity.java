package android.preference;

import android.os.Bundle;
import android.os.ServiceManager;
import android.util.Log;
import android.view.ViewGroup;

public class PreferenceActivity extends android.app.Activity implements
    PreferenceManager.OnPreferenceTreeClickListener, PreferenceFragment.OnPreferenceStartFragmentCallback {

  public static final String TAG = PreferenceActivity.class.getSimpleName();

  PreferenceManager mPreferenceManager;
  PreferenceScreen mPreferenceScreen;

  ViewGroup root;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    Log.i(TAG, "new PreferenceManager created");
    mPreferenceManager = (PreferenceManager) ServiceManager.getSystemService("PREFERENCE_MANAGER");
    mPreferenceManager.init(this);

    mPreferenceManager.setOnPreferenceTreeClickListener(this);
    root = new ViewGroup(this);
    root.setName("background");
    super.onCreate(savedInstanceState);
    setContentView(root);

  }

  public void addPreferencesFromResource(int resourceID) {
    Log.i(TAG, "Adding preferences from Resource resourceID: " + resourceID);

    mPreferenceScreen = mPreferenceManager.inflate(resourceID);

  }

  public android.preference.Preference findPreference(java.lang.CharSequence key) {
    return mPreferenceScreen.findPreference(key);
  }

  @Override
  public boolean onPreferenceStartFragment(PreferenceFragment caller, Preference pref) {
    return false;
  }

  public void setPreferenceScreen(PreferenceScreen preferenceScreen) {
    mPreferenceManager.setPreferenceScreen(preferenceScreen);
    mPreferenceScreen = preferenceScreen;
    CharSequence title = getPreferenceScreen().getTitle();
    // Set the title of the activity
    if (title != null) {
      setTitle(title);
    }
  }

  public PreferenceScreen getPreferenceScreen() {
    if (mPreferenceManager != null) {
      return mPreferenceManager.currentScreen;
    }
    return null;
  }

  public PreferenceManager getPreferenceManager() {
    return mPreferenceManager;
  }

}