package android.os;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;
import gov.nasa.jpf.util.event.AndroidEventProducer;
import gov.nasa.jpf.util.event.events.KeyPressEvent;
import gov.nasa.jpf.util.event.events.LocationEvent;
import gov.nasa.jpf.util.event.events.SharedPreferenceEvent;
import gov.nasa.jpf.util.event.events.SystemEvent;
import gov.nasa.jpf.util.event.events.UIEvent;

import java.util.HashMap;

import models.ContactsContentProvider;
import models.DefaultContentProvider;
import android.app.ActivityThread;
import android.app.AlarmManager;
import android.app.ContextImpl;
import android.app.NotificationManager;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.JPFWindowManager;

import com.android.internal.policy.PolicyManager;
import com.android.server.BatteryService;
import com.android.server.am.ActivityManagerService;

/**
 * Keeps references to all system services and stores main system context. Some Services are remote and other
 * local. But from the view of the application the Services are all local.
 * 
 * @author Heila van der Merwe
 * 
 */
public class ServiceManager {
  private static final String TAG = "ServiceManager";


  private static ServiceManager manager;

  /** For quick lookup of services by name */
  @FilterField
  @NeverBreak
  private static HashMap<String, Object> SYSTEM_SERVICE_MAP;

  /** Direct references to services from the native ServiceManager class */
  @FilterField
  private ConnectivityManager connectionManager;
  @FilterField
  private PowerManager powerMananger;
  @FilterField
  private BatteryService batteryService;
  @FilterField
  private JPFWindowManager windowManager;
  @FilterField
  private PackageManager packageManager;
  @FilterField
  private ActivityManagerService activityManager;

  @FilterField
  private NotificationManager notificationManager;
  @FilterField
  private AudioManager audioManager;
  @FilterField
  private PowerManager powerManager;
  @FilterField
  private WifiManager wifimanager;
  @FilterField
  private PreferenceManager prefsManager;

  @FilterField
  private TelephonyManager telefManager;

  @FilterField
  private BluetoothManager blueManager;

  @FilterField
  private LocationManager locationManager;

  @FilterField
  private AlarmManager alarmManager;
  @FilterField
  private ClipboardManager clipboardManager;

  @FilterField
  private Context mSystemContext;

  @FilterField
  static int count = 0;

  private AndroidEventProducer eventProducer;

  public ServiceManager() {
    startServices();
  }

  public void startServices() {
    count++;
    init0();
    Log.i("ServiceManager", "************* Setting up Android Environment..." + count + " *************");

    SYSTEM_SERVICE_MAP = new HashMap<String, Object>();

    // Setup PackageManager
    packageManager = new PackageManager();
    SYSTEM_SERVICE_MAP.put("package", packageManager);

    mSystemContext = createSystemContext();

    // Setup ActivityManager
    activityManager = new ActivityManagerService(packageManager.getPackageInfo());
    SYSTEM_SERVICE_MAP.put(Context.ACTIVITY_SERVICE, activityManager);

    // Setup WindowManager
    windowManager = (JPFWindowManager) PolicyManager.makeNewWindowManager();
    SYSTEM_SERVICE_MAP.put(Context.WINDOW_SERVICE, windowManager);

    // Setup ConnectivityManager
    connectionManager = new ConnectivityManager(mSystemContext);
    SYSTEM_SERVICE_MAP.put(Context.CONNECTIVITY_SERVICE, connectionManager);

    // Setup BatteryManager
    batteryService = new BatteryService(mSystemContext);

    // Setup EventProducer
    eventProducer = new AndroidEventProducer();
    SYSTEM_SERVICE_MAP.put("eventproducer", eventProducer);

    // WifiManager
    wifimanager = new WifiManager(mSystemContext);
    SYSTEM_SERVICE_MAP.put(Context.WIFI_SERVICE, wifimanager);

    // WifiManager
    audioManager = new AudioManager();
    SYSTEM_SERVICE_MAP.put(Context.AUDIO_SERVICE, audioManager);

    // PowerManager
    powerManager = new PowerManager();
    SYSTEM_SERVICE_MAP.put(Context.POWER_SERVICE, powerManager);

    // WifiManager
    notificationManager = new NotificationManager();
    SYSTEM_SERVICE_MAP.put(Context.NOTIFICATION_SERVICE, notificationManager);

    // PrefsManager
    prefsManager = new PreferenceManager();
    SYSTEM_SERVICE_MAP.put("PREFERENCE_MANAGER", prefsManager);

    // TelephonyManager
    telefManager = new TelephonyManager();
    SYSTEM_SERVICE_MAP.put(Context.TELEPHONY_SERVICE, telefManager);

    // // PrefsManager
    // blueManager = new BluetoothManager(mSystemContext);
    // SYSTEM_SERVICE_MAP.put(Context.BLUETOOTH_SERVICE, blueManager);

    alarmManager = new AlarmManager();
    SYSTEM_SERVICE_MAP.put(Context.ALARM_SERVICE, alarmManager);

    clipboardManager = new ClipboardManager();
    SYSTEM_SERVICE_MAP.put(Context.CLIPBOARD_SERVICE, clipboardManager);

    locationManager = new LocationManager();
    SYSTEM_SERVICE_MAP.put(Context.LOCATION_SERVICE, locationManager);

    // Register services for event processing
    eventProducer.registerService(LocationEvent.class, locationManager);
    eventProducer.registerService(SystemEvent.class, activityManager);
    eventProducer.registerService(UIEvent.class, windowManager);
    eventProducer.registerService(KeyPressEvent.class, windowManager);
    eventProducer.registerService(SharedPreferenceEvent.class, prefsManager);
    // eventProducer.registerService(ConnectivityEvent.class,
    // connectionManager);
    // eventProducer.registerService(BatteryEvent.class, connectionManager);
    // /eventProducer.registerService(AlarmEvent.class.getName(), alarmManager);
  }

  /** Called from Activity's main method to start the system and the application */
  public static void start() {

    // Verify.beginAtomic();
    // Startup services
    ActivityThread thread = new ActivityThread();
    manager = new ServiceManager();

    // Create new ActivityThread for this application to handle inputs

    // Define the Resource class
    thread.setResources(new Resources());

    // Add ContentProvider for specific URI
    Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
    // thread.addContentProvider("media", new MusicContentProvider());
    thread.addContentProvider("contacts", new ContactsContentProvider());
    thread.addContentProvider("com.android.contacts", new ContactsContentProvider());
    thread.addContentProvider("com.nloko.provider.SyncMyPix", new ContactsContentProvider());
    thread.addContentProvider("default", new DefaultContentProvider());
    // thread.addContentProvider(Contacts.People.CONTENT_URI.getAuthority(), new ContactsContentProvider());
    // Verify.endAtomic();
    thread.start();

  }

  private native void init0();

  public static Object getSystemService(String name) {
    return SYSTEM_SERVICE_MAP.get(name);
  }

  /**
   * Global Context - application context can change for example setting to a different local or orientation
   * as what the current system is.
   * 
   * @return
   */
  public Context createSystemContext() {

    if (mSystemContext == null) {
      ContextImpl context = ContextImpl.createSystemContext(null);
      context.init(Resources.getSystem(), null); // set resources

      // LoadedApk info = new LoadedApk(null, "android", context, null, null);
      // context.init(info, null, null);
      // context.getResources().updateConfiguration(getConfiguration(),
      // getDisplayMetricsLocked(CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO,
      // false));
      mSystemContext = context;
      // Slog.i(TAG, "Created system resources " + context.getResources()
      // + ": " + context.getResources().getConfiguration());
    }

    return mSystemContext;
  }

  /**
   * Called by MessageQueue and all other objects that store a reference to the ServiceManager
   */
  public static ServiceManager getManager() {
    return manager;
  }

}
