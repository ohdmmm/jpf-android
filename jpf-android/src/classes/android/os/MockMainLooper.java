package android.os;

import gov.nasa.jpf.annotation.FilterFrame;
import gov.nasa.jpf.util.event.AndroidEventProducer;
import gov.nasa.jpf.vm.Verify;
import android.util.Log;
import android.util.LogPrinter;
import android.util.Printer;

public class MockMainLooper extends Looper {
  private static final boolean DEBUG_LOOPER = false;

  /**
   * Run the message queue in this thread. Be sure to call {@link #quit()} to end the loop.
   */
  public static void testloop() {
    Log.i(TAG, "Starting Looper");

    final Looper me = Looper.myLooper();

    if (me == null) {
      throw new RuntimeException("No Looper; Looper.prepare() wasn't called on this thread.");
    }
    MessageQueue queue = me.mQueue;

    // Make sure the identity of this thread is that of the local process,
    // and keep track of what that identity token actually is.
    // Binder.clearCallingIdentity();
    // final long ident = Binder.clearCallingIdentity();

    do {
      // This must be in a local variable, in case a UI event sets the
      // logger
      Printer logging = me.mLogging;
      Message msg = queue.next(); // might block
      if (msg != null) {
        if (msg.target == null) {
          // No target is a magic identifier for the quit message.
          if (logging != null) {
            logging.println(">>>>> Stopping loop " + msg.target + " " + msg.callback + ": " + msg.what);
            System.out.println("Stopping application");

          }
          return;
        }

        if (logging != null) {
          logging.println(">>>>> Dispatching to " + msg.target + " " + msg.callback + ": " + msg.what);
        }

        msg.target.dispatchMessage(msg);

        if (logging != null) {
          logging.println("<<<<< Finished to " + msg.target + " " + msg.callback);
        }

        msg.recycle();

      } else {
        queue.enqueueStopMessage();
        break;
      }
    } while (true);
  }

  /**
   * Run the message queue in this thread. Be sure to call {@link #quit()} to end the loop.
   */
  @FilterFrame
  public static void loopWait() {
    Log.i(TAG, "***************************** Start Looping **********************************");

    final Looper me = Looper.myLooper();
    if (me == null) {
      throw new RuntimeException("No Looper; Looper.prepare() wasn't called on this thread.");
    }

    do {
      boolean mContinue = true;

      if (!myQueue().isEmpty()) {
        System.out.println("MainMessageQueue: not empty.");
        mContinue = processMessage();
      } else {
        if (Thread.activeCount() > 1) {
          System.out.println("MainMessageQueue: continue looping due to multiple threads.");
          Thread.yield();
          continue;
        }
        System.out.println("MainMessageQueue: empty.");
        if (checkInputGenForEvent()) {
          continue;
        } else {
          System.out.println("MainMessageQueue: break and continue due to no more msgs in script");
          Verify.breakTransition("MainMessageQueue: break and continue due to no more msgs in script");
          continue;
        }
      }
      if (!mContinue) {
        break;
      }
    } while (true);
    Log.i(TAG, "***************************** Stop Looping **********************************");

  }

  @FilterFrame
  public static void loopWaitStop() {
    Log.i(TAG, "***************************** Start Looping **********************************");

    final Looper me = Looper.myLooper();
    if (me == null) {
      throw new RuntimeException("No Looper; Looper.prepare() wasn't called on this thread.");
    }

    boolean mContinue = true;
    do {

      if (!myQueue().isEmpty()) {
        System.out.println("MainMessageQueue: not empty.");
        mContinue = processMessage();
      } else {
        if (Thread.activeCount() > 1) {
          System.out.println("MainMessageQueue: continue looping due to multiple threads.");
          Thread.yield();
          continue;
        }
        if (mContinue) {
          System.out.println("MainMessageQueue: empty.");
          if (checkInputGenForEvent()) {
            continue;
          } else {
            System.out.println("MainMessageQueue: break and continue due to no more msgs in script");
            mContinue = false;
            continue;
          }
        }
      }
      if (!mContinue) {
        break;
      }
    } while (true);
    Log.i(TAG, "***************************** Stop Looping **********************************");

  }

  /**
   * Run the message queue in this thread. Be sure to call {@link #quit()} to end the loop.
   */
  public static void loop() {
    Log.i(TAG, "***************************** Start Looping **********************************");

    if (Looper.myLooper() == null) {
      throw new RuntimeException("No Looper; Looper.prepare() wasn't called on this thread.");
    }
    boolean mContinue = true;
    do {
      if (!myQueue().isEmpty()) {
        mContinue = processMessage();
      } else {
        if (checkInputGenForEvent()) {
          continue;
        } else {
          System.out.println("No more messages on queue");
          Verify.breakTransition("No more messages on queue");
          continue;
        }
      }
      if (!mContinue) {
        break;
      }
    } while (true);
    Log.i(TAG, "***************************** Stop Looping **********************************");

  }

  public static boolean processMessage() {
    Message msg = myQueue().next(); // might block
    if (msg.target == null) {
      // No target is a magic identifier for the quit message.
      System.out.println(">>>>> Stop looping " + msg.target);
      return false;
    }
    System.out.println(">>>>> Dispatching to " + msg.target + " " + msg.callback + ": " + msg.what);
    msg.target.dispatchMessage(msg);
    System.out.println("<<<<< Finished to " + msg.target + " " + msg.callback);

    msg.recycle();
    return true;
  }

  /**
   * If true is returned we do not break but check msq for more msgs. if false returned we break the
   * transition since this thread is done so we want to induce a state match is no more threads are running or
   * not
   * 
   * 
   * @return true if we should continue looping. This is in the case that there are still more events to
   *         process or when the
   */
  private static boolean checkInputGenForEvent() {
    System.out.println("MainMessageQueue: checking for inputgen events.");
    AndroidEventProducer eventProducer = (AndroidEventProducer) ServiceManager.getSystemService("eventproducer");

    // are there more events to process?
    int continueLooping = eventProducer.processNextEvent();

    if (continueLooping == 2) {
      // we expect events will be put in the queue still
      return true;

    } else if (continueLooping == 1) {
      // stop this line of questioning -- we will be rescheduled
      return false;

    } else if (continueLooping == 0) {
      // no more events will be put in the queue since we have no more events to
      // process **except** if stop events are still being processed
      return false;

      // if (!AndroidVerify.getConfigString("event.strategy",
      // "script").equals("script")) {
      // // we are using input generation and not script
      // return myQueue().enqueueStopMessage();
      // // true if stop messages are pu on queue
      // } else {
      // // wait for other threads to finish and quit
      // // while (Thread.activeCount() > 1) {
      // // Thread.yield();
      // // }
      // // if (!myQueue().mMessages.isEmpty()) {
      // //
      // System.out.println("Looper was waiting for a thread and now has received more msgs from it.");
      // // return true;
      // // } else {
      // // System.out
      // //
      // .println("Looper was waiting for a thread and has NOT received more msgs from it - stoping.");
      // // return false;
      // // }
      // if (Thread.activeCount() > 1) {
      // System.out.println("Looper is waiting for a thread to complete- block");
      // return true;
      // } else
      // return false;
      // }
    }
    return false;
  }

  private MockMainLooper(boolean quitAllowed) {
    super(quitAllowed);
    mQueue = new MockMessageQueue(quitAllowed);

    if (DEBUG_LOOPER)
      mLogging = new LogPrinter(Log.DEBUG, TAG);

  }

  @Override
  public void quit() {
    Message msg = new Message();
    // NOTE: By enqueueing directly into the message queue, the
    // message is left with a null target. This is how we know it is
    // a quit message.
    mQueue.enqueueMessage(msg, 0);
  }

}
