/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.graphics;

import gov.nasa.jpf.vm.Abstraction;

/**
 * The Paint class holds the style and color information about how to draw geometries, text and bitmaps.
 */
public class Paint {
  public static final int ANTI_ALIAS_FLAG = 1;
  public static final int DEV_KERN_TEXT_FLAG = 256;
  public static final int DITHER_FLAG = 4;
  public static final int EMBEDDED_BITMAP_TEXT_FLAG = 1024;
  public static final int FAKE_BOLD_TEXT_FLAG = 32;
  public static final int FILTER_BITMAP_FLAG = 2;
  public static final int HINTING_OFF = 0;
  public static final int HINTING_ON = 1;
  public static final int LINEAR_TEXT_FLAG = 64;
  public static final int STRIKE_THRU_TEXT_FLAG = 16;
  public static final int SUBPIXEL_TEXT_FLAG = 128;
  public static final int UNDERLINE_TEXT_FLAG = 8;
  public static android.graphics.Paint TOP = new android.graphics.Paint();

  public enum Style {
    // public static android.graphics.Paint.Style TOP = new android.graphics.Paint.Style(0);
    /**
     * Geometry and text drawn with this style will be filled, ignoring all stroke-related settings in the
     * paint.
     */
    FILL(0),
    /**
     * Geometry and text drawn with this style will be stroked, respecting the stroke-related fields on the
     * paint.
     */
    STROKE(1),
    /**
     * Geometry and text drawn with this style will be both filled and stroked at the same time, respecting
     * the stroke-related fields on the paint. This mode can give unexpected results if the geometry is
     * oriented counter-clockwise. This restriction does not apply to either FILL or STROKE.
     */
    FILL_AND_STROKE(2);

    Style(int nativeInt) {
      this.nativeInt = nativeInt;
    }

    final int nativeInt;
  }

  /**
   * The Cap specifies the treatment for the beginning and ending of stroked lines and paths. The default is
   * BUTT.
   */
  public enum Cap {
    /**
     * The stroke ends with the path, and does not project beyond it.
     */
    BUTT(0),
    /**
     * The stroke projects out as a semicircle, with the center at the end of the path.
     */
    ROUND(1),
    /**
     * The stroke projects out as a square, with the center at the end of the path.
     */
    SQUARE(2);

    private Cap(int nativeInt) {
      this.nativeInt = nativeInt;
    }

    final int nativeInt;
  }

  /**
   * The Join specifies the treatment where lines and curve segments join on a stroked path. The default is
   * MITER.
   */
  public enum Join {
    /**
     * The outer edges of a join meet at a sharp angle
     */
    MITER(0),
    /**
     * The outer edges of a join meet in a circular arc.
     */
    ROUND(1),
    /**
     * The outer edges of a join meet with a straight line
     */
    BEVEL(2);

    private Join(int nativeInt) {
      this.nativeInt = nativeInt;
    }

    final int nativeInt;
  }

  /**
   * Align specifies how drawText aligns its text relative to the [x,y] coordinates. The default is LEFT.
   */
  public enum Align {
    /**
     * The text is drawn to the right of the x,y origin
     */
    LEFT(0),
    /**
     * The text is drawn centered horizontally on the x,y origin
     */
    CENTER(1),
    /**
     * The text is drawn to the left of the x,y origin
     */
    RIGHT(2);

    private Align(int nativeInt) {
      this.nativeInt = nativeInt;
    }

    final int nativeInt;
  }

  public static class FontMetrics {
    /**
     * The maximum distance above the baseline for the tallest glyph in the font at a given text size.
     */
    public float top;
    /**
     * The recommended distance above the baseline for singled spaced text.
     */
    public float ascent;
    /**
     * The recommended distance below the baseline for singled spaced text.
     */
    public float descent;
    /**
     * The maximum distance below the baseline for the lowest glyph in the font at a given text size.
     */
    public float bottom;
    /**
     * The recommended additional space to add between lines of text.
     */
    public float leading;
  }

  public static class FontMetricsInt {
    public int top;
    public int ascent;
    public int descent;
    public int bottom;
    public int leading;

    @Override
    public String toString() {
      return "FontMetricsInt: top=" + top + " ascent=" + ascent + " descent=" + descent + " bottom=" + bottom
          + " leading=" + leading;
    }
  }

  public Paint() {
  }

  public Paint(int param0) {
  }

  public Paint(android.graphics.Paint param0) {
  }

  public void reset() {
  }

  public void set(android.graphics.Paint param0) {
  }

  public int getFlags() {
    return Abstraction.TOP_INT;
  }

  public void setFlags(int param0) {
  }

  public int getHinting() {
    return Abstraction.TOP_INT;
  }

  public void setHinting(int param0) {
  }

  public final boolean isAntiAlias() {
    return Abstraction.TOP_BOOL;
  }

  public void setAntiAlias(boolean param0) {
  }

  public final boolean isDither() {
    return Abstraction.TOP_BOOL;
  }

  public void setDither(boolean param0) {
  }

  public final boolean isLinearText() {
    return Abstraction.TOP_BOOL;
  }

  public void setLinearText(boolean param0) {
  }

  public final boolean isSubpixelText() {
    return Abstraction.TOP_BOOL;
  }

  public void setSubpixelText(boolean param0) {
  }

  public final boolean isUnderlineText() {
    return Abstraction.TOP_BOOL;
  }

  public void setUnderlineText(boolean param0) {
  }

  public final boolean isStrikeThruText() {
    return Abstraction.TOP_BOOL;
  }

  public void setStrikeThruText(boolean param0) {
  }

  public final boolean isFakeBoldText() {
    return Abstraction.TOP_BOOL;
  }

  public void setFakeBoldText(boolean param0) {
  }

  public final boolean isFilterBitmap() {
    return Abstraction.TOP_BOOL;
  }

  public void setFilterBitmap(boolean param0) {
  }

  public android.graphics.Paint.Style getStyle() {
    return ((android.graphics.Paint.Style) Abstraction.randomObject("android.graphics.Paint.Style"));
  }

  public void setStyle(android.graphics.Paint.Style param0) {
  }

  public int getColor() {
    return Abstraction.TOP_INT;
  }

  public void setColor(int param0) {
  }

  public int getAlpha() {
    return Abstraction.TOP_INT;
  }

  public void setAlpha(int param0) {
  }

  public void setARGB(int param0, int param1, int param2, int param3) {
  }

  public float getStrokeWidth() {
    return Abstraction.TOP_FLOAT;
  }

  public void setStrokeWidth(float param0) {
  }

  public float getStrokeMiter() {
    return Abstraction.TOP_FLOAT;
  }

  public void setStrokeMiter(float param0) {
  }

  public android.graphics.Paint.Cap getStrokeCap() {
    return ((android.graphics.Paint.Cap) Abstraction.randomObject("android.graphics.Paint.Cap"));
  }

  public void setStrokeCap(android.graphics.Paint.Cap param0) {
  }

  public android.graphics.Paint.Join getStrokeJoin() {
    return ((android.graphics.Paint.Join) Abstraction.randomObject("android.graphics.Paint.Join"));
  }

  public void setStrokeJoin(android.graphics.Paint.Join param0) {
  }

  public boolean getFillPath(android.graphics.Path param0, android.graphics.Path param1) {
    return Abstraction.TOP_BOOL;
  }

  public android.graphics.Shader getShader() {
    return null;
  }

  public android.graphics.Shader setShader(android.graphics.Shader param0) {
    return null;
  }

  public android.graphics.ColorFilter getColorFilter() {
    return null;
  }

  public android.graphics.ColorFilter setColorFilter(android.graphics.ColorFilter param0) {
    return null;
  }

  public android.graphics.Xfermode getXfermode() {
    return null;
  }

  public android.graphics.Xfermode setXfermode(android.graphics.Xfermode param0) {
    return null;
  }

  public android.graphics.PathEffect getPathEffect() {
    return null;
  }

  public android.graphics.PathEffect setPathEffect(android.graphics.PathEffect param0) {
    return null;
  }

  public android.graphics.MaskFilter getMaskFilter() {
    return null;
  }

  public android.graphics.MaskFilter setMaskFilter(android.graphics.MaskFilter param0) {
    return null;
  }

  public android.graphics.Typeface getTypeface() {
    return null;
  }

  public android.graphics.Typeface setTypeface(android.graphics.Typeface param0) {
    return null;
  }

  public android.graphics.Rasterizer getRasterizer() {
    return null;
  }

  public android.graphics.Rasterizer setRasterizer(android.graphics.Rasterizer param0) {
    return null;
  }

  public void setShadowLayer(float param0, float param1, float param2, int param3) {
  }

  public void clearShadowLayer() {
  }

  public android.graphics.Paint.Align getTextAlign() {
    return ((android.graphics.Paint.Align) Abstraction.randomObject("android.graphics.Paint.Align"));
  }

  public void setTextAlign(android.graphics.Paint.Align param0) {
  }

  public java.util.Locale getTextLocale() {
    return ((java.util.Locale) Abstraction.randomObject("java.util.Locale"));
  }

  public void setTextLocale(java.util.Locale param0) {
  }

  public boolean isElegantTextHeight() {
    return Abstraction.TOP_BOOL;
  }

  public void setElegantTextHeight(boolean param0) {
  }

  public float getTextSize() {
    return Abstraction.TOP_FLOAT;
  }

  public void setTextSize(float param0) {
  }

  public float getTextScaleX() {
    return Abstraction.TOP_FLOAT;
  }

  public void setTextScaleX(float param0) {
  }

  public float getTextSkewX() {
    return Abstraction.TOP_FLOAT;
  }

  public void setTextSkewX(float param0) {
  }

  public float getLetterSpacing() {
    return Abstraction.TOP_FLOAT;
  }

  public void setLetterSpacing(float param0) {
  }

  public java.lang.String getFontFeatureSettings() {
    return Abstraction.TOP_STRING;
  }

  public void setFontFeatureSettings(java.lang.String param0) {
  }

  public float ascent() {
    return Abstraction.TOP_FLOAT;
  }

  public float descent() {
    return Abstraction.TOP_FLOAT;
  }

  public float getFontMetrics(android.graphics.Paint.FontMetrics param0) {
    return Abstraction.TOP_FLOAT;
  }

  public android.graphics.Paint.FontMetrics getFontMetrics() {
    return ((android.graphics.Paint.FontMetrics) Abstraction
        .randomObject("android.graphics.Paint.FontMetrics"));
  }

  public int getFontMetricsInt(android.graphics.Paint.FontMetricsInt param0) {
    return Abstraction.TOP_INT;
  }

  public android.graphics.Paint.FontMetricsInt getFontMetricsInt() {
    return ((android.graphics.Paint.FontMetricsInt) Abstraction
        .randomObject("android.graphics.Paint.FontMetricsInt"));
  }

  public float getFontSpacing() {
    return Abstraction.TOP_FLOAT;
  }

  public float measureText(char[] param0, int param1, int param2) {
    return Abstraction.TOP_FLOAT;
  }

  public float measureText(java.lang.String param0, int param1, int param2) {
    return Abstraction.TOP_FLOAT;
  }

  public float measureText(java.lang.String param0) {
    return Abstraction.TOP_FLOAT;
  }

  public float measureText(java.lang.CharSequence param0, int param1, int param2) {
    return Abstraction.TOP_FLOAT;
  }

  public int breakText(char[] param0, int param1, int param2, float param3, float[] param4) {
    return Abstraction.TOP_INT;
  }

  public int breakText(java.lang.CharSequence param0, int param1, int param2, boolean param3, float param4,
                       float[] param5) {
    return Abstraction.TOP_INT;
  }

  public int breakText(java.lang.String param0, boolean param1, float param2, float[] param3) {
    return Abstraction.TOP_INT;
  }

  public int getTextWidths(char[] param0, int param1, int param2, float[] param3) {
    return Abstraction.TOP_INT;
  }

  public int getTextWidths(java.lang.CharSequence param0, int param1, int param2, float[] param3) {
    return Abstraction.TOP_INT;
  }

  public int getTextWidths(java.lang.String param0, int param1, int param2, float[] param3) {
    return Abstraction.TOP_INT;
  }

  public int getTextWidths(java.lang.String param0, float[] param1) {
    return Abstraction.TOP_INT;
  }

  public void getTextPath(char[] param0, int param1, int param2, float param3, float param4,
                          android.graphics.Path param5) {
  }

  public void getTextPath(java.lang.String param0, int param1, int param2, float param3, float param4,
                          android.graphics.Path param5) {
  }

  public void getTextBounds(java.lang.String param0, int param1, int param2, android.graphics.Rect param3) {
  }

  public void getTextBounds(char[] param0, int param1, int param2, android.graphics.Rect param3) {
  }

  @Override
  protected void finalize() throws java.lang.Throwable {
  }

}
