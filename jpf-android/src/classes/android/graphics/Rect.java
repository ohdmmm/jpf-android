package android.graphics;

import gov.nasa.jpf.vm.Abstraction;

public final class Rect implements android.os.Parcelable {
  public static final android.os.Parcelable.Creator CREATOR = ((android.os.Parcelable.Creator) Abstraction
      .randomObject("android.os.Parcelable.Creator"));
  public int bottom;
  public int left;
  public int right;
  public int top;
  public static android.graphics.Rect TOP = new android.graphics.Rect();

  public Rect() {
  }

  public Rect(int param0, int param1, int param2, int param3) {
  }

  public Rect(android.graphics.Rect param0) {
  }

  @Override
  public boolean equals(java.lang.Object param0) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public int hashCode() {
    return Abstraction.TOP_INT;
  }

  @Override
  public java.lang.String toString() {
    return Abstraction.TOP_STRING;
  }

  public java.lang.String toShortString() {
    return Abstraction.TOP_STRING;
  }

  public java.lang.String flattenToString() {
    return Abstraction.TOP_STRING;
  }

  public static android.graphics.Rect unflattenFromString(java.lang.String param0) {
    return android.graphics.Rect.TOP;
  }

  public final boolean isEmpty() {
    return Abstraction.TOP_BOOL;
  }

  public final int width() {
    return Abstraction.TOP_INT;
  }

  public final int height() {
    return Abstraction.TOP_INT;
  }

  public final int centerX() {
    return Abstraction.TOP_INT;
  }

  public final int centerY() {
    return Abstraction.TOP_INT;
  }

  public final float exactCenterX() {
    return Abstraction.TOP_FLOAT;
  }

  public final float exactCenterY() {
    return Abstraction.TOP_FLOAT;
  }

  public void setEmpty() {
  }

  public void set(int param0, int param1, int param2, int param3) {
  }

  public void set(android.graphics.Rect param0) {
  }

  public void offset(int param0, int param1) {
  }

  public void offsetTo(int param0, int param1) {
  }

  public void inset(int param0, int param1) {
  }

  public boolean contains(int param0, int param1) {
    return Abstraction.TOP_BOOL;
  }

  public boolean contains(int param0, int param1, int param2, int param3) {
    return Abstraction.TOP_BOOL;
  }

  public boolean contains(android.graphics.Rect param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean intersect(int param0, int param1, int param2, int param3) {
    return Abstraction.TOP_BOOL;
  }

  public boolean intersect(android.graphics.Rect param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean setIntersect(android.graphics.Rect param0, android.graphics.Rect param1) {
    return Abstraction.TOP_BOOL;
  }

  public boolean intersects(int param0, int param1, int param2, int param3) {
    return Abstraction.TOP_BOOL;
  }

  public static boolean intersects(android.graphics.Rect param0, android.graphics.Rect param1) {
    return Abstraction.TOP_BOOL;
  }

  public void union(int param0, int param1, int param2, int param3) {
  }

  public void union(android.graphics.Rect param0) {
  }

  public void union(int param0, int param1) {
  }

  public void sort() {
  }

  @Override
  public int describeContents() {
    return Abstraction.TOP_INT;
  }

  @Override
  public void writeToParcel(android.os.Parcel param0, int param1) {
  }

  public void readFromParcel(android.os.Parcel param0) {
  }
}
