package android.graphics;

import java.io.InputStream;

public class BitmapFactory {
  public static android.graphics.BitmapFactory TOP = new android.graphics.BitmapFactory();

  public static class Options {

    public int inSampleSize = 0;

  }

  public BitmapFactory() {
  }

  public static Bitmap decodeStream(InputStream is, Rect outPadding, Options opts) {
    return android.graphics.Bitmap.TOP;
  }

  public static android.graphics.Bitmap decodeResource(android.content.res.Resources param0, int param1) {
    return android.graphics.Bitmap.TOP;
  }

  public static Bitmap decodeByteArray(byte[] data, int offset, int length) {
    return android.graphics.Bitmap.TOP;
  }

  public static Bitmap decodeStream(InputStream is) {
    return Bitmap.TOP;
  }
}