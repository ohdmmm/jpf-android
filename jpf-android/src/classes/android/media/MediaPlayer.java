package android.media;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.FileDescriptor;
import java.io.IOException;

import android.content.Context;

public class MediaPlayer {
  /*
   * Do not change these values without updating their counterparts in include/media/mediaplayer.h!
   */
  /**
   * Unspecified media player info.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_UNKNOWN = 1;

  /**
   * The player was started because it was used as the next player for another player, which just completed
   * playback.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   * @hide
   */
  public static final int MEDIA_INFO_STARTED_AS_NEXT = 2;

  /**
   * The player just pushed the very first video frame for rendering.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_VIDEO_RENDERING_START = 3;

  /**
   * The video is too complex for the decoder: it can't decode frames fast enough. Possibly only the audio
   * plays fine at this stage.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_VIDEO_TRACK_LAGGING = 700;

  /**
   * MediaPlayer is temporarily pausing playback internally in order to buffer more data.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_BUFFERING_START = 701;

  /**
   * MediaPlayer is resuming playback after filling buffers.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_BUFFERING_END = 702;

  /**
   * Bad interleaving means that a media has been improperly interleaved or not interleaved at all, e.g has
   * all the video samples first then all the audio ones. Video is playing but a lot of disk seeks may be
   * happening.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_BAD_INTERLEAVING = 800;

  /**
   * The media cannot be seeked (e.g live stream)
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_NOT_SEEKABLE = 801;

  /**
   * A new set of metadata is available.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_METADATA_UPDATE = 802;

  /**
   * A new set of external-only metadata is available. Used by JAVA framework to avoid triggering track
   * scanning.
   * 
   * @hide
   */
  public static final int MEDIA_INFO_EXTERNAL_METADATA_UPDATE = 803;

  /**
   * Failed to handle timed text track properly.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   *
   *      {@hide}
   */
  public static final int MEDIA_INFO_TIMED_TEXT_ERROR = 900;

  /**
   * Subtitle track was not supported by the media framework.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_UNSUPPORTED_SUBTITLE = 901;

  /**
   * Reading the subtitle track takes too long.
   * 
   * @see android.media.MediaPlayer.OnInfoListener
   */
  public static final int MEDIA_INFO_SUBTITLE_TIMED_OUT = 902;

  private OnPreparedListener mOnPreparedListener;
  private OnCompletionListener mOnCompletionListener;
  private OnErrorListener mOnErrorListener;

  public interface OnPreparedListener {
    void onPrepared(MediaPlayer mp);
  }

  public interface OnCompletionListener {
    void onCompletion(MediaPlayer mp);
  }

  public interface OnErrorListener {
    boolean onError(MediaPlayer mp, int what, int extra);
  }

  /**
   * Possible states for the media player to be in. These states are as defined in the documentation for
   * {@link android.media.MediaPlayer}.
   */
  public static enum State {
    IDLE, INITIALIZED, PREPARING, PREPARED, STARTED, STOPPED, PAUSED, PLAYBACK_COMPLETED, END, ERROR
  }

  /** Current state of the media player. */
  @NeverBreak
  private State state = State.IDLE;

  public MediaPlayer() {
    // no must se;
    // no may se;
    state = State.INITIALIZED;
  }

  @FilterField
  @NeverBreak
  int i = 0;
  public int getCurrentPosition() {
    return i += 10;
  }

  public void setWakeMode(Context param0, int param1) {
    // no must se;
    // no may se;
  }

  public void setOnPreparedListener(OnPreparedListener param0) {
    // begin must se;
    this.mOnPreparedListener = param0;
    // end must se;
    // no may se;
  }

  public void setOnCompletionListener(OnCompletionListener param0) {
    // begin must se;
    this.mOnCompletionListener = param0;
    // end must se;
    // no may se;
  }

  public void setOnErrorListener(OnErrorListener param0) {
    // begin must se;
    this.mOnErrorListener = param0;
    // end must se;
    // no may se;
  }

  public void reset() {

    state = State.IDLE;
    this.mOnPreparedListener = null;
    this.mOnErrorListener = null;
    this.mOnCompletionListener = null;
    // no must se;
    // no may se;
  }

  public void pause() throws java.lang.IllegalStateException {
    state = State.PAUSED;

  }

  public void seekTo(int param0) throws java.lang.IllegalStateException {
    // no must se;
    // no may se;
    state = State.STARTED;
  }

  public void release() {
    state = State.STOPPED;
    // begin must se;
    this.mOnErrorListener = null;
    this.mOnPreparedListener = null;
    this.mOnCompletionListener = null;
    // end must se;
    // no may se;
  }

  public boolean isPlaying() {
    // no must se;
    // no may se;
    System.out.println("isPLaying:" + state);
    return state == State.STARTED;
    // AndroidVerify.getBoolean("MediaPlayer.isPlaying()"); //
    // State.STARTED;
  }

  public void setVolume(float param0, float param1) {
    // no must se;
    // no may se;
  }

  public void start() throws java.lang.IllegalStateException {

    int choice = AndroidVerify.getInt(0, 2, "MediaPlayer.start");
    this.state = State.STARTED;
    if (choice == 0) {
      System.out.println("Started MP without completing");
    } else if (choice == 1) {
      if (mOnErrorListener != null) {
        System.out.println("Started MP with errror");
        mOnErrorListener.onError(this, MEDIA_INFO_BAD_INTERLEAVING, 0);
      }
      state = State.ERROR;
    } else {
      if (mOnCompletionListener != null) {
        System.out.println("Started MP with completing");
        mOnCompletionListener.onCompletion(this);
      }
      state = State.PLAYBACK_COMPLETED;
    }

  }

  public void stop() {
    this.state = State.STOPPED;
  }

  public void setAudioStreamType(int param0) {
    // no must se;
    // no may se;
  }

  public void setDataSource(FileDescriptor fd, long a, long b) {

  }

  public void setDataSource(java.lang.String param0) throws java.io.IOException,
      java.lang.IllegalArgumentException, java.lang.SecurityException, java.lang.IllegalStateException {
    // begin must se;
    // end must se;
    // no may se;
  }

  public void setDataSource(android.content.Context param0, android.net.Uri param1)
      throws java.io.IOException, java.lang.IllegalArgumentException, java.lang.SecurityException,
      java.lang.IllegalStateException {
    boolean b = AndroidVerify.getBoolean("MediaPlayer.setDataSource()");
    if (b) {
      return;
    } else {
      throw new IOException("Could not set datasource");
    }

  }

  public void prepareAsync() throws java.lang.IllegalStateException {
    state = State.PREPARED;
    if (mOnPreparedListener != null)
      mOnPreparedListener.onPrepared(this);
  }

  public void prepare() throws IOException, IllegalStateException {
    state = State.PREPARED;
    if (mOnPreparedListener != null)
      mOnPreparedListener.onPrepared(this);
  }

}
