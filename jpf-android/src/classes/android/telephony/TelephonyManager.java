package android.telephony;

import android.content.Context;
import android.os.ServiceManager;

public class TelephonyManager {
  public static final java.lang.String EXTRA_STATE_RINGING = "RINGING";

  public TelephonyManager(Context context) {
  }

  public TelephonyManager() {
  }

  public static TelephonyManager getDefault() {
    return (TelephonyManager) ServiceManager.getSystemService(Context.TELEPHONY_SERVICE);
  }

  public int getCallState() {
    return 0;// AndroidVerify.getInt(0, 2, "TelephonyManager.getCallState");
  }

}