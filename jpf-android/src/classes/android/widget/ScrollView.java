package android.widget;

import android.content.Context;
import android.util.AttributeSet;

public class ScrollView extends FrameLayout {

  public ScrollView(Context param0) {
    super(param0);
  }

  public ScrollView(Context context, AttributeSet attrs, int defStyle) {
    super(context);
  }

}
