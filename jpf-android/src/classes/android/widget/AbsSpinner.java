package android.widget;

import android.content.Context;
import android.view.View;

public class AbsSpinner extends android.widget.AdapterView<SpinnerAdapter> {
  SpinnerAdapter mAdapter;

  public AbsSpinner(Context context) {
    super(context);
    initAbsSpinner();
  }

  /**
   * Common code for different constructor flavors
   */
  private void initAbsSpinner() {
    setFocusable(true);
    setWillNotDraw(false);
  }

  @Override
  public void setSelection(int param0) {
    mSelectedPosition = param0;
  }

  @Override
  public void setAdapter(SpinnerAdapter adapter) {
    if (null != mAdapter) {
      resetList();
    }

    mAdapter = adapter;

    mOldSelectedPosition = INVALID_POSITION;
    mOldSelectedRowId = INVALID_ROW_ID;

    if (mAdapter != null) {
      mOldItemCount = mItemCount;
      mItemCount = mAdapter.getCount();
      checkFocus();

      int position = mItemCount > 0 ? 0 : INVALID_POSITION;

      setSelectedPositionInt(position);
      setNextSelectedPositionInt(position);

      if (mItemCount == 0) {
        // Nothing selected
        checkSelectionChanged();
      }

    } else {
      checkFocus();
      resetList();
      // Nothing selected
      checkSelectionChanged();
    }

    requestLayout();

  }

  @Override
  public SpinnerAdapter getAdapter() {
    return mAdapter;
  }

  @Override
  public View getSelectedView() {
    if (mItemCount > 0 && mSelectedPosition >= 0) {
      return getChildAt(mSelectedPosition);
    } else {
      return null;
    }
  }

  void resetList() {
    mDataChanged = false;
    mNeedSync = false;

    this.removeViews();
    mOldSelectedPosition = INVALID_POSITION;
    mOldSelectedRowId = INVALID_ROW_ID;

    setSelectedPositionInt(INVALID_POSITION);
    setNextSelectedPositionInt(INVALID_POSITION);
    invalidate();
  }

  @Override
  public int getCount() {
    return mItemCount;
  }

  // @Override
  // public List<Event> collectEvents() {
  // List<Event> events = new LinkedList<Event>();
  // if (getAdapter() != null) {
  // if (mOnItemClickListener != null && getAdapter().getCount() > 0) {
  // for (int i = 0; i < getAdapter().getCount(); i++) {
  // // String s = (String) getAdapter().getItem(i);
  // UIEvent uiEvent = new UIEvent("$" + this.getName(), "onClickItem", new Object[] { i });
  // events.add(uiEvent);
  // }
  // }
  // if (mOnItemLongClickListener != null && getAdapter().getCount() > 0) {
  // UIEvent uiEvent = new UIEvent("$" + this.getName(), "onLongClickItem");
  // events.add(uiEvent);
  // }
  // }
  // events.addAll(super.collectEvents());
  // return events;
  // }
  //
  // @Override
  // public boolean processEvent(Event event) {
  // if (mOnItemClickListener != null && event instanceof UIEvent && ((UIEvent)
  // event).getAction().equals("onClickItem")) {
  // if (getAdapter() != null) {
  // int pos = (int) ((UIEvent) event).getArguments()[0];
  // (this).mOnItemClickListener.onItemClick(this, (this).getChildAt(pos), pos, pos);
  // return true;
  // }
  // } else if (mOnItemLongClickListener != null && event instanceof UIEvent && ((UIEvent)
  // event).getAction().equals("onLongClickItem")) {
  // if (getAdapter() != null) {
  // (this).mOnItemLongClickListener.onItemLongClick(this, (this).getChildAt(0), 0, 0);
  // return true;
  // }
  // }
  //
  // return super.processEvent(event);
  // }

}