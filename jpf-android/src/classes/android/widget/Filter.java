package android.widget;

public class Filter {
  public interface FilterListener {

  }

  public static android.widget.Filter TOP = new android.widget.Filter();

  public Filter() {
  }

  public final void filter(java.lang.CharSequence param0) {
  }

  protected FilterResults performFiltering(CharSequence constraint) {
    return null;
  }

  protected static class FilterResults {
    public FilterResults() {
      // nothing to see here
    }

    /**
     * <p>
     * Contains all the values computed by the filtering operation.
     * </p>
     */
    public Object values;

    /**
     * <p>
     * Contains the number of values computed by the filtering operation.
     * </p>
     */
    public int count;
  }

  protected void publishResults(CharSequence constraint, FilterResults results) {
  }

}