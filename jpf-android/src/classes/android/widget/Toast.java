package android.widget;

import android.content.Context;
import android.content.res.Resources.NotFoundException;

public class Toast {
  public static android.widget.Toast TOP = new android.widget.Toast();

  public Toast() {
  }

  public static android.widget.Toast makeText(android.content.Context param0,
                                              java.lang.CharSequence param1, int param2) {
    System.out.println("MAKING TOAST: " + param1);
    return android.widget.Toast.TOP;
  }

  public static android.widget.Toast makeText(Context context, int resId, int duration)
      throws NotFoundException {
    System.out.println("MAKING TOAST: " + context.getString(resId));

    return android.widget.Toast.TOP;

  }

  public void show() {
  }
}