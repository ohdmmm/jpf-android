package android.widget;

import gov.nasa.jpf.vm.Abstraction;
import android.content.Context;
import android.util.AttributeSet;

public class ImageButton extends android.widget.ImageView {

  public ImageButton(Context c) {
    super(c);
  }

  public ImageButton(Context c, AttributeSet set) {
    super(c);
  }

  public ImageButton(Context c, AttributeSet attrs, int defStyle) {
    super(c);
  }

  @Override
  protected boolean onSetAlpha(int param0) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  @Override
  public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo param0) {
  }

}