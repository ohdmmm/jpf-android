package android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class LinearLayout extends ViewGroup {

  public LinearLayout(Context context) {
    super(context);
  }

  public LinearLayout(Context context, AttributeSet att) {
    super(context);
  }

  @Override
  public void setPadding(int margin, int margin2, int margin3, int margin4) {

  }

  public void setOrientation(int orientation) {

  }
}
