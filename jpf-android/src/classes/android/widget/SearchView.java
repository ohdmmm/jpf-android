package android.widget;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.UIEvent;
import gov.nasa.jpf.vm.Abstraction;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorImpl;
import android.view.View;
import android.view.ViewGroup;

public class SearchView extends android.widget.LinearLayout implements android.view.CollapsibleActionView {
  // public static android.widget.SearchView TOP = new android.widget.SearchView();

  public static interface OnCloseListener {
  }

  public static interface OnQueryTextListener {
    // public static android.widget.SearchView.OnQueryTextListener TOP = new
    // android.widget.SearchView.OnQueryTextListener();
    public boolean onQueryTextChange(String newText);

    public boolean onQueryTextSubmit(String query);
  }

  public static class OnSuggestionListener {
    // public static android.widget.SearchView.OnSuggestionListener TOP = new
    // android.widget.SearchView.OnSuggestionListener();
    public OnSuggestionListener() {
    }
  }

  public SearchView(android.content.Context param0) {
    super(param0);
  }

  // public SearchView(android.content.Context param0, android.util.AttributeSet param1) {
  // }
  //
  // public SearchView(android.content.Context param0, android.util.AttributeSet param1, int param2) {
  // }
  //
  // public SearchView(android.content.Context param0, android.util.AttributeSet param1, int param2, int
  // param3) {
  // }

  public void setSearchableInfo(android.app.SearchableInfo param0) {
  }

  public void setImeOptions(int param0) {
  }

  public int getImeOptions() {
    return Abstraction.TOP_INT;
  }

  public void setInputType(int param0) {
  }

  public int getInputType() {
    return Abstraction.TOP_INT;
  }

  public void setOnQueryTextListener(android.widget.SearchView.OnQueryTextListener param0) {
    textListener = param0;
  }

  public void setOnCloseListener(android.widget.SearchView.OnCloseListener param0) {
    closeListener = param0;
  }

  public void setOnQueryTextFocusChangeListener(android.view.View.OnFocusChangeListener param0) {
    focusListener = param0;
  }

  public void setOnSuggestionListener(android.widget.SearchView.OnSuggestionListener param0) {
    suggestionListener = param0;
  }

  public void setOnSearchClickListener(android.view.View.OnClickListener param0) {
    this.setOnClickListener(param0);
  }

  OnQueryTextListener textListener;
  OnCloseListener closeListener;
  OnFocusChangeListener focusListener;
  OnSuggestionListener suggestionListener;

  @Override
  public List<Event> collectEvents() {
    List<Event> events = super.collectEvents();
    if (this.enabled && this.visible) {
      if (textListener != null) {
        UIEvent uiEvent = new UIEvent("$" + this.getName(), "onQueryTextChange");
        events.add(uiEvent);
        uiEvent = new UIEvent("$" + this.getName(), "onQueryTextSubmit");
        events.add(uiEvent);
      }
    }
    return events;
  }

  @Override
  public boolean processEvent(Event event) {
    if (event instanceof UIEvent) {
      String action = ((UIEvent) event).getAction();
      if (action.equals("onQueryTextChange")) {
        return textListener.onQueryTextChange("Text");
      } else if (action.equals("onQueryTextSubmit")) {
        return textListener.onQueryTextSubmit("Text");
      }
    }
    return super.processEvent(event);
  }

  public java.lang.CharSequence getQuery() {
    return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  }

  public void setQuery(java.lang.CharSequence param0, boolean param1) {
  }

  public void setQueryHint(java.lang.CharSequence param0) {
  }

  public java.lang.CharSequence getQueryHint() {
    return ((java.lang.CharSequence) Abstraction.randomObject("java.lang.CharSequence"));
  }

  public void setIconifiedByDefault(boolean param0) {
  }

  public boolean isIconfiedByDefault() {
    return Abstraction.TOP_BOOL;
  }

  public void setIconified(boolean param0) {
  }

  public boolean isIconified() {
    return Abstraction.TOP_BOOL;
  }

  public void setSubmitButtonEnabled(boolean param0) {
  }

  public boolean isSubmitButtonEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setQueryRefinementEnabled(boolean param0) {
  }

  public boolean isQueryRefinementEnabled() {
    return Abstraction.TOP_BOOL;
  }

  public void setSuggestionsAdapter(android.widget.CursorAdapter param0) {
  }

  public android.widget.CursorAdapter getSuggestionsAdapter() {
    return new SearchAdapter(mContext, new CursorImpl());
  }

  private class SearchAdapter extends CursorAdapter {

    public SearchAdapter(Context context, Cursor c) {
      super(context, c);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
      View v = new TextView(context);
      parent.addView(v);
      return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
      TextView c = (TextView) view;
      c.setText(cursor.getString(1));
    }
  }

  public void setMaxWidth(int param0) {
  }

  public int getMaxWidth() {
    return Abstraction.TOP_INT;
  }

  @Override
  protected void onMeasure(int param0, int param1) {
  }

  @Override
  protected void onDetachedFromWindow() {
  }

  @Override
  public boolean onKeyDown(int param0, android.view.KeyEvent param1) {
    return Abstraction.TOP_BOOL;
  }

  @Override
  public void onWindowFocusChanged(boolean param0) {
  }

  @Override
  public void onActionViewCollapsed() {
  }

  @Override
  public void onActionViewExpanded() {
  }

  @Override
  public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent param0) {
  }

  @Override
  public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo param0) {
  }
}