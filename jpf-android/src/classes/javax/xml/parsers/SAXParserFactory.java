package javax.xml.parsers;

import gov.nasa.jpf.vm.AndroidVerify;

public class SAXParserFactory {

  private static final SAXParserFactory TOP = new SAXParserFactory();

  public static SAXParserFactory newInstance() {
    return TOP;
  }

  public synchronized SAXParser newSAXParser() throws ParserConfigurationException {
    boolean b = AndroidVerify.getBoolean("SAXParserFactory.newSAXParser");
    if (b)
      return new AndroidSAXParser();
    else
      throw new ParserConfigurationException("Could not create parser");
  }
}
