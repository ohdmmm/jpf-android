import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.JPFConfigException;
import gov.nasa.jpf.JPFException;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;

/**
 * This class runs JPF-Android given a project name and configuration values and stored the results of the run
 * in a directory under the project name and time.
 * 
 * @author Heila
 *
 */
public class RunJPFAndroid {

  /** The name of the project used to group results together */
  protected final String projectName;
  /** The configuration object */
  protected final Config conf;
  /** The time the run was started */
  Date startTime;
  /** Store the logs and System.out contents in a file or discard it */
  boolean storedOutput = false;

  /** Cache values used to preload classes */
  public final String projectPath;

  /** Location of results */
  public final File output;

  /** Cache */
  public JPF jpf;
  /** Backup */
  PrintStream out;

  /** List of startup classes to preload */
  ArrayList<String> startupClasses = new ArrayList<String>();

  public RunJPFAndroid(String projectName, String[] configProperties) throws IOException {
    this.projectName = projectName;

    conf = JPF.createConfig(configProperties);

    System.out.flush();
    out = System.out;
    if (conf.getBoolean("jpf-android.store_output", false)) {
      System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(new File("output.log")))));
    } else {
      System.setOut(new PrintStream(new OutputStream() {
        @Override
        public void write(int b) {
          // DO NOTHING
        }
      }));
    }

    projectPath = conf.getString("projectpath");
    setStartupClasses(conf);
    startTime = new Date();
    output = createOutputDir(conf);
    jpf = new JPF(conf);
  }

  private void setStartupClasses(Config conf) {

    startupClasses.add("android.app.ActivityThread");
    startupClasses.add("android.app.ContextImpl");
    startupClasses.add("gov.nasa.jpf.vm.AndroidVerify");

    // load application classes
    listFilesAndFilesSubDirectories(projectPath + "/bin/classes", projectPath + "/bin/classes", true);
    // load custom models
    listFilesAndFilesSubDirectories("build/classes/" + projectName, "build/classes/" + projectName, false);

    String[] exclude = conf.getStringArray("jpf-android.excludeFromPreloading");
    if (exclude != null)
      for (String s : exclude) {
        startupClasses.remove(s);
      }

    String[] extraStartupClasses = conf.getStringArray("vm.extra_startup_classes");
    if (extraStartupClasses != null) {
      for (String extraCls : extraStartupClasses) {
        startupClasses.add(extraCls);
      }
    }
    StringBuilder ret = new StringBuilder();
    for (String s : startupClasses) {
      ret.append(s + ",");
    }
    String r = ret.toString();
    System.out.println("Loading classes...  " + r);
    conf.setProperty("vm.extra_startup_classes", r);
  }

  public void listFilesAndFilesSubDirectories(String directoryName, String dir, boolean app) {
    File directory = new File(directoryName);
    // get all the files from a directory
    File[] fList = directory.listFiles();
    if (fList != null) {
      for (File file : fList) {
        if (file.isFile() && file.getName().endsWith(".class")) {
          startupClasses.add(getClassName(file.getAbsolutePath(), dir));
        } else if (file.isDirectory()) {
          if (app && (file.getName().startsWith("bouncycastle") || file.getName().startsWith("base64Coder"))) {
            continue;
          }
          listFilesAndFilesSubDirectories(file.getAbsolutePath(), dir, app);
        }
      }
    }
  }

  public String getClassName(String filename, String directoryName) {
    int i = filename.indexOf(directoryName);
    filename = filename.substring(i + directoryName.length() + 1); // remove path
    filename = filename.substring(0, filename.length() - 6); // remove .class
    filename = filename.replace('/', '.');
    return filename;
  }

  public RunJPFAndroid(String projectName, String configFile) throws IOException {
    this(projectName, new String[] { configFile });
  }

  public RunJPFAndroid(String projectName, String configFile, String[] configProperties) throws IOException {
    this(projectName, increaseArray(configProperties, configFile));
  }

  private static String[] increaseArray(String[] theArray, String prepend) {
    int i = theArray.length;
    String[] newArray = new String[i + 1];
    newArray[0] = prepend;
    for (int cnt = 1; cnt < theArray.length; cnt++) {
      newArray[cnt] = theArray[cnt];
    }
    return newArray;
  }

  public void runJPF() {
    deleteDir("coverage");
    deleteDir("event");
    deleteDir("cgs");
    // deleteDir("tmp");
    deleteFile("statematching.txt");
    deleteFile("coverage.ec");
    deleteFile("coverageTotal.ec");

    // print output
    try {
      PrintWriter pw = new PrintWriter(output.getAbsoluteFile() + File.separator + projectName + "0.jpf");
      conf.print(pw);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    try {
      jpf.run();
      if (jpf.foundErrors()) {
        // ... process property violations discovered by JPF
      }
      moveDir("coverage", output);
      moveDir("event", output);
      moveDir("cgs", output);
      moveDir("path-app-logs", output);
      moveDir("tmp", output);

      moveFile("statematching.txt", output);
      moveFile("test.xml", output);
      moveFile("output.log", output);
      moveFile("result.txt", output);
      deleteFile("coverage.ec");
      deleteFile("coverageTotal.ec");

      // print output
      try {
        PrintWriter pw = new PrintWriter(output.getAbsoluteFile() + File.separator + projectName + ".jpf");
        conf.print(pw);
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      }

    } catch (JPFConfigException cx) {
      cx.printStackTrace();
      // ... handle configuration exception
      // ... can happen before running JPF and indicates inconsistent configuration data
    } catch (JPFException jx) {
      jx.printStackTrace();
      // ... handle exception while executing JPF, can be further differentiated into
      // ... JPFListenerException - occurred from within configured listener
      // ... JPFNativePeerException - occurred from within MJI method/native peer
      // ... all others indicate JPF internal errors
    } catch (Exception jx) {
      jx.printStackTrace();

    }

    System.out.println("---------- end -----------");
    PrintStream tmp = System.out;
    System.setOut(out);
    tmp.flush();
    tmp.close();

  }

  protected File createOutputDir(Config conf) throws IOException {
    String OUTPUT_FOLDER = conf.getProperty("jpf-android.output", null);
    if (OUTPUT_FOLDER == null) {
      DateFormat df = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
      OUTPUT_FOLDER = "results/" + projectName + "/results_" + conf.getString("event.strategy", "default")
          + "_sd_" + conf.getString("search.depth_limit") + "_ed_" + conf.getString("event.max_depth") + "-"
          + df.format(startTime);
    } else {
      DateFormat df = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
      OUTPUT_FOLDER = OUTPUT_FOLDER + "/" + projectName + "/results_"
          + conf.getString("event.strategy", "default") + "_sd_" + conf.getString("search.depth_limit")
          + "_ed_" + conf.getString("event.max_depth") + "-" + df.format(startTime);
    }
    Path p = Paths.get(OUTPUT_FOLDER);
    Files.createDirectories(p);
    return p.toFile();
  }

  protected final void deleteDir(String name) {
    File f = new File(name);
    if (f.exists() && f.isDirectory()) {
      try {
        FileUtils.deleteDirectory(f);
      } catch (IllegalArgumentException e) {
        // we know file does exist at this point so shouldn't be thrown
      } catch (IOException e) {
        e.printStackTrace();
      }
    } else {
      System.out.println("Directory " + name + " not deleted: does not exist");
    }
  }

  protected final static void deleteFile(String name) {
    File f = new File(name);
    if (f.exists() && f.isFile()) {
      try {
        FileUtils.forceDelete(f);
      } catch (IOException e) {
        e.printStackTrace();
      }
    } else {
      System.out.println("File " + name + " not deleted: does not exist");
    }
  }

  protected final void moveDir(String name, File dest) {
    File f = new File(name);
    if (f.exists() && f.isDirectory()) {
      try {
        FileUtils.moveDirectoryToDirectory(f, dest, true);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  protected final void moveFile(String name, File dest) {
    File f = new File(name);
    if (f.exists() && f.isFile()) {
      try {
        FileUtils.moveFileToDirectory(f, dest, true);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  protected final void copyFile(String name, File dest) {
    File f = new File(name);
    if (f.exists() && f.isFile()) {
      try {
        FileUtils.copyFileToDirectory(f, dest, true);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

}
