package gov.nasa.jpf.android;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.util.JPFLogger;
import gov.nasa.jpf.util.event.AndroidEventGenerator;
import gov.nasa.jpf.util.event.EventGenerator;
import gov.nasa.jpf.util.event.EventsProducer;
import gov.nasa.jpf.util.event.events.AndroidEvent;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.generator.HuristicEventGenerator;
import gov.nasa.jpf.util.event.generator.dynamic.MongoDBWrapper;
import gov.nasa.jpf.util.event.generator.dynamic.XStreamConverter;
import gov.nasa.jpf.util.event.tree.AndroidEventChoiceGenerator;
import gov.nasa.jpf.util.event.tree.EventNode;
import gov.nasa.jpf.util.event.tree.EventTree;
import gov.nasa.jpf.util.event.tree.EventTreeWrapper;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;
import gov.nasa.jpf.vm.SystemState;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import nhandler.conversion.ConversionException;
import nhandler.conversion.ConverterBase;
import nhandler.conversion.jpf2jvm.JPF2JVMConverter;
import nhandler.conversion.jvm2jpf.JVM2JPFConverter;

/**
 * The JPF_gov_nasa_jpf_AndroidEventProducer is responsible for scheduling the set of next events
 * non-deterministically. It creates and manages the {@link EventTree} as a history of which events were
 * returned successfully.
 * 
 * <h2>Retrieving next events</h2>
 * <p>
 * Events are retrieved from EventGenerators. The generators can access the state of the system via the
 * reference to the actual AndroidEventProducer object and retrieve information required. They are also able
 * to store and restore their state and register certain information to be used for state-matching.
 * </p>
 * 
 * <h2>Scheduling next events</h2>
 * <p>
 * After a set of non-deterministic events have been retrieved the events have to be stored and executed one
 * by one. This is done by putting the next events in the event tree. But when we backtrack in the program
 * execution, we should backtrack the position in the tree as well. The current position in the tree is stored
 * in a AndroidEventChoiceGenerator. For each set of events added to the tree the position in the tree changes
 * and a new ACG needs to be created to store the new position. The ACG is stored on a stack which position is
 * backtracked together with the state of the application.
 * </p>
 * 
 * <h2>Processing generated events</h2>
 * <p>
 * When a new ACG is pushed the current transition is ended and the current state is saved. The current
 * instruction is then re-executed for each choice - each re-scheduling creating a new transition with its
 * choice of event as the current event. The event is then executed by passing it to the native
 * AndroidEventProducer to be executed on the sut.
 * </p>
 * 
 * @author Heila van der Merwe <heilamvdm@gmail.com>
 * @date 26 March 18 August 2015 - changed to only push cg if more than one event available
 *
 */
public class JPF_gov_nasa_jpf_util_event_AndroidEventProducer extends NativePeer implements EventsProducer {
  protected static JPFLogger log = JPF.getLogger("event");

  /** The event tree storing all event sequences that have been generated */
  private static EventTreeWrapper eventTree;

  /** Current EventGenerator */
  private static EventGenerator eventGenerator;

  /** Events collected from JPF environment */
  protected Event[] events;
  String[] exclude;
  String[] include;

  /**
   * This method id called each time a new instance of AndroidEventProducer is created. The purpose of the
   * method is to set the variables of the AndroidEventProducer class from the configuration of the
   * environment stored in the native class (this class).
   * 
   * @param env
   * @param objRef
   */
  @MJI
  public void setup(MJIEnv env, int objRef) {
    if (eventGenerator == null) {
      // this class has not been initialized

      eventGenerator = new AndroidEventGenerator(env, this);
      ((AndroidEventGenerator) eventGenerator).registerListener(env.getJPF());
      exclude = env.getConfig().getStringArray("event.exclude");
      include = env.getConfig().getStringArray("event.include");

    }

    if (eventTree == null) {
      eventTree = new EventTreeWrapper(env.getConfig());
      eventTree.registerListener(env.getJPF());
    }

  }

  /**
   * Used for testing to reset this environment so that it can re-used with new config
   * 
   * @param env
   * @param objRef
   */
  @MJI
  public static void reset(MJIEnv env, int objRef) {
    if (eventGenerator != null) {
      eventGenerator.destroy(env.getJPF());
      eventGenerator = null;
    }
    if (eventTree != null) {
      eventTree.unregisterListener(env.getJPF());
      eventTree = null;
    }
  }

  @MJI
  public int getNextEvent(MJIEnv env, int objRef, int eventsRef) {

    ConverterBase.reset(env);
    System.out.println("###############################");
    log("Entering native getNextEvent() for " + getCurrentWindow(env, MJIEnv.NULL));
    Event retEvent = null;

    if (!eventTree.reachedMaxDepth()) { // no more events must be collected
      AndroidEventChoiceGenerator cg = env.getSystemState().getCurrentChoiceGeneratorOfType(
          AndroidEventChoiceGenerator.class);

      if (env.getThreadInfo().isFirstStepInsn() && cg != null) {
        // this is a new transition so get next event from current AndroidECG

        log("getNextEvent() - return next event in current ACG");
        retEvent = processNextChoice(env, objRef);

      } else {
        log("getNextEvent() - no current ACG");

        // either return event or end transition by pushing AndroidECG

        if (eventsRef == MJIEnv.NULL) {
          events = null; // either no events found or not using
        } else {
          events = (Event[]) JPF2JVM(env, eventsRef);
        }

        // get new events
        Event[] new_events = eventGenerator.getEvents(env, objRef, this);

        // filter events
        if (new_events != null && new_events.length > 0)
          new_events = filterEvents(new_events);

        // schedule new events
        if (new_events == null || new_events.length == 0) {
          retEvent = null; // no events available

        }
        // else if (new_events.length == 1) {
        // env.getVM().breakTransition("New event");
        // boolean visited = env.getVM().isVisitedState();
        // System.out.println("HAS THIS STATE HAS BEEN VISITED??? " + visited);
        // retEvent = new_events[0]; // one event available

        // }
        else {
          if (!new_events[0].equals(AndroidEvent.DEFAULT))
            scheduleAndroidEvents(env, objRef, new_events);
          retEvent = AndroidEvent.DEFAULT;
        }
      }
    }
    if (retEvent == null) {
      return MJIEnv.NULL;
    } else if (retEvent == AndroidEvent.DEFAULT) {
      // VM.getVM().getSearch().setIgnoredState(true);
      int retEventRef = JVM2JPF(env, retEvent);
      log("Returning " + retEvent + " for " + getCurrentWindow(env, MJIEnv.NULL));
      return retEventRef;
    } else {
      int retEventRef = JVM2JPF(env, retEvent);
      // int test = env.newObject("gov.nasa.jpf.util.event.UIEvent");
      // env.setReferenceField(test, "target", env.newString(((UIEvent) retEvent).getTarget()));
      eventTree.addEvent(retEvent);
      if (((AndroidEventGenerator) eventGenerator).eventGenerator instanceof HuristicEventGenerator)
        ((HuristicEventGenerator) ((AndroidEventGenerator) eventGenerator).eventGenerator).addEvent(retEvent);
      log("Returning " + retEvent + " for " + getCurrentWindow(env, MJIEnv.NULL));
      return retEventRef;
      // return test;
    }

  }

  /**
   * Filters Events using include/exclude filters specified in config file
   * 
   * @param include
   * @param exclude
   * @param nextEvents
   * @return
   */
  public Event[] filterEvents(Event[] nextEvents) {

    List<Event> ret = new LinkedList<Event>();
    if (exclude != null) {
      for (Event event : nextEvents) {
        boolean test = false;
        // if the event matched one of the exclusions - do not add it
        for (String e : exclude) {
          if (event.toString().matches(e)) {
            test = true;
            break;
          }
        }
        if (test == true) {
          continue;
        } else {
          ret.add(event);
        }
      }
    }
    if (include != null) {
      for (Event event : nextEvents) {
        boolean test = false;
        // if the event matched one of the exclusions - do not add it
        for (String e : include) {
          if (event.toString().matches(e)) {
            test = true;
          }
        }
        if (test == false) {
          continue;
        } else {
          ret.add(event);
        }
      }
    }
    if (include == null && exclude == null) {
      return nextEvents;
    } else {
      return ret.toArray(new Event[ret.size()]);
    }

  }

  private void scheduleAndroidEvents(MJIEnv env, int objRef, Event[] new_events) {
    SystemState ss = env.getSystemState();
    log("AndroidEventChoiceGenerator for events " + Arrays.toString(new_events));

    AndroidEventChoiceGenerator cg = new AndroidEventChoiceGenerator(new_events);
    ss.setNextChoiceGenerator(cg);

    log("Pushing new AndroidEventChoiceGenerator = " + cg.toString());

    env.repeatInvocation();
  }

  /**
   * This method is called when the processNextEvent methods was rescheduled due to a choice in calculating
   * the next event. It can be called in one of two cases: 1. An AlternativeChoiceGenerator was pushed due to
   * ANY's in the script or 2. because the next event (choice) needs to be retrieved from the current
   * AndroidEventChoiceGenerator.
   * 
   * @param env
   * 
   * @param objRef
   * 
   * @return true if more events are available in this path or false if no more events are available and we
   *         need to stop the application
   */
  protected Event processNextChoice(MJIEnv env, int objRef) {
    log("Retrieving next choice");
    SystemState ss = env.getSystemState();
    Event event = null;
    // Need to know if this transition was ended due to ScriptCG or event
    AndroidEventChoiceGenerator cg = ss.getCurrentChoiceGeneratorOfType(AndroidEventChoiceGenerator.class);

    if (cg != null) {
      log("AndroidEventChoiceGenerator loaded: " + cg.toString());
      event = cg.getNextChoice();
    } else {
      // we have been re-scheduled because of a ANY in the script.
    }
    return event;
  }

  private int JVM2JPF(MJIEnv env, Object obj) {
    try {
      int ret = JVM2JPFConverter.obtainJPFObj(obj, env);
      return ret;
    } catch (ConversionException e) {
      env.throwException(Exception.class.getName(), "Could not convert " + obj + " from JVM to JPF object.");
      e.printStackTrace();
    }
    return MJIEnv.NULL;
  }

  private Object JPF2JVM(MJIEnv env, int objRef) {
    Object obj = null;
    try {
      obj = JPF2JVMConverter.obtainJVMObj(objRef, env);
    } catch (ConversionException e) {
      env.throwException(Exception.class.getName(), "Could not convert " + objRef
          + " from JPF to JVM object.");
      e.printStackTrace();
    }
    return obj;
  }

  @MJI
  public boolean collectEvents(MJIEnv env, int objRef) {
    if (!eventTree.reachedMaxDepth()) { // no more events must be collected
      log("EventTree has **not** reached max depth");
      if (eventGenerator.collectEvents()) {
        log("Collect events for EventGenerator");
        return true;
      } else {
        log("Do not collect events for EventGenerator");
      }
    } else {
      log("EventTree reached max depth");
    }
    return false;
  }

  private static void log(String message) {
    log.info("AndroidEventGeneration: " + message);
  }

  @Override
  public String getCurrentWindow(MJIEnv env, int objRef) {
    String sectionName = JPF_android_view_JPFWindowManager.getCurrentWindow(env);
    if (sectionName == null) {
      sectionName = "default";
    }
    return sectionName;
  }

  @MJI
  public int getCurrentWindowName(MJIEnv env, int objRef) {
    String sectionName = JPF_android_view_JPFWindowManager.getCurrentWindow(env);
    if (sectionName == null) {
      sectionName = "default";
    }
    return env.newString(sectionName);
  }

  @Override
  public Event[] getEvents() {

    return filterEvents(this.events);
  }

  @MJI
  public int getEventTreeString(MJIEnv env, int objref) {
    return env.newString(eventTree.getEventPath());
  }

  @Override
  public boolean isFirstEvent(MJIEnv env) {
    if (env.getConfig().getBoolean("testing", false)) {
      return false;
    } else {
      return eventTree.isFirstEvent();
    }
  }

  @MJI
  public int getDynamicReturnValues(MJIEnv env, int objref, int iclassname, int imethod_name,
                                    int imethod_signature, int iparams) {
    try {
      MongoDBWrapper tester = new MongoDBWrapper("event_db", "events");
      XStreamConverter converter = new XStreamConverter();

      String classname = env.getStringObject(iclassname);
      String method_name = env.getStringObject(imethod_name);
      String method_signature = env.getStringObject(imethod_signature);
      Object[] args = (Object[]) JPF2JVM(env, iparams);
      String arg = converter.getXML(method_signature, args);

      // assert statements
      List<String> params = tester.getReturnObject(classname, method_name + method_signature, arg);
      System.out.println(params);

      Object[] returnObjs = new Object[params.size()];
      int index = 0;
      for (String p : params) {
        Object o = converter.getObject(p);
        returnObjs[index] = o;
        index++;
      }

      System.out.println(Arrays.toString(returnObjs));
      return JVM2JPF(env, returnObjs);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return MJIEnv.NULL;
  }

  public static void setNewWindow(String window) {
    eventTree.newWindow(window);

  }

  @Override
  public EventNode getCurrentEvent() {
    return eventTree.getCurrentEvent();
  }
}
