/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */
package gov.nasa.jpf.android;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.BooleanChoiceGenerator;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.IntChoiceGenerator;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;
import gov.nasa.jpf.vm.SystemState;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.TypedObjectSetChoice;
import gov.nasa.jpf.vm.choice.IntIntervalGenerator;

import java.util.HashMap;
import java.util.Random;

/**
 * native peer class for programmatic JPF interface (that can be used inside of apps to verify - if you are
 * aware of the danger that comes with it)
 * 
 * this peer is a bit different in that it only uses static fields and methods because its use is supposed to
 * be JPF global (without classloader namespaces)
 */
public class JPF_gov_nasa_jpf_vm_AndroidVerify extends NativePeer {
  private static final Random rand = new Random();
  private static String choice;

  public static boolean init(Config conf) {
    // random, first, all, last, next
    choice = conf.getString("verify.choice", "first");

    // read config values from file
    String[] inputMappings = conf.getKeysStartingWith("verify.input");
    if (inputMappings != null) {
      for (String s : inputMappings) {
        inputs.put(s.substring(13), conf.getStringArray(s));
      }
    }
    return true;
  }

  @MJI
  public static int getConfigString(MJIEnv env, int clsObjRef, int sOption, int sDef) {
    String option = env.getStringObject(sOption);
    String s = env.getConfig().getString(option);
    if (s == null)
      return sDef;
    else
      return env.newString(s);
  }

  @MJI
  public static boolean getConfigBoolean(MJIEnv env, int clsObjRef, int sOption, boolean sDef) {
    String option = env.getStringObject(sOption);
    boolean s = env.getConfig().getBoolean(option, sDef);
    return s;
  }

  @MJI
  public static int getValuesNative(MJIEnv env, int clsObjRef, int stringListRef, int sIdRef) {
    ThreadInfo ti = env.getThreadInfo();
    SystemState ss = env.getSystemState();
    String id = env.getStringObject(sIdRef);

    int[] refs = env.getReferenceArrayObject(stringListRef);

    if (!ti.isFirstStepInsn()) { // first time around

      if (!choice.equals("all")) {
        int c = MJIEnv.NULL;
        if (choice.equals("random")) {
          c = refs[rand.nextInt(refs.length)];
        } else if (choice.equals("first")) {
          c = refs[0];
        } else if (choice.equals("last")) {
          c = refs[refs.length - 1];
        }
        refs = new int[] { c };
      }

      TypedObjectSetChoice cg = new TypedObjectSetChoice("verifyGetValues([Ljava/lang/Object;) - " + id, refs);
      int ret = registerChoiceGenerator(env, ss, ti, cg, 0);
      return ret;

    } else {
      return getNextChoice(ss, "verifyGetValues([Ljava/lang/Object;) - " + id, TypedObjectSetChoice.class);
    }
  }

  @MJI
  public static int getIntNative(MJIEnv env, int clsObjRef, int min, int max, int sIdRef) {
    ThreadInfo ti = env.getThreadInfo();
    SystemState ss = env.getSystemState();
    String id = env.getStringObject(sIdRef);

    if (!ti.isFirstStepInsn()) { // first time around

      if (!choice.equals("all")) {
        int c = MJIEnv.NULL;
        if (choice.equals("random")) {
          int value = (max) - min;
          if (value == 0)
            c = value;
          else
            c = rand.nextInt((max) - min) + min;
        } else if (choice.equals("first")) {
          c = min;
        } else if (choice.equals("last")) {
          c = max;
        }
        min = c;
        max = c;

      }

      IntChoiceGenerator cg = new IntIntervalGenerator("verifyGetInt(II) - " + id, min, max);
      return registerChoiceGenerator(env, ss, ti, cg, 0);

    } else {
      return getNextChoice(ss, "verifyGetInt(II) - " + id, IntChoiceGenerator.class);
    }
  }

  /**
   * first - true last - false
   * 
   * @param env
   * @param clsObjRef
   * @param sIdRef
   * @return
   */
  @MJI
  public static boolean getBooleanNative(MJIEnv env, int clsObjRef, int sIdRef) {
    ThreadInfo ti = env.getThreadInfo();
    SystemState ss = env.getSystemState();
    ChoiceGenerator<?> cg = null;
    String id = env.getStringObject(sIdRef);

    if (!ti.isFirstStepInsn()) { // first time around

      if (choice.equals("all")) {
        cg = new BooleanChoiceGenerator(env.getConfig(), "verifyGetBoolean - " + id);
      } else if (choice.equals("random")) {
        int i = rand.nextInt(2);
        cg = new BooleanChoiceGenerator("verifyGetBoolean - " + id, (i == 0));
      } else if (choice.equals("first")) {
        cg = new BooleanChoiceGenerator("verifyGetBoolean - " + id, false);
      } else if (choice.equals("last")) {
        cg = new BooleanChoiceGenerator("verifyGetBoolean - " + id, true);
      }

      if (ss.setNextChoiceGenerator(cg)) {
        env.repeatInvocation();
      }
      return true; // not used if we repeat

    } else { // this is what really returns results
      return getNextChoice(ss, "verifyGetBoolean - " + id, BooleanChoiceGenerator.class);
    }
  }

  static <T> T registerChoiceGenerator(MJIEnv env, SystemState ss, ThreadInfo ti, ChoiceGenerator<T> cg,
                                       T dummyVal) {
    // need to check this?
    boolean breakSingleChoice = true;
    int n = cg.getTotalNumberOfChoices();
    if (n == 0) {
      // nothing, just return the default value
    } else if (n == 1 && !breakSingleChoice) {
      // no choice -> no CG optimization
      cg.advance();
      return cg.getNextChoice();
    } else {
      // ss.setForced(true);
      if (ss.setNextChoiceGenerator(cg)) {
        env.repeatInvocation();
      }
    }

    return dummyVal;
  }

  @SuppressWarnings("unchecked")
  static <T, C extends ChoiceGenerator<T>> T getNextChoice(SystemState ss, String id, Class<C> cgClass) {
    ChoiceGenerator<?> cg = ss.getCurrentChoiceGenerator(id, cgClass);

    assert (cg != null) : "no ChoiceGenerator of type " + cgClass.getName();
    T t = ((ChoiceGenerator<T>) cg).getNextChoice();
    if (!choice.equals("all")) {
      ((ChoiceGenerator<T>) cg).setDone();
    }
    return t;
  }

  private static HashMap<String, String[]> inputs = new HashMap<String, String[]>();

  @MJI
  public static int getTextInput(MJIEnv env, int clsObjRef, int nameRef) {
    String sName = env.getStringObject(nameRef);
    String[] values = inputs.get(sName);
    if (values == null) {
      // use default value
      values = new String[] { "123" };
    } else {
    }

    return env.newString(getStringNative(env, values, sName + ".getText()"));
  }

  @MJI
  public static int getStringNative(MJIEnv env, int clsObjRef, int stringListRef, int sIdRef) {
    ThreadInfo ti = env.getThreadInfo();
    SystemState ss = env.getSystemState();
    String id = env.getStringObject(sIdRef);

    int[] refs = env.getReferenceArrayObject(stringListRef);

    if (refs.length == 1)
      return refs[0];

    if (!ti.isFirstStepInsn()) { // first time around

      if (!choice.equals("all")) {
        int c = MJIEnv.NULL;
        if (choice.equals("random")) {
          c = refs[rand.nextInt(refs.length)];
        } else if (choice.equals("first")) {
          c = refs[0];
        } else if (choice.equals("last")) {
          c = refs[refs.length - 1];
        }
        refs = new int[] { c };
      }

      TypedObjectSetChoice cg = new TypedObjectSetChoice("verifyGetString([Ljava/lang/String;) - " + id, refs);
      return registerChoiceGenerator(env, ss, ti, cg, 0);

    } else {
      return getNextChoice(ss, "verifyGetString([Ljava/lang/String;) - " + id, TypedObjectSetChoice.class);
    }
  }

  public static String getStringNative(MJIEnv env, String[] refs, String id) {
    ThreadInfo ti = env.getThreadInfo();
    SystemState ss = env.getSystemState();

    if (refs.length == 1)
      return refs[0];

    if (!ti.isFirstStepInsn()) { // first time around

      if (!choice.equals("all")) {
        String c = null;
        if (choice.equals("random")) {
          c = refs[rand.nextInt(refs.length)];
          return c;
        } else if (choice.equals("first")) {
          c = refs[0];
        } else if (choice.equals("last")) {
          c = refs[refs.length - 1];
        }
        refs = new String[] { c };
      }
      IntChoiceGenerator cg = new IntIntervalGenerator("verifyGetString([Ljava/lang/String;) - " + id, 0,
          refs.length - 1);
      return refs[registerChoiceGenerator(env, ss, ti, cg, 0)];
    } else {
      int choice = getNextChoice(ss, "verifyGetString([Ljava/lang/String;) - " + id,
          IntIntervalGenerator.class);
      return refs[choice];
    }

  }
  //
  // @MJI
  // public static boolean getBoolInput(MJIEnv env, int clsObjRef, int nameRef) {
  // String sName = env.getStringObject(nameRef);
  // }
  //
  // @MJI
  // public static int getIntInput(MJIEnv env, int clsObjRef, int nameRef) {
  // String sName = env.getStringObject(nameRef);
  // }
}