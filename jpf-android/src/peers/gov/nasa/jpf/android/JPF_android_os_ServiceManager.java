package gov.nasa.jpf.android;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.android.AndroidProjectInfo.ProjectParseException;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.util.JPFLogger;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

import java.util.logging.Level;

public class JPF_android_os_ServiceManager extends NativePeer {
  private static final JPFLogger logger = JPF.getLogger("JPF_android_content_pm_PackageManager");

  @MJI
  public void init0(MJIEnv env, int classRef) throws ProjectParseException {
    // this.classRef = classRef;
    // make sure the projectInfo is initialized as soon as possible
    Config config = env.getConfig();
    String projectPath = config.getString("projectpath");
    String jpaPath = config.getString("jpf-android");
    logger.log(Level.INFO, "ServiceManager: Working directory: " + projectPath);

    // make sure the projectInfo is initialized as soon as possible
    AndroidSDKProjectInfo.initialize(jpaPath);
    AndroidProjectInfo.initialize(projectPath);
  }

}
